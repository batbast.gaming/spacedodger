package org.tyranobast.spacedodger;

import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.server.MainServer;

public class SuperMain {

    public static void main(String[] args) {
        if (args.length != 0 && args[0].equals("--launch-server")) {
            System.out.println("launching server...");
            MainServer.main(args);
        } else {
            System.out.println("launching client...");
            MainClient.main(args);
        }
    }
}
