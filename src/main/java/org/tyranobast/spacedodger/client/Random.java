package org.tyranobast.spacedodger.client;

public class Random {
    public static double random(double minimum, double maximum) {

        if (maximum < minimum) {
            double minTemp = minimum;
            minimum = maximum;
            maximum = minTemp;
        }

        if (maximum == minimum) {
            return maximum;
        }

        double range = maximum - minimum;

        double random = Math.random();
        random = minimum + (random * range);

        return random;
    }
}
