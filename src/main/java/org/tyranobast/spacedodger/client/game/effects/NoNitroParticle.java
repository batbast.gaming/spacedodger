package org.tyranobast.spacedodger.client.game.effects;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class NoNitroParticle {
    private int durationLeft;

    public NoNitroParticle() {
        this.durationLeft = 10;
    }

    public boolean shade() {
        durationLeft--;
        return durationLeft == 0;
    }

    public Rectangle display(double xStart, double yStart, double width, double height) {

        Rectangle rectangle = new Rectangle();
        rectangle.setX(xStart);
        rectangle.setY(yStart);
        rectangle.setWidth(width);
        rectangle.setHeight(height);
        rectangle.setFill(Color.DARKRED);
        rectangle.setOpacity(durationLeft * 10 / 100d);

        return rectangle;
    }
}
