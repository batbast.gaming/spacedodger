package org.tyranobast.spacedodger.client.game.effects;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.tyranobast.spacedodger.client.Random;

public class ShipReactorParticle {

    double x;
    double y;
    private double xTo;
    private double yTo;
    double durationLeft;
    double color;
    int particleSpeed;

    public ShipReactorParticle(double x, double y, double xTo, double yTo) {
        this.x = x;
        this.y = y;
        durationLeft = 10;
        this.particleSpeed = 10;

        if ((xTo > x + 20 || xTo < x - 20) && (yTo > y + 20 || yTo < y - 20)) {
            this.xTo = xTo;
            this.yTo = yTo;

            double baseDistance = distance((int) this.x, (int) this.y, (int) this.xTo, (int) this.yTo);
            this.xTo = Random.random((this.xTo + 0.1 * baseDistance), (this.xTo - 0.1 * baseDistance));
            this.yTo = Random.random((this.yTo + 0.1 * baseDistance), (this.yTo - 0.1 * baseDistance));
        } else {
            this.xTo = Random.random((this.x + 5), (this.x - 5));
            this.yTo = Random.random((this.y + 5), (this.y - 5));
        }
    }

    public void move(int gameFPS) {
        color = color + (25.5 * 10d / gameFPS);
        if (color > 255) {
            color = 255;
        }

        double oneTurnFlyDistance = particleSpeed * 10d / gameFPS;

        double distance = distance((int) this.x, (int) this.y, (int) this.xTo, (int) this.yTo);

        double partOfTheDistance = (oneTurnFlyDistance / distance);

        if (distance > partOfTheDistance) {
            this.x = partOfTheDistance * this.xTo + ((1 - partOfTheDistance) * this.x);
            this.y = partOfTheDistance * this.yTo + ((1 - partOfTheDistance) * this.y);
        }
    }

    private double distance(int xFrom, int yFrom, int xTo, int yTo) {
        return Math.sqrt(Math.pow((xTo - xFrom), 2) + Math.pow((yTo - yFrom), 2));
    }

    public boolean shade(int gameFPS) {
        durationLeft = durationLeft - 10d / gameFPS;
        return durationLeft == 0;
    }

    public Circle display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        int xCenter = (int) (gameScreenX + (double) gameScreenScale / 1000d * (this.x + 500));
        int yCenter = (int) (gameScreenY + (double) gameScreenScale / 1000d * (this.y + 500));

        Circle circle = new Circle();
        circle.setCenterX(xCenter);
        circle.setCenterY(yCenter);
        circle.setRadius(gameScreenScale / 150d);
        circle.setFill(Color.rgb(255, (int) color, 0));
        circle.setOpacity(durationLeft * 10 / 100d);

        return circle;
    }
}
