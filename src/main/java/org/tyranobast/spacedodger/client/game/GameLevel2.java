package org.tyranobast.spacedodger.client.game;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import org.tyranobast.spacedodger.client.Random;

import java.util.ArrayList;
import java.util.Collections;

public class GameLevel2 extends Game {

    private final ArrayList<MovingWall> walls;

    public GameLevel2(Group gameGroup, int gameScreenScale, int gameScreenX, int gameScreenY, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth);

        walls = new ArrayList<>();
    }

    @Override
    protected void initWalls() {
        walls.clear();

        indexColorToGive = 1;
        walls.add(new MovingWall(0, 0, 15, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        walls.add(new MovingWall(0, 0, 30, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        walls.add(new MovingWall(0, 0, 45, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        walls.add(new MovingWall(0, 0, 60, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
    }

    @Override
    protected boolean hasCollision() {
        if (walls.get(0).getDistance() <= 1) {
            double playerX = playerShip.getX();
            double playerY = playerShip.getY();
            double obstacleXStart = walls.get(0).getObstacleXStart();
            double obstacleYStart = walls.get(0).getObstacleYStart();
            double obstacleXEnd = walls.get(0).getObstacleXEnd();
            double obstacleYEnd = walls.get(0).getObstacleYEnd();
            int holeXStart = walls.get(0).getHoleXStart();
            int holeYStart = walls.get(0).getHoleYStart();
            int holeXEnd = walls.get(0).getHoleXEnd();
            int holeYEnd = walls.get(0).getHoleYEnd();

            boolean obstacleCollision = playerX > obstacleXStart && playerX < obstacleXEnd && playerY > obstacleYStart && playerY < obstacleYEnd;
            boolean wallCollision = !(playerX > holeXStart && playerX < holeXEnd && playerY > holeYStart && playerY < holeYEnd);

            return (obstacleCollision || wallCollision);
        }
        return false;
    }

    @Override
    protected void wallOperations() {
        MovingWall toAdd = null;
        MovingWall toRemove = null;

        Collections.reverse(walls);
        for (MovingWall movingWall : walls) {
            if (movingWall != null) {
                if (movingWall.approach() <= 0.1) {
                    score++;
                    toRemove = walls.get(walls.size() - 1);
                    toAdd = new MovingWall((int) Random.random(100, 500), (int) Random.random(100, 500), 60, actualColorToGive[indexColorToGive % 20], gameFPS);
                    int color = (int) (128 * ((10 * (actualColorToGive[indexColorToGive % 20] + 1) + 25) / 100f));
                    indexColorToGive++;
                    scene.setFill(Color.rgb(color % 10, color % 10, color % 10, 1));
                } else {
                    movingWall.move();
                    gameGroup.getChildren().addAll(movingWall.display(gameScreenScale, gameScreenX, gameScreenY));
                }
            }
        }

        Collections.reverse(walls);

        if (toAdd != null) {
            walls.add(toAdd);
        }

        walls.remove(toRemove);
    }
}
