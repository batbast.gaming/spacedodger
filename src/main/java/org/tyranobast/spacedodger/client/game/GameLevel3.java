package org.tyranobast.spacedodger.client.game;

import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import org.tyranobast.spacedodger.client.game.effects.NoNitroParticle;

import java.util.ArrayList;
import java.util.Collections;

public class GameLevel3 extends Game {

    private final ArrayList<EnemyWall> enemyWalls;
    private double shootCountdown;
    private boolean loose;

    public GameLevel3(Group gameGroup, int gameScreenScale, int gameScreenX, int gameScreenY, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth);

        this.loose = false;

        this.enemyWalls = new ArrayList<>();

        this.shootCountdown = 15;
    }

    @Override
    protected void initEvent() {
        CLICKButtonEventHandler = event -> {
            if (event.getCode() == KeyCode.N) {
                if (nitro == 100 && !playerShip.isNitroActivated()) {
                    playerShip.activateNitro();
                    bubbleParticleCountdown = 0;
                    bubbleParticles.clear();
                } else {
                    noNitroParticle = new NoNitroParticle();
                }
            } else if (event.getCode() == KeyCode.SPACE) {
                if (shootCountdown <= 0) {
                    playerShip.shoot();
                    shootCountdown = 2;
                }
            }

        };

        CLICKMouseEventHandler = event ->
                playerShip.setDirection((int) event.getX(), (int) event.getY(), gameScreenScale, gameScreenX, gameScreenY);

        MOUSEClickEventHandler = event -> {
            if (event.isSecondaryButtonDown()) {
                if (nitro == 100 && !playerShip.isNitroActivated()) {
                    playerShip.activateNitro();
                    bubbleParticleCountdown = 0;
                    bubbleParticles.clear();
                } else {
                    noNitroParticle = new NoNitroParticle();
                }
            } else if (event.isPrimaryButtonDown()) {
                if (shootCountdown <= 0) {
                    playerShip.shoot();
                    shootCountdown = 2;
                }
            }

        };

        MOUSEMoveEventHandler = event ->
                playerShip.setMouseDirection((int) event.getX(), (int) event.getY(), gameScreenScale, gameScreenX, gameScreenY);
    }

    @Override
    protected void initWalls() {
        loose = false;

        enemyWalls.clear();

        indexColorToGive = 1;
        enemyWalls.add(new EnemyWall(15, actualColorToGive[indexColorToGive % 20], gameFPS, playerShip, false));
        indexColorToGive++;
        enemyWalls.add(new EnemyWall(30, actualColorToGive[indexColorToGive % 20], gameFPS, playerShip, false));
        indexColorToGive++;
        enemyWalls.add(new EnemyWall(45, actualColorToGive[indexColorToGive % 20], gameFPS, playerShip, false));
        indexColorToGive++;
        enemyWalls.add(new EnemyWall(60, actualColorToGive[indexColorToGive % 20], gameFPS, playerShip, false));
        indexColorToGive++;
    }

    private boolean isEnemyDestroyed(EnemyWall enemyWall, ArrayList<Projectile> playerProjectiles) {
        double enemyWallXMin = enemyWall.getX() - enemyWall.getShipScale();
        double enemyWallXMax = enemyWall.getX() + enemyWall.getShipScale();
        double enemyWallYMin = enemyWall.getY() - enemyWall.getShipScale();
        double enemyWallYMax = enemyWall.getY() + enemyWall.getShipScale();

        for (Projectile projectile : playerProjectiles) {
            if (projectile.getDistance() < enemyWall.getDistance() + 1 && projectile.getDistance() > enemyWall.getDistance() - 1) {
                if (!enemyWall.isDead() && projectile.getX() < enemyWallXMax && projectile.getX() > enemyWallXMin && projectile.getY() < enemyWallYMax && projectile.getY() > enemyWallYMin) {
                    playerShip.removeProjectiles();
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected boolean hasCollision() {
        ArrayList<Projectile> projectiles = new ArrayList<>();
        projectiles.addAll(enemyWalls.get(0).getProjectiles());
        projectiles.addAll(enemyWalls.get(1).getProjectiles());
        projectiles.addAll(enemyWalls.get(2).getProjectiles());
        projectiles.addAll(enemyWalls.get(3).getProjectiles());
        double playerX = playerShip.getX();
        double playerY = playerShip.getY();
        int holeXStart = enemyWalls.get(0).getHoleXStart();
        int holeYStart = enemyWalls.get(0).getHoleYStart();
        int holeXEnd = enemyWalls.get(0).getHoleXEnd();
        int holeYEnd = enemyWalls.get(0).getHoleYEnd();

        for (Projectile projectile : projectiles) {
            double playerShipScale = playerShip.getShipScale();
            if (projectile.getDistance() < 1) {
                if (projectile.getX() < playerX + playerShipScale && projectile.getX() > playerX - playerShipScale && projectile.getY() < playerY + playerShipScale && projectile.getY() > playerY - playerShipScale) {
                    return true;
                }
            }
        }

        if (enemyWalls.get(0).getDistance() <= 1) {
            return !(playerX > holeXStart && playerX < holeXEnd && playerY > holeYStart && playerY < holeYEnd);
        }

        return loose;
    }

    @Override
    protected void wallOperations() {
        ArrayList<Projectile> playerProjectiles = playerShip.getProjectiles();

        shootCountdown = shootCountdown - 10d / gameFPS;

        EnemyWall toAdd = null;
        EnemyWall toRemove = null;

        enemyWalls.get(1).shot();

        Collections.reverse(enemyWalls);
        for (EnemyWall enemyWall : enemyWalls) {
            if (enemyWall != null) {
                if (enemyWall.approach() <= 0.1) {
                    if (enemyWall.isDead()) {
                        score++;
                        toRemove = enemyWalls.get(enemyWalls.size() - 1);
                        toAdd = new EnemyWall(60, actualColorToGive[indexColorToGive % 20], gameFPS, playerShip, true);
                        int color = (int) (128 * ((10 * (actualColorToGive[indexColorToGive % 20] + 1) + 25) / 100f));
                        indexColorToGive++;
                        scene.setFill(Color.rgb(color % 10, color % 10, color % 10, 1));
                    } else {
                        loose = true;
                        return;
                    }
                } else {
                    enemyWall.move();
                    gameGroup.getChildren().addAll(enemyWall.display(gameScreenScale, gameScreenX, gameScreenY));
                }
                if (isEnemyDestroyed(enemyWall, playerProjectiles)) {
                    enemyWall.die();
                }
            }
        }

        for (EnemyWall enemyWall : enemyWalls) {
            gameGroup.getChildren().addAll(enemyWall.displayProjectiles(gameScreenScale, gameScreenX, gameScreenY));
        }

        Collections.reverse(enemyWalls);

        if (toAdd != null) {
            enemyWalls.add(toAdd);
        }

        enemyWalls.remove(toRemove);
    }
}
