package org.tyranobast.spacedodger.client.game;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

class Projectile {

    private final double x;
    private final double y;
    private double distance;
    private final String sender;

    public Projectile(double x, double y, double distance, String sender) {
        this.sender = sender;
        this.distance = distance;
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getDistance() {
        return distance;
    }

    public double approach(int gameFPS) {
        int speed = 3;
        if (sender.equals("enemy")) {
            for (int i = 0; i < speed; i++) {
                if (distance <= 25 && distance >= 1) {
                    distance = distance - (0.1f * distance) * (10d / gameFPS);
                } else if (distance <= 1) {
                    distance = distance - 0.1f * (10d / gameFPS);
                } else {
                    distance = distance - 10d / gameFPS;
                }
            }
        } else if (sender.equals("player")) {
            for (int i = 0; i < speed; i++) {
                if (distance <= 25 && distance >= 1) {
                    distance = distance + (0.1f * distance) * (10d / gameFPS);
                } else if (distance <= 1) {
                    distance = distance + 0.1f * (10d / gameFPS);
                } else {
                    distance = distance + 10d / gameFPS;
                }
            }
        }
        return distance;
    }

    public Circle display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        int xStartShotGame = (int) (this.x / distance + 500);
        int yStartShotGame = (int) (this.y / distance + 500);

        int xStartShotScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xStartShotGame);
        int yStartShotScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yStartShotGame);

        Circle shot = new Circle();
        shot.setCenterX(xStartShotScreen);
        shot.setCenterY(yStartShotScreen);
        if (sender.equals("enemy")) {
            shot.setFill(Color.RED);
        } else if (sender.equals("player")) {
            shot.setFill(Color.LIMEGREEN);
        }
        shot.setRadius(gameScreenScale / 200d / distance);

        return shot;
    }
}