package org.tyranobast.spacedodger.client.game;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.Random;
import org.tyranobast.spacedodger.client.game.effects.NitroBubbleParticle;
import org.tyranobast.spacedodger.client.game.effects.NoNitroParticle;
import org.tyranobast.spacedodger.client.MainClient;

import java.util.ArrayList;
import java.util.Collections;

public class Game {

    protected final Group gameGroup;
    protected final int gameScreenScale;
    protected final int gameScreenX;
    protected final int gameScreenY;
    protected final int screenWidth;
    protected final int screenHeight;
    protected final ArrayList<Wall> walls = new ArrayList<>();
    protected final ArrayList<NitroBubbleParticle> bubbleParticles = new ArrayList<>();
    protected PlayerShip playerShip;
    protected int mainLoopActive;
    protected int[] actualColorToGive;
    protected int indexColorToGive;
    protected final Scene scene;
    protected Timeline gameTimeline;
    protected int score;
    protected double nitro;
    protected NoNitroParticle noNitroParticle;
    protected int gameFPS;
    protected double bubbleParticleCountdown;
    protected String controlType;
    protected EventHandler<KeyEvent> CLICKButtonEventHandler;
    protected EventHandler<MouseEvent> CLICKMouseEventHandler;
    protected EventHandler<MouseEvent> MOUSEClickEventHandler;
    protected EventHandler<MouseEvent> MOUSEMoveEventHandler;
    protected boolean eventInitialized;

    protected Game(Group gameGroup, int gameScreenScale, int gameScreenX, int gameScreenY, int screenHeight, int screenWidth) {
        this.gameGroup = gameGroup;
        this.gameScreenScale = gameScreenScale;
        this.gameScreenX = gameScreenX;
        this.gameScreenY = gameScreenY;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.scene = gameGroup.getScene();
        this.bubbleParticleCountdown = 0;
        double gameSpeed = MainClient.config.getDoubleValue("game_speed");
        this.gameFPS = MainClient.config.getIntValue("game_fps");
        this.controlType = MainClient.config.getStringValue("control");
        this.eventInitialized = false;

        initEvent();

        gameTimeline = new Timeline(new KeyFrame(Duration.seconds(gameSpeed * 10 / gameFPS), event -> {
            if (mainLoopActive == 0) {
                mainLoopActive++;
                mainGameLoop();
            }
        }));

        gameTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    protected void initEvent() {
        CLICKButtonEventHandler = event -> {
            if (event.getCode() == KeyCode.N) {
                if (nitro == 100 && !playerShip.isNitroActivated()) {
                    playerShip.activateNitro();
                    bubbleParticleCountdown = 0;
                    bubbleParticles.clear();
                } else {
                    noNitroParticle = new NoNitroParticle();
                }
            }
        };

        CLICKMouseEventHandler = event ->
                playerShip.setDirection((int) event.getX(), (int) event.getY(), gameScreenScale, gameScreenX, gameScreenY);

        MOUSEClickEventHandler = event -> {
            if (event.isSecondaryButtonDown()) {
                if (nitro == 100 && !playerShip.isNitroActivated()) {
                    playerShip.activateNitro();
                    bubbleParticleCountdown = 0;
                    bubbleParticles.clear();
                } else {
                    noNitroParticle = new NoNitroParticle();
                }

            }
        };

        MOUSEMoveEventHandler = event -> {
            if (playerShip != null) {
                playerShip.setMouseDirection((int) event.getX(), (int) event.getY(), gameScreenScale, gameScreenX, gameScreenY);
            }
        };
    }

    protected void initWalls() {
        walls.clear();

        indexColorToGive = 1;
        walls.add(new Wall(-450, -450, 450, 450, 15, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        walls.add(new Wall(-450, -450, 450, 450, 30, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        walls.add(new Wall(-450, -450, 450, 450, 45, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        walls.add(new Wall(-450, -450, 450, 450, 60, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
    }

    protected void initGame() {

        bubbleParticleCountdown = 0;

        actualColorToGive = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2};

        playerShip = new PlayerShip();

        initWalls();

        mainLoopActive = 0;

        nitro = 0;

        score = 0;

        scene.setFill(Color.BLACK);
    }

    protected void reloadConfig() {
        String oldControlType = this.controlType;
        this.controlType = MainClient.config.getStringValue("control");
        this.gameFPS = MainClient.config.getIntValue("game_fps");
        double gameSpeed = MainClient.config.getDoubleValue("game_speed");

        gameTimeline = new Timeline(new KeyFrame(Duration.seconds(gameSpeed * 10 / gameFPS), event -> {
            if (mainLoopActive == 0) {
                mainLoopActive++;
                mainGameLoop();
            }
        }));
        gameTimeline.setCycleCount(Timeline.INDEFINITE);


        if (!oldControlType.equals(controlType) || !eventInitialized) {
            if (this.controlType.equals("click")) {
                if (!eventInitialized) {
                    scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, MOUSEClickEventHandler);
                    scene.removeEventHandler(MouseEvent.MOUSE_MOVED, MOUSEMoveEventHandler);
                    eventInitialized = true;
                }
                scene.addEventHandler(KeyEvent.KEY_PRESSED, CLICKButtonEventHandler);
                scene.addEventHandler(MouseEvent.MOUSE_PRESSED, CLICKMouseEventHandler);
            } else if (this.controlType.equals("mouse")) {
                if (!eventInitialized) {
                    scene.removeEventHandler(KeyEvent.KEY_PRESSED, CLICKButtonEventHandler);
                    scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, CLICKMouseEventHandler);
                    eventInitialized = true;
                }
                scene.addEventHandler(MouseEvent.MOUSE_PRESSED, MOUSEClickEventHandler);
                scene.addEventHandler(MouseEvent.MOUSE_MOVED, MOUSEMoveEventHandler);
            }
        }
    }

    public void startGame() {
        reloadConfig();
        initGame();
        gameTimeline.play();
    }

    protected void stopGame() {
        scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, MOUSEClickEventHandler);
        scene.removeEventHandler(MouseEvent.MOUSE_MOVED, MOUSEMoveEventHandler);
        scene.removeEventHandler(KeyEvent.KEY_PRESSED, CLICKButtonEventHandler);
        scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, CLICKMouseEventHandler);
        gameTimeline.stop();
    }

    protected boolean hasCollision() {
        if (walls.get(0).getDistance() <= 1) {
            double playerX = playerShip.getX();
            double playerY = playerShip.getY();
            int holeXStart = walls.get(0).getHoleXStart();
            int holeYStart = walls.get(0).getHoleYStart();
            int holeXEnd = walls.get(0).getHoleXEnd();
            int holeYEnd = walls.get(0).getHoleYEnd();

            return !(playerX > holeXStart && playerX < holeXEnd && playerY > holeYStart && playerY < holeYEnd);
        }

        return false;
    }

    protected void wallOperations() {
        Wall toAdd = null;
        Wall toRemove = null;

        Collections.reverse(walls);
        for (Wall wall : walls) {
            if (wall != null) {
                if (wall.approach() <= 0.1) {
                    score++;
                    toRemove = walls.get(walls.size() - 1);
                    int[] newWallCoordinates = defineHoles();
                    toAdd = new Wall(newWallCoordinates[0], newWallCoordinates[1], newWallCoordinates[2], newWallCoordinates[3], 60, actualColorToGive[indexColorToGive % 20], gameFPS);
                    int color = (int) (128 * ((10 * (actualColorToGive[indexColorToGive % 20] + 1) + 25) / 100f));
                    indexColorToGive++;
                    scene.setFill(Color.rgb(color % 10, color % 10, color % 10, 1));
                } else {
                    gameGroup.getChildren().addAll(wall.display(gameScreenScale, gameScreenX, gameScreenY));
                }
            }
        }

        Collections.reverse(walls);

        if (toAdd != null) {
            walls.add(toAdd);
        }

        walls.remove(toRemove);
    }

    protected void mainGameLoop() {

        if (nitro < 100 && !playerShip.isNitroActivated()) {
            nitro = nitro + 10d / gameFPS;
        } else if (playerShip.isNitroActivated()) {
            nitro = nitro - 40d / gameFPS;
        }

        if (nitro > 100) {
            nitro = 100;
        }

        gameGroup.getChildren().clear();

        if (hasCollision()) {
            stopGame();
            MainClient.menuTransition.menuStart("looser", String.valueOf(score));
            return;
        }

        wallOperations();

        gameGroup.getChildren().addAll(genBounds());

        playerShip.move(gameFPS);

        gameGroup.getChildren().addAll(playerShip.display(gameScreenScale, gameScreenX, gameScreenY));
        double topLimit = (gameScreenScale / 10d) + (gameScreenX / 10d);
        double bottomLimit = gameScreenScale * 9 / 10d;
        double height = bottomLimit - topLimit;
        double yTop = (topLimit + height * (1 - (nitro / 100d)));

        gameGroup.getChildren().addAll(genNitroFill(topLimit, height));

        double xMin = gameScreenX * 4 / 10d;
        double xMax = gameScreenX * 6 / 10d;

        double bottomLimitGame = ((bottomLimit - gameScreenY) * (1000f / gameScreenScale)) - 500;
        double yTopGame = ((yTop - gameScreenY) * (1000f / gameScreenScale)) - 500;

        double xMinGame = ((xMin - gameScreenX) * (1000f / gameScreenScale)) - 500;
        double xMaxGame = ((xMax - gameScreenX) * (1000f / gameScreenScale)) - 500;

        if (!playerShip.isNitroActivated() && bubbleParticleCountdown > 1) {
            bubbleParticles.add(new NitroBubbleParticle(bottomLimitGame, yTopGame, xMinGame, xMaxGame));
            bubbleParticleCountdown = 0;
        } else if (!playerShip.isNitroActivated()) {
            bubbleParticleCountdown = bubbleParticleCountdown + 10d / gameFPS;
        }

        NitroBubbleParticle[] toRemoveNitroBubble = new NitroBubbleParticle[bubbleParticles.size()];

        for (int i = 0; i < bubbleParticles.size(); i++) {
            bubbleParticles.get(i).move(yTopGame, gameFPS);
            gameGroup.getChildren().add(bubbleParticles.get(i).display(gameScreenScale, gameScreenX, gameScreenY));
            if (bubbleParticles.get(i).disappear()) {
                toRemoveNitroBubble[i] = bubbleParticles.get(i);
            }
        }

        for (NitroBubbleParticle remove : toRemoveNitroBubble) {
            bubbleParticles.remove(remove);
        }

        if (noNitroParticle != null) {
            if (noNitroParticle.shade()) {
                noNitroParticle = null;
            } else {
                double errorXStart = gameScreenX * 4 / 10d;
                double errorYStart = gameScreenScale / 10d + gameScreenX / 10d;
                double errorWidth = gameScreenX * 2 / 10d;
                gameGroup.getChildren().add(noNitroParticle.display(errorXStart, errorYStart, errorWidth, height));
            }
        }

        mainLoopActive--;
    }

    protected Rectangle[] genBounds() {
        Rectangle[] rectangles = new Rectangle[3];

        rectangles[0] = new Rectangle(0, 0, gameScreenX, screenHeight);
        rectangles[1] = new Rectangle(gameScreenScale + gameScreenX, 0, gameScreenX, screenHeight);
        rectangles[2] = new Rectangle(0, screenHeight, screenWidth, 30);

        return rectangles;
    }

    protected Node[] genNitroFill(double topLimit, double height) {
        Rectangle[] rectangles = new Rectangle[5];

        Rectangle boundLeft = new Rectangle();
        boundLeft.setX(gameScreenX * 3 / 10d);
        boundLeft.setY(gameScreenScale / 10d);
        boundLeft.setWidth(gameScreenX / 10d);
        boundLeft.setHeight(gameScreenScale * 8 / 10d);
        boundLeft.setFill(Color.DIMGRAY);

        Rectangle boundDown = new Rectangle();
        boundDown.setX(gameScreenX * 3 / 10d);
        boundDown.setY(gameScreenScale * 9 / 10d);
        boundDown.setWidth(gameScreenX * 4 / 10d);
        boundDown.setHeight(gameScreenX / 10d);
        boundDown.setFill(Color.DIMGRAY);

        Rectangle boundRight = new Rectangle();
        boundRight.setX(gameScreenX * 6 / 10d);
        boundRight.setY(gameScreenScale / 10d);
        boundRight.setWidth(gameScreenX / 10d);
        boundRight.setHeight(gameScreenScale * 8 / 10d);
        boundRight.setFill(Color.DIMGRAY);

        Rectangle boundUp = new Rectangle();
        boundUp.setX(gameScreenX * 3 / 10d);
        boundUp.setY(gameScreenScale / 10d);
        boundUp.setWidth(gameScreenX * 4 / 10d);
        boundUp.setHeight(gameScreenX / 10d);
        boundUp.setFill(Color.DIMGRAY);

        Rectangle nitroLevel = new Rectangle();
        nitroLevel.setX(gameScreenX * 4 / 10d);
        nitroLevel.setY(topLimit + height * (1 - (nitro / 100d)));
        nitroLevel.setWidth(gameScreenX * 2 / 10d);
        nitroLevel.setHeight(height * (nitro / 100d));
        nitroLevel.setFill(Color.ORANGERED);

        rectangles[0] = boundLeft;
        rectangles[1] = boundDown;
        rectangles[2] = boundRight;
        rectangles[3] = boundUp;
        rectangles[4] = nitroLevel;

        return rectangles;
    }

    protected int[] defineHoles() {
        int xStart = (int) Random.random(-450, 300);
        int yStart = (int) Random.random(-450, 300);
        int xEnd = (int) Random.random(xStart + 150, 450);
        int yEnd = (int) Random.random(yStart + 150, 450);

        return new int[]{xStart, yStart, xEnd, yEnd};
    }
}