package org.tyranobast.spacedodger.client.game;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import org.tyranobast.spacedodger.client.game.effects.NewShipDirectionTarget;
import org.tyranobast.spacedodger.client.game.effects.ShipReactorParticle;

import java.util.ArrayList;

public class PlayerShip {

    private double x;
    private double y;
    private double xToGo;
    private double yToGo;
    private final ArrayList<NewShipDirectionTarget> shipDirectionTargets;
    private final ArrayList<ShipReactorParticle> shipReactorParticles;
    private final ArrayList<Projectile> projectiles;
    private double nitroCountDown;
    private final double shipScale;
    private double reactorParticleCountdown;

    public PlayerShip() {
        this.shipScale = 40;
        this.x = 0;
        this.y = 0;
        this.xToGo = 0;
        this.yToGo = 0;
        this.shipDirectionTargets = new ArrayList<>();
        this.shipReactorParticles = new ArrayList<>();
        this.projectiles = new ArrayList<>();
        this.nitroCountDown = -1;
        this.reactorParticleCountdown = 0;
    }

    public ArrayList<Projectile> getProjectiles() {
        return projectiles;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean isNitroActivated() {
        return nitroCountDown > -1;
    }

    public void activateNitro() {
        nitroCountDown = 30;
    }

    public void shoot() {
        projectiles.add(new Projectile(this.x, this.y, 1, "player"));
    }

    public void move(int gameFPS) {

        Projectile[] toRemoveProjectile = new Projectile[projectiles.size()];

        for (int i = 0; i < projectiles.size(); i++) {
            if (projectiles.get(i).approach(gameFPS) > 80) {
                toRemoveProjectile[i] = projectiles.get(i);
            }
        }

        for (Projectile remove : toRemoveProjectile) {
            projectiles.remove(remove);
        }

        double oneTurnFlyDistance;

        if (nitroCountDown > -1) {
            nitroCountDown = nitroCountDown - 10d / gameFPS;
            oneTurnFlyDistance = 30 * 10d / gameFPS;
        } else {
            oneTurnFlyDistance = 15 * 10d / gameFPS;
        }

        double distance = distance(this.x, this.y, this.xToGo, this.yToGo);

        if (distance < oneTurnFlyDistance) {
            this.xToGo = this.x;
            this.yToGo = this.y;
            distance = 0;
        }

        double partOfTheDistance;

        if (distance != 0) {
            partOfTheDistance = (oneTurnFlyDistance / distance);
        } else {
            partOfTheDistance = 0;
        }

        double distanceFromCenter = distance(this.x, this.y, 0, 0);

        double distanceFromExtremity = distance(this.x, this.y, this.x * 10, this.y * 10);

        double length = distanceFromCenter / 5;

        double offsetPart;

        if (distanceFromExtremity > 1) {
            offsetPart = (length / distanceFromExtremity);
        } else {
            offsetPart = 0;
        }

        double xCenter = offsetPart * this.x * 10 + ((1 - offsetPart) * this.x);
        double yCenter = offsetPart * this.y * 10 + ((1 - offsetPart) * this.y);

        reactorParticleCountdown = reactorParticleCountdown + 10d / gameFPS;

        if (reactorParticleCountdown > 1) {
            shipReactorParticles.add(new ShipReactorParticle(xCenter, yCenter, xCenter * 2, yCenter * 2));
            shipReactorParticles.add(new ShipReactorParticle(xCenter, yCenter, xCenter * 2, yCenter * 2));
            shipReactorParticles.add(new ShipReactorParticle(xCenter, yCenter, xCenter * 2, yCenter * 2));
            reactorParticleCountdown = 0;
        }

        double possibleX = partOfTheDistance * this.xToGo + ((1 - partOfTheDistance) * this.x);
        double possibleY = partOfTheDistance * this.yToGo + ((1 - partOfTheDistance) * this.y);


        if (possibleX < 500 && possibleX > -500) {
            this.x = possibleX;
        }
        if (possibleY < 500 && possibleY > -500) {
            this.y = possibleY;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////

        ShipReactorParticle[] toRemoveReactor = new ShipReactorParticle[shipReactorParticles.size()];

        for (int i = 0; i < shipReactorParticles.size(); i++) {
            shipReactorParticles.get(i).move(gameFPS);
            if (shipReactorParticles.get(i).shade(gameFPS)) {
                toRemoveReactor[i] = shipReactorParticles.get(i);
            }
        }

        for (ShipReactorParticle remove : toRemoveReactor) {
            shipReactorParticles.remove(remove);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////

        NewShipDirectionTarget[] toRemove = new NewShipDirectionTarget[shipDirectionTargets.size()];

        for (int i = 0; i < shipDirectionTargets.size(); i++) {
            if (shipDirectionTargets.get(i).shade(gameFPS)) {
                toRemove[i] = shipDirectionTargets.get(i);
            }
        }

        for (NewShipDirectionTarget remove : toRemove) {
            shipDirectionTargets.remove(remove);
        }
    }

    private double distance(double xFrom, double yFrom, double xTo, double yTo) {
        return Math.sqrt(Math.pow((xTo - xFrom), 2) + Math.pow((yTo - yFrom), 2));
    }

    public void setDirection(double x1, double y1, int gameScreenScale, int gameScreenX, int gameScreenY) {
        this.xToGo = ((x1 - gameScreenX) * (1000f / gameScreenScale)) - 500;
        this.yToGo = ((y1 - gameScreenY) * (1000f / gameScreenScale)) - 500;

        shipDirectionTargets.add(new NewShipDirectionTarget(xToGo, yToGo));
    }

    public void setMouseDirection(double x1, double y1, int gameScreenScale, int gameScreenX, int gameScreenY) {
        this.xToGo = ((x1 - gameScreenX) * (1000f / gameScreenScale)) - 500;
        this.yToGo = ((y1 - gameScreenY) * (1000f / gameScreenScale)) - 500;
    }

    public Node[] display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        double moveX = gameScreenX + (double) gameScreenScale / 1000 * (this.x + 500);
        double moveY = gameScreenY + (double) gameScreenScale / 1000 * (this.y + 500);

        int shipNodeCount = 6;

        Node[] nodes = new Node[shipDirectionTargets.size() + shipReactorParticles.size() + projectiles.size() + shipNodeCount];

        double distance = distance(this.x, this.y, this.x * 10, this.y * 10);

        double distanceFromCenter = distance(this.x, this.y, 0, 0);

        double length = distanceFromCenter / 5;

        double offsetPart;
        if (distance > 1) {
            offsetPart = (length / distance);
        } else {
            offsetPart = 0;
        }

        double xCenter = offsetPart * this.x * 10 + ((1 - offsetPart) * this.x);
        double yCenter = offsetPart * this.y * 10 + ((1 - offsetPart) * this.y);

        xCenter = gameScreenX + (double) gameScreenScale / 1000 * (xCenter + 500);
        yCenter = gameScreenY + (double) gameScreenScale / 1000 * (yCenter + 500);

        double shipScaleScreen = (gameScreenScale / 1000d) * this.shipScale;
        double cockpitScale = shipScaleScreen / 3.5d;

        Polygon horizontalLeft = new Polygon();
        horizontalLeft.getPoints().addAll(
                xCenter - shipScaleScreen, yCenter,
                xCenter - cockpitScale, yCenter,
                moveX, moveY
        );
        horizontalLeft.setFill(Color.DARKSLATEGRAY);

        Polygon horizontalRight = new Polygon();
        horizontalRight.getPoints().addAll(
                xCenter + shipScaleScreen, yCenter,
                xCenter + cockpitScale, yCenter,
                moveX, moveY
        );
        horizontalRight.setFill(Color.DARKSLATEGRAY);

        Polygon verticalUp = new Polygon();
        verticalUp.getPoints().addAll(
                xCenter, yCenter - shipScaleScreen,
                xCenter, yCenter - cockpitScale,
                moveX, moveY
        );
        verticalUp.setFill(Color.DARKGRAY);

        Polygon verticalDown = new Polygon();
        verticalDown.getPoints().addAll(
                xCenter, yCenter + shipScaleScreen,
                xCenter, yCenter + cockpitScale,
                moveX, moveY
        );

        verticalDown.setFill(Color.DARKGRAY);

        //getting tangents of cockpit circle by (xCenter, yCenter)
        double b = Math.sqrt(Math.pow(moveX - xCenter, 2) + Math.pow(moveY - yCenter, 2));

        double th = Math.acos(cockpitScale / b);
        double d = Math.atan2(moveY - yCenter, moveX - xCenter);
        double d1 = d + th;
        double d2 = d - th;

        double xTriangleBase1 = xCenter + cockpitScale * Math.cos(d1);
        double yTriangleBase1 = yCenter + cockpitScale * Math.sin(d1);
        double xTriangleBase2 = xCenter + cockpitScale * Math.cos(d2);
        double yTriangleBase2 = yCenter + cockpitScale * Math.sin(d2);

        Polygon fuselage = new Polygon();
        fuselage.getPoints().addAll(
                        xTriangleBase1, yTriangleBase1,
                        xTriangleBase2, yTriangleBase2,
                        moveX, moveY
                );
        fuselage.setFill(Color.DIMGRAY);

        nodes[2] = fuselage;

        if (distance > 20) {
            if (moveX > xCenter && moveY > yCenter) {
                nodes[0] = horizontalLeft;
                nodes[1] = verticalUp;
                nodes[3] = horizontalRight;
                nodes[4] = verticalDown;
            } else if (moveX > xCenter && moveY < yCenter) {
                nodes[0] = horizontalLeft;
                nodes[1] = verticalDown;
                nodes[3] = horizontalRight;
                nodes[4] = verticalUp;
            } else if (moveX < xCenter && moveY > yCenter) {
                nodes[0] = horizontalRight;
                nodes[1] = verticalUp;
                nodes[3] = horizontalLeft;
                nodes[4] = verticalDown;
            } else if (moveX < xCenter && moveY < yCenter) {
                nodes[0] = horizontalRight;
                nodes[1] = verticalDown;
                nodes[3] = horizontalLeft;
                nodes[4] = verticalUp;
            }
        } else {
            nodes[0] = new Line(xCenter, yCenter, xCenter + shipScaleScreen, yCenter);
            nodes[1] = new Line(xCenter, yCenter, xCenter - shipScaleScreen, yCenter);
            nodes[3] = new Line(xCenter, yCenter, xCenter, yCenter + shipScaleScreen);
            nodes[4] = new Line(xCenter, yCenter, xCenter, yCenter - shipScaleScreen);
        }

        Circle circle = new Circle();
        circle.setRadius(cockpitScale);
        circle.setCenterX(xCenter);
        circle.setCenterY(yCenter);
        circle.setFill(Color.DIMGRAY);

        nodes[5] = circle;

        int printIteration = shipNodeCount;

        for (NewShipDirectionTarget toAdd : shipDirectionTargets) {
            nodes[printIteration] = toAdd.display(gameScreenScale, gameScreenX, gameScreenY);
            printIteration++;
        }

        for (ShipReactorParticle toAdd : shipReactorParticles) {
            nodes[printIteration] = toAdd.display(gameScreenScale, gameScreenX, gameScreenY);
            printIteration++;
        }

        for (Projectile toAdd : projectiles) {
            nodes[printIteration] = toAdd.display(gameScreenScale, gameScreenX, gameScreenY);
            printIteration++;
        }

        return nodes;
    }

    public void removeProjectiles() {
        projectiles.clear();
    }

    public double getShipScale() {
        return shipScale;
    }
}
