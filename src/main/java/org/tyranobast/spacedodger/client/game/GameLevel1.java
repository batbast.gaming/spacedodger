package org.tyranobast.spacedodger.client.game;

import javafx.scene.Group;

public class GameLevel1 extends Game {
    public GameLevel1(Group gameGroup, int gameScreenScale, int gameScreenX, int gameScreenY, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth);
    }
}
