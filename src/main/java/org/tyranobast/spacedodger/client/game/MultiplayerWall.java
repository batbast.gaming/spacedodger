package org.tyranobast.spacedodger.client.game;

class MultiplayerWall extends Wall {

    public MultiplayerWall(int x1, int y1, int x2, int y2, int distance, int color, int gameFPS) {
        super(x1, y1, x2, y2, distance, color, gameFPS);
    }

    public double approach(double playerSpeed) {

        distance = distance - ((10d / gameFPS) / 2) * playerSpeed;

        return distance;

    }
}
