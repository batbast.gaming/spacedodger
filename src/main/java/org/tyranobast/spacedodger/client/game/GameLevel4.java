package org.tyranobast.spacedodger.client.game;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.Transmission;
import org.tyranobast.spacedodger.client.game.effects.NitroBubbleParticle;
import org.tyranobast.spacedodger.client.game.effects.NoNitroParticle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class GameLevel4 extends Game {

    private Timeline networkTimeline;
    private final ArrayList<MultiplayerWall> multiplayerWalls;
    private double playerSpeed;
    private double z;
    private EnemyPlayerShip[] enemyPlayerShips;
    private final ArrayList<int[]> predefinedWallHoles;
    private int maxDistance;
    private EventHandler<ScrollEvent> MOUSEScrollEventHandler;

    public GameLevel4(Group gameGroup, int gameScreenScale, int gameScreenX, int gameScreenY, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth);

        gameTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / gameFPS), event -> {
            if (mainLoopActive == 0) {
                mainLoopActive++;
                mainGameLoop();
            }
        }));

        int network_rate = MainClient.config.getIntValue("network_rate");

        predefinedWallHoles = new ArrayList<>();

        this.multiplayerWalls = new ArrayList<>();

        this.playerSpeed = 1;
        this.z = 0;

        networkTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / network_rate), event -> mainNetworkLoop()));

        networkTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    @Override
    public void reloadConfig() {
        String oldControlType = this.controlType;
        this.controlType = MainClient.config.getStringValue("control");
        this.gameFPS = MainClient.config.getIntValue("game_fps");
        int network_rate = MainClient.config.getIntValue("network_rate");

        networkTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / network_rate), event -> mainNetworkLoop()));

        networkTimeline.setCycleCount(Timeline.INDEFINITE);

        gameTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / gameFPS), event -> {
            if (mainLoopActive == 0) {
                mainLoopActive++;
                mainGameLoop();
            }
        }));

        gameTimeline.setCycleCount(Timeline.INDEFINITE);

        if (!oldControlType.equals(controlType) || !eventInitialized) {
            if (this.controlType.equals("click")) {
                if (!eventInitialized) {
                    scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, MOUSEClickEventHandler);
                    scene.removeEventHandler(MouseEvent.MOUSE_MOVED, MOUSEMoveEventHandler);
                    scene.removeEventHandler(ScrollEvent.SCROLL, MOUSEScrollEventHandler);
                    eventInitialized = true;
                }
                scene.addEventHandler(KeyEvent.KEY_PRESSED, CLICKButtonEventHandler);
                scene.addEventHandler(MouseEvent.MOUSE_PRESSED, CLICKMouseEventHandler);
            } else if (this.controlType.equals("mouse")) {
                if (!eventInitialized) {
                    scene.removeEventHandler(KeyEvent.KEY_PRESSED, CLICKButtonEventHandler);
                    scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, CLICKMouseEventHandler);
                    scene.addEventHandler(MouseEvent.MOUSE_PRESSED, MOUSEClickEventHandler);
                    eventInitialized = true;
                }
                scene.addEventHandler(MouseEvent.MOUSE_PRESSED, MOUSEClickEventHandler);
                scene.addEventHandler(MouseEvent.MOUSE_MOVED, MOUSEMoveEventHandler);
            }
        }
    }

    @Override
    protected void initEvent() {
        CLICKButtonEventHandler = event -> {
            if (event.getCode() == KeyCode.N) {
                if (nitro == 100 && !playerShip.isNitroActivated()) {
                    playerShip.activateNitro();
                    bubbleParticleCountdown = 0;
                    bubbleParticles.clear();
                } else {
                    noNitroParticle = new NoNitroParticle();
                }
            } else if (event.getCode() == KeyCode.UP) {
                playerSpeed = playerSpeed + 0.1;
                if (playerSpeed > 2.5) {
                    playerSpeed = 2.5;
                }
            } else if (event.getCode() == KeyCode.DOWN) {
                playerSpeed = playerSpeed - 0.1;
                if (playerSpeed < 0.5) {
                    playerSpeed = 0.5;
                }
            }
        };

        CLICKMouseEventHandler = event ->
            playerShip.setDirection((int) event.getX(), (int) event.getY(), gameScreenScale, gameScreenX, gameScreenY);

        MOUSEClickEventHandler = event -> {
            if (event.isSecondaryButtonDown()) {
                if (nitro == 100 && !playerShip.isNitroActivated()) {
                    playerShip.activateNitro();
                    bubbleParticleCountdown = 0;
                    bubbleParticles.clear();
                } else {
                    noNitroParticle = new NoNitroParticle();
                }

            }
        };

        MOUSEMoveEventHandler = event ->
            playerShip.setMouseDirection((int) event.getX(), (int) event.getY(), gameScreenScale, gameScreenX, gameScreenY);

        MOUSEScrollEventHandler = event -> {
            if (event.getDeltaY() > 0) {
                playerSpeed = playerSpeed + 0.1;
                if (playerSpeed > 2.5) {
                    playerSpeed = 2.5;
                }
            } else if (event.getDeltaY() < 0) {
                playerSpeed = playerSpeed - 0.1;
                if (playerSpeed < 0.5) {
                    playerSpeed = 0.5;
                }
            }
        };
    }

    private void mainNetworkLoop() {
        MainClient.networkerClient.sendMessage(MainClient.onlineToken + " " + playerShip.getX() + " " + playerShip.getY() + " " + this.z, "coordinate");

        String messageDead = MainClient.networkerClient.getMessage(Transmission.Util, (String data) ->
                data.split(" ")[0].equals("dead"));

        if (!Objects.equals(messageDead, "")) {
            for (EnemyPlayerShip enemyPlayerShip : enemyPlayerShips) {
                if (enemyPlayerShip.getUsername().equals(messageDead.split(" ")[1])) {
                    enemyPlayerShip.die();
                }
            }
        }

        String messageWin = MainClient.networkerClient.getMessage(Transmission.Util, (String data) ->
                data.split(" ")[0].equals("winner"));
        if (!messageWin.equals("")) {
            stopGame();
            if (messageWin.split(" ")[1].equals(MainClient.onlineUsername)) {
                MainClient.menuTransition.menuStart("win");
            } else {
                MainClient.menuTransition.menuStart("gameEnd", messageWin.split(" ")[1]);
            }
        } else if (this.z > maxDistance) {
            MainClient.networkerClient.sendMessage("win " + MainClient.onlineToken, "util");
            stopGame();
            MainClient.menuTransition.menuStart("win");
        }
    }

    @Override
    protected void initWalls() {
        multiplayerWalls.clear();

        indexColorToGive = 1;
        multiplayerWalls.add(new MultiplayerWall(-450, -450, 450, 450, 15, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        multiplayerWalls.add(new MultiplayerWall(-450, -450, 450, 450, 30, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        multiplayerWalls.add(new MultiplayerWall(-450, -450, 450, 450, 45, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
        multiplayerWalls.add(new MultiplayerWall(-450, -450, 450, 450, 60, actualColorToGive[indexColorToGive % 20], gameFPS));
        indexColorToGive++;
    }

    private void initMultiplayer() {
        String gameInitData = MainClient.multiplayerHubMenu.getGameInitData();
        String playerUUIDList = gameInitData.split("&")[0];

        playerUUIDList = playerUUIDList.replace(MainClient.onlineUsername + " ", "");

        this.enemyPlayerShips = new EnemyPlayerShip[playerUUIDList.split(" ").length];

        for (int i = 0; i < playerUUIDList.split(" ").length; i++) {
            enemyPlayerShips[i] = new EnemyPlayerShip(playerUUIDList.split(" ")[i], gameFPS);
        }

        maxDistance = Integer.parseInt(gameInitData.split("&")[1]);

        String[] wallHoles = gameInitData.split("&")[2].split(" ");

        for (int i = 0; i < wallHoles.length; ) {
            int[] toAdd = new int[4];
            toAdd[0] = Integer.parseInt(wallHoles[i]);
            i++;
            toAdd[1] = Integer.parseInt(wallHoles[i]);
            i++;
            toAdd[2] = Integer.parseInt(wallHoles[i]);
            i++;
            toAdd[3] = Integer.parseInt(wallHoles[i]);
            i++;
            predefinedWallHoles.add(toAdd);
        }

        networkTimeline.play();

    }

    @Override
    protected boolean hasCollision() {
        double playerX = playerShip.getX();
        double playerY = playerShip.getY();
        int holeXStart = multiplayerWalls.get(0).getHoleXStart();
        int holeYStart = multiplayerWalls.get(0).getHoleYStart();
        int holeXEnd = multiplayerWalls.get(0).getHoleXEnd();
        int holeYEnd = multiplayerWalls.get(0).getHoleYEnd();

        if (multiplayerWalls.get(0).getDistance() <= 1) {
            return !(playerX > holeXStart && playerX < holeXEnd && playerY > holeYStart && playerY < holeYEnd);
        }

        return false;
    }

    @Override
    protected void wallOperations() {

        String[][] elementsToSort = new String[multiplayerWalls.size() + enemyPlayerShips.length][2];

        for (int i = 0; i < multiplayerWalls.size(); i++) {
            elementsToSort[i][0] = "wall" + i;
            elementsToSort[i][1] = String.valueOf(multiplayerWalls.get(i).getDistance());
        }

        for (int i = 0; i < enemyPlayerShips.length; i++) {
            elementsToSort[multiplayerWalls.size() + i][0] = "ship" + i;
            elementsToSort[multiplayerWalls.size() + i][1] = String.valueOf(enemyPlayerShips[i].getDistance());
        }

        Arrays.sort(elementsToSort, (entry1, entry2) -> {
            final Double value1 = Double.parseDouble(entry1[1]);
            final Double value2 = Double.parseDouble(entry2[1]);
            return value1.compareTo(value2);
        });

        String[][] elementsSorted = new String[elementsToSort.length][elementsToSort[0].length];
        for (int i = 0; i < elementsToSort.length; i++) {
            elementsSorted[(elementsToSort.length - 1) - i] = elementsToSort[i];
        }

        MultiplayerWall toAdd = null;
        MultiplayerWall toRemove = null;

        for (String[] element : elementsSorted) {
            if (element[0].contains("wall")) {
                int i = Integer.parseInt(element[0].replace("wall", ""));
                MultiplayerWall multiplayerWall = multiplayerWalls.get(i);
                if (multiplayerWall != null) {
                    if (multiplayerWall.approach(playerSpeed) <= 0.1) {
                        score++;
                        toRemove = multiplayerWall;
                        int[] newWallCoordinates = defineHoles();
                        toAdd = new MultiplayerWall(newWallCoordinates[0], newWallCoordinates[1], newWallCoordinates[2], newWallCoordinates[3], 60, actualColorToGive[indexColorToGive % 20], gameFPS);
                        int color = (int) (128 * ((10 * (actualColorToGive[indexColorToGive % 20] + 1) + 25) / 100f));
                        indexColorToGive++;
                        scene.setFill(Color.rgb(color % 10, color % 10, color % 10, 1));
                    } else {
                        gameGroup.getChildren().addAll(multiplayerWall.display(gameScreenScale, gameScreenX, gameScreenY));
                    }
                }
            } else if (element[0].contains("ship")) {
                int i = Integer.parseInt(element[0].replace("ship", ""));
                gameGroup.getChildren().addAll(enemyPlayerShips[i].display(gameScreenScale, gameScreenX, gameScreenY));
            }
        }

        if (toAdd != null) {
            multiplayerWalls.add(toAdd);
        }

        multiplayerWalls.remove(toRemove);
    }

    @Override
    protected void mainGameLoop() {

        this.z = this.z + ((10d / gameFPS) / 2) * playerSpeed;

        String coordinates = MainClient.networkerClient.getMessage(Transmission.Coordinate);

        if (!coordinates.equals("")) {
            String[] lastCoordinatesReceivedRaw = coordinates.split("£");

            ArrayList<String> lastCoordinatesReceived = new ArrayList<>(lastCoordinatesReceivedRaw.length);
            lastCoordinatesReceived.addAll(List.of(lastCoordinatesReceivedRaw));

            lastCoordinatesReceived.removeIf(n -> (n.contains(MainClient.onlineUsername)));

            for (int i = 0; i < lastCoordinatesReceived.size(); i++) {
                for (int j = 0; j < enemyPlayerShips.length; j++) {
                    String[] data = lastCoordinatesReceived.get(i).split(" ");
                    if (enemyPlayerShips[i].getUsername().equals(data[0])) {
                        enemyPlayerShips[i].move(Double.parseDouble(data[1]), Double.parseDouble(data[2]), Double.parseDouble(data[3]), this.z);
                        break;
                    }
                }
            }
        }

        if (nitro < 100 && !playerShip.isNitroActivated()) {
            nitro = nitro + 10d / gameFPS;
        } else if (playerShip.isNitroActivated()) {
            nitro = nitro - 40d / gameFPS;
        }

        if (nitro > 100) {
            nitro = 100;
        }

        gameGroup.getChildren().clear();

        if (hasCollision()) {
            MainClient.networkerClient.sendMessage("loose " + MainClient.onlineToken, "util");
            stopGame();
            MainClient.menuTransition.menuStart("looser", String.valueOf(score));
            return;
        }

        wallOperations();

        gameGroup.getChildren().addAll(genBounds());

        playerShip.move(gameFPS);

        gameGroup.getChildren().addAll(playerShip.display(gameScreenScale, gameScreenX, gameScreenY));
        double topLimit = (gameScreenScale / 10d) + (gameScreenX / 10d);
        double bottomLimit = gameScreenScale * 9 / 10d;
        double height = bottomLimit - topLimit;
        double yTop = (topLimit + height * (1 - (nitro / 100d)));

        gameGroup.getChildren().addAll(genNitroFill(topLimit, height));
        gameGroup.getChildren().addAll(genSpeedIndicator(topLimit, height));

        double xMin = gameScreenX * 4 / 10d;
        double xMax = gameScreenX * 6 / 10d;

        double bottomLimitGame = ((bottomLimit - gameScreenY) * (1000f / gameScreenScale)) - 500;
        double yTopGame = ((yTop - gameScreenY) * (1000f / gameScreenScale)) - 500;

        double xMinGame = ((xMin - gameScreenX) * (1000f / gameScreenScale)) - 500;
        double xMaxGame = ((xMax - gameScreenX) * (1000f / gameScreenScale)) - 500;

        if (!playerShip.isNitroActivated() && bubbleParticleCountdown > 1) {
            bubbleParticles.add(new NitroBubbleParticle(bottomLimitGame, yTopGame, xMinGame, xMaxGame));
            bubbleParticleCountdown = 0;
        } else if (!playerShip.isNitroActivated()) {
            bubbleParticleCountdown = bubbleParticleCountdown + 10d / gameFPS;
        }

        NitroBubbleParticle[] toRemoveNitroBubble = new NitroBubbleParticle[bubbleParticles.size()];

        for (int i = 0; i < bubbleParticles.size(); i++) {
            bubbleParticles.get(i).move(yTopGame, gameFPS);
            gameGroup.getChildren().add(bubbleParticles.get(i).display(gameScreenScale, gameScreenX, gameScreenY));
            if (bubbleParticles.get(i).disappear()) {
                toRemoveNitroBubble[i] = bubbleParticles.get(i);
            }
        }

        for (NitroBubbleParticle remove : toRemoveNitroBubble) {
            bubbleParticles.remove(remove);
        }

        if (noNitroParticle != null) {
            if (noNitroParticle.shade()) {
                noNitroParticle = null;
            } else {
                double errorXStart = gameScreenX * 4 / 10d;
                double errorYStart = gameScreenScale / 10d + gameScreenX / 10d;
                double errorWidth = gameScreenX * 2 / 10d;
                gameGroup.getChildren().add(noNitroParticle.display(errorXStart, errorYStart, errorWidth, height));
            }
        }

        mainLoopActive--;
    }

    private Node[] genSpeedIndicator(double topLimit, double height) {
        Rectangle[] rectangles = new Rectangle[5];

        Rectangle boundLeft = new Rectangle();
        boundLeft.setX(gameScreenX + gameScreenScale + gameScreenX * 3 / 10d);
        boundLeft.setY(gameScreenScale / 10d);
        boundLeft.setWidth(gameScreenX / 10d);
        boundLeft.setHeight(gameScreenScale * 8 / 10d);
        boundLeft.setFill(Color.DIMGRAY);

        Rectangle boundDown = new Rectangle();
        boundDown.setX(gameScreenX + gameScreenScale + gameScreenX * 3 / 10d);
        boundDown.setY(gameScreenScale * 9 / 10d);
        boundDown.setWidth(gameScreenX * 4 / 10d);
        boundDown.setHeight(gameScreenX / 10d);
        boundDown.setFill(Color.DIMGRAY);

        Rectangle boundRight = new Rectangle();
        boundRight.setX(gameScreenX + gameScreenScale + gameScreenX * 6 / 10d);
        boundRight.setY(gameScreenScale / 10d);
        boundRight.setWidth(gameScreenX / 10d);
        boundRight.setHeight(gameScreenScale * 8 / 10d);
        boundRight.setFill(Color.DIMGRAY);

        Rectangle boundUp = new Rectangle();
        boundUp.setX(gameScreenX + gameScreenScale + gameScreenX * 3 / 10d);
        boundUp.setY(gameScreenScale / 10d);
        boundUp.setWidth(gameScreenX * 4 / 10d);
        boundUp.setHeight(gameScreenX / 10d);
        boundUp.setFill(Color.DIMGRAY);

        double speed = ((playerSpeed - 0.5) / 2) * 100;

        Rectangle speedLevel = new Rectangle();
        speedLevel.setX(gameScreenX + gameScreenScale + gameScreenX * 4 / 10d);
        speedLevel.setY(topLimit + height * (1 - (speed / 100d)));
        speedLevel.setWidth(gameScreenX * 2 / 10d);
        speedLevel.setHeight(height * (speed / 100d));
        speedLevel.setFill(Color.LIGHTSKYBLUE);

        rectangles[0] = boundLeft;
        rectangles[1] = boundDown;
        rectangles[2] = boundRight;
        rectangles[3] = boundUp;
        rectangles[4] = speedLevel;

        return rectangles;
    }

    @Override
    protected int[] defineHoles() {
        if (predefinedWallHoles.size() > 0) {
            int[] holes = predefinedWallHoles.get(0);
            predefinedWallHoles.remove(0);
            return holes;
        } else {
            return new int[]{450, 450, -450, -450};
        }
    }

    @Override
    public void startGame() {
        reloadConfig();
        this.z = 0;
        initGame();
        initMultiplayer();
        gameTimeline.play();
    }

    @Override
    protected void stopGame() {
        scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, MOUSEClickEventHandler);
        scene.removeEventHandler(MouseEvent.MOUSE_MOVED, MOUSEMoveEventHandler);
        scene.removeEventHandler(ScrollEvent.SCROLL, MOUSEScrollEventHandler);
        scene.removeEventHandler(KeyEvent.KEY_PRESSED, CLICKButtonEventHandler);
        scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, CLICKMouseEventHandler);
        gameTimeline.stop();
        networkTimeline.stop();
    }
}
