package org.tyranobast.spacedodger.client.game;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import org.tyranobast.spacedodger.client.game.effects.ExplosionParticle;
import org.tyranobast.spacedodger.client.Random;

import java.util.ArrayList;

public class EnemyWall extends Wall {

    private final int shipScale;
    private double x;
    private double y;
    private final ArrayList<Projectile> projectiles;
    private final ArrayList<ExplosionParticle> explosionParticles;
    private double xTo;
    private double yTo;
    private final PlayerShip playerShip;
    private double shotCountdown;
    private final boolean canShoot;
    private boolean isDead;

    public EnemyWall(int distance, int color, int gameFPS, PlayerShip playerShip, boolean canShoot) {

        super(-450, -450, 450, 450, distance, color, gameFPS);

        this.isDead = false;

        this.canShoot = canShoot;

        this.shotCountdown = 10;

        this.playerShip = playerShip;

        this.projectiles = new ArrayList<>();

        this.explosionParticles = new ArrayList<>();

        this.shipScale = 60;

        this.x = Random.random(-300, 300);
        this.y = Random.random(-300, 300);

        targetPlayer();
    }

    public int getShipScale() {
        return this.shipScale;
    }

    public boolean isDead() {
        return isDead;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    private double distance(double xFrom, double yFrom, double xTo, double yTo) {
        return Math.sqrt(Math.pow((xTo - xFrom), 2) + Math.pow((yTo - yFrom), 2));
    }

    private void targetPlayer() {
        this.xTo = playerShip.getX();
        this.yTo = playerShip.getY();
    }

    public void shot() {
        if (canShoot && !isDead) {
            shotCountdown = shotCountdown - 100d / gameFPS;

            if (shotCountdown <= 0) {
                shotCountdown = 10;
                projectiles.add(new Projectile(this.x, this.y, this.distance, "enemy"));
            }
        }
    }

    public ArrayList<Projectile> getProjectiles() {
        return projectiles;
    }

    public void die() {
        isDead = true;
        for (int i = 0; i < 10; i++) {
            explosionParticles.add(new ExplosionParticle(this.x, this.y, this.x + Random.random(-200,200), this.y + Random.random(-200,200), this.distance));
        }
    }

    public void move() {

        Projectile[] toRemoveProjectile = new Projectile[projectiles.size()];

        for (int i = 0; i < projectiles.size(); i++) {
            if (projectiles.get(i).approach(gameFPS) < 0.1d) {
                toRemoveProjectile[i] = projectiles.get(i);
            }
        }

        for (Projectile remove : toRemoveProjectile) {
            projectiles.remove(remove);
        }

        targetPlayer();

        double oneTurnFlyDistance = 5 * 10d / gameFPS;

        double distance = distance(this.x, this.y, this.xTo, this.yTo);

        if (distance < oneTurnFlyDistance) {
            distance = 0;
        }

        double partOfTheDistance;

        if (distance != 0) {
            partOfTheDistance = (oneTurnFlyDistance / distance);
        } else {
            partOfTheDistance = 0;
        }

        this.x = partOfTheDistance * this.xTo + ((1 - partOfTheDistance) * this.x);
        this.y = partOfTheDistance * this.yTo + ((1 - partOfTheDistance) * this.y);

        ExplosionParticle[] toRemoveReactor = new ExplosionParticle[explosionParticles.size()];

        for (int i = 0; i < explosionParticles.size(); i++) {
            explosionParticles.get(i).move(gameFPS);
            if (explosionParticles.get(i).shade(gameFPS)) {
                toRemoveReactor[i] = explosionParticles.get(i);
            }
        }

        for (ExplosionParticle remove : toRemoveReactor) {
            explosionParticles.remove(remove);
        }
    }

    public Node[] display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        int xStart = (int) (holeXStart / distance + 500);
        int yStart = (int) (holeYStart / distance + 500);
        int xEnd = (int) (holeXEnd / distance + 500);
        int yEnd = (int) (holeYEnd / distance + 500);

        int xHoleStartScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xStart);
        int yHoleStartScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yStart);
        int xHoleEndScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xEnd);
        int yHoleEndScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yEnd);

        int xStartWall = (int) (-500 / distance + 500);
        int yStartWall = (int) (-500 / distance + 500);
        int xEndWall = (int) (500 / distance + 500);
        int yEndWall = (int) (500 / distance + 500);

        int xWallStartScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xStartWall);
        int yWallStartScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yStartWall);
        int xWallEndScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xEndWall);
        int yWallEndScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yEndWall);

        int xStartObstacleGame = (int) (this.x / distance + 500);
        int yStartObstacleGame = (int) (this.y / distance + 500);
        int obstacleSizeGame = (int) (this.shipScale / distance);

        int xStartObstacleScreen = (int) (gameScreenX + gameScreenScale / 1000d * xStartObstacleGame);
        int yStartObstacleScreen = (int) (gameScreenY + gameScreenScale / 1000d * yStartObstacleGame);
        int obstacleSizeScreen = (int) (gameScreenScale / 1000d * obstacleSizeGame);

        if (xHoleStartScreen < gameScreenX) {
            xHoleStartScreen = gameScreenX;
        }
        if (yHoleStartScreen < gameScreenY) {
            yHoleStartScreen = gameScreenY;
        }
        if (xHoleEndScreen > gameScreenX + gameScreenScale) {
            xHoleEndScreen = gameScreenX + gameScreenScale;
        }

        if (yHoleEndScreen > gameScreenY + gameScreenScale) {
            yHoleEndScreen = gameScreenY + gameScreenScale;
        }

        Rectangle[] rectangle = new Rectangle[4];

        rectangle[0] = new Rectangle();
        rectangle[0].setX(gameScreenX);
        rectangle[0].setY(gameScreenY);
        rectangle[0].setWidth(gameScreenScale);
        rectangle[0].setHeight(yHoleStartScreen);
        rectangle[0].setFill(Color.rgb(color, color, color, 1));

        rectangle[1] = new Rectangle();
        rectangle[1].setX(xHoleEndScreen);
        rectangle[1].setY(gameScreenY);
        rectangle[1].setWidth(gameScreenX + gameScreenScale - xHoleEndScreen);
        rectangle[1].setHeight(gameScreenScale);
        rectangle[1].setFill(Color.rgb(color, color, color, 1));

        rectangle[2] = new Rectangle();
        rectangle[2].setX(gameScreenX);
        rectangle[2].setY(yHoleEndScreen);
        rectangle[2].setWidth(gameScreenScale);
        rectangle[2].setHeight(gameScreenScale - yHoleEndScreen);
        rectangle[2].setFill(Color.rgb(color, color, color, 1));

        rectangle[3] = new Rectangle();
        rectangle[3].setX(gameScreenX);
        rectangle[3].setY(gameScreenY);
        rectangle[3].setWidth(xHoleStartScreen - gameScreenX);
        rectangle[3].setHeight(gameScreenScale);
        rectangle[3].setFill(Color.rgb(color, color, color, 1));

        Line[] line = new Line[12];

        line[0] = new Line();
        line[0].setStartX(gameScreenX);
        line[0].setStartY(gameScreenY);
        line[0].setEndX(xWallStartScreen);
        line[0].setEndY(yWallStartScreen);

        line[1] = new Line();
        line[1].setStartX(gameScreenX + gameScreenScale);
        line[1].setStartY(gameScreenY);
        line[1].setEndX(xWallEndScreen);
        line[1].setEndY(yWallStartScreen);

        line[2] = new Line();
        line[2].setStartX(gameScreenX + gameScreenScale);
        line[2].setStartY(gameScreenY + gameScreenScale);
        line[2].setEndX(xWallEndScreen);
        line[2].setEndY(yWallEndScreen);

        line[3] = new Line();
        line[3].setStartX(gameScreenX);
        line[3].setStartY(gameScreenY + gameScreenScale);
        line[3].setEndX(xWallStartScreen);
        line[3].setEndY(yWallEndScreen);

        line[4] = new Line();
        line[4].setStartX(xWallStartScreen);
        line[4].setStartY(yWallStartScreen);
        line[4].setEndX(xWallStartScreen);
        line[4].setEndY(yWallEndScreen);

        line[5] = new Line();
        line[5].setStartX(xWallStartScreen);
        line[5].setStartY(yWallEndScreen);
        line[5].setEndX(xWallEndScreen);
        line[5].setEndY(yWallEndScreen);

        line[6] = new Line();
        line[6].setStartX(xWallEndScreen);
        line[6].setStartY(yWallEndScreen);
        line[6].setEndX(xWallEndScreen);
        line[6].setEndY(yWallStartScreen);

        line[7] = new Line();
        line[7].setStartX(xWallEndScreen);
        line[7].setStartY(yWallStartScreen);
        line[7].setEndX(xWallStartScreen);
        line[7].setEndY(yWallStartScreen);

        line[8] = new Line();
        line[8].setStartX(xHoleStartScreen);
        line[8].setStartY(yHoleStartScreen);
        line[8].setEndX(xHoleEndScreen);
        line[8].setEndY(yHoleStartScreen);

        line[9] = new Line();
        line[9].setStartX(xHoleEndScreen);
        line[9].setStartY(yHoleStartScreen);
        line[9].setEndX(xHoleEndScreen);
        line[9].setEndY(yHoleEndScreen);

        line[10] = new Line();
        line[10].setStartX(xHoleEndScreen);
        line[10].setStartY(yHoleEndScreen);
        line[10].setEndX(xHoleStartScreen);
        line[10].setEndY(yHoleEndScreen);

        line[11] = new Line();
        line[11].setStartX(xHoleStartScreen);
        line[11].setStartY(yHoleEndScreen);
        line[11].setEndX(xHoleStartScreen);
        line[11].setEndY(yHoleStartScreen);

        Node[] nodes;
        int nodeCount;

        if (isDead) {
            nodeCount = 16;
            nodes = new Node[nodeCount + explosionParticles.size()];
        } else {
            nodeCount = 17;
            nodes = new Node[nodeCount + explosionParticles.size()];

            Circle enemyShip = new Circle();
            enemyShip.setCenterX(xStartObstacleScreen);
            enemyShip.setCenterY(yStartObstacleScreen);
            enemyShip.setRadius(obstacleSizeScreen);
            enemyShip.setFill(Color.INDIANRED);

            nodes[16] = enemyShip;
        }

        System.arraycopy(rectangle, 0, nodes, 0, 4);
        System.arraycopy(line, 0, nodes, 4, 12);

        int printIteration = nodeCount;

        for (ExplosionParticle toAdd : explosionParticles) {
            nodes[printIteration] = toAdd.display(gameScreenScale, gameScreenX, gameScreenY);
            printIteration++;
        }

        return nodes;
    }

    public Node[] displayProjectiles(int gameScreenScale, int gameScreenX, int gameScreenY) {
        Node[] nodes = new Node[projectiles.size()];
        for (int i = 0; i < projectiles.size(); i++) {
            nodes[i] = projectiles.get(i).display(gameScreenScale, gameScreenX, gameScreenY);
        }
        return nodes;
    }
}
