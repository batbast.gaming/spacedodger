package org.tyranobast.spacedodger.client.game.effects;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class NewShipDirectionTarget {

    private final double x;
    private final double y;
    private double durationLeft;

    public NewShipDirectionTarget(double x, double y) {
        this.x = x;
        this.y = y;
        durationLeft = 10;
    }

    public boolean shade(int gameFPS) {
        durationLeft = durationLeft - 10d / gameFPS;
        return durationLeft == 0;
    }

    public ImageView display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        int xCenter = (int) (gameScreenX + (double) gameScreenScale / 1000 * (this.x + 500));
        int yCenter = (int) (gameScreenY + (double) gameScreenScale / 1000 * (this.y + 500));

        Image image = new Image(String.valueOf(getClass().getResource("/images/move_target.png")));
        ImageView imageView = new ImageView(image);
        imageView.setX(xCenter);
        imageView.setY(yCenter);
        imageView.setFitWidth(gameScreenScale / 15f * (1 - durationLeft / 10d));
        imageView.setPreserveRatio(true);
        imageView.setOpacity(durationLeft * 10 / 100d);

        return imageView;
    }
}
