package org.tyranobast.spacedodger.client.game.effects;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.tyranobast.spacedodger.client.Random;

public class NitroBubbleParticle {

    private final double x;
    private double y;
    private double topY;
    private final double scale;

    public NitroBubbleParticle(double bottomY, double topY, double xMin, double xMax) {
        this.scale = Random.random(3, 5);

        if (topY < bottomY) {
            topY = bottomY;
        }

        this.x = Random.random(xMin + scale, xMax - scale);
        this.y = Random.random(bottomY, topY);

        this.topY = topY;
    }

    public boolean disappear() {
        return this.y < this.topY;
    }

    public void move(double topY, int fps) {
        this.topY = topY;
        double oneTurnFlyDistance = 5 * 10d / fps;

        this.y = this.y - oneTurnFlyDistance;
    }

    public Circle display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        int radius = (int) (this.scale * gameScreenScale / 1000);
        int xCenter = (int) (gameScreenX + (double) gameScreenScale / 1000 * (this.x + 500));
        int yCenter = (int) (gameScreenY + (double) gameScreenScale / 1000 * (this.y + 500));

        Circle circle = new Circle();
        circle.setCenterX(xCenter);
        circle.setCenterY(yCenter);
        circle.setRadius(radius);
        circle.setFill(Color.ORANGE);

        return circle;
    }
}
