package org.tyranobast.spacedodger.client.game;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import org.tyranobast.spacedodger.client.Random;

public class MovingWall extends Wall {

    private final int obstacleWidth;
    private final int obstacleHeight;
    private double obstacleXStart;
    private double obstacleYStart;
    private double xTo;
    private double yTo;

    public MovingWall(int obstacleWidth, int obstacleHeight, int distance, int color, int gameFPS) {

        super(-450, -450, 450, 450, distance, color, gameFPS);

        this.obstacleWidth = obstacleWidth;
        this.obstacleHeight = obstacleHeight;

        int[] newCoordinates = defineNewCoordinates();
        this.obstacleXStart = newCoordinates[0];
        this.obstacleYStart = newCoordinates[1];

        int[] nextCoordinates = defineNewCoordinates();
        this.xTo = nextCoordinates[0];
        this.yTo = nextCoordinates[1];
    }

    private double distance(double xFrom, double yFrom, double xTo, double yTo) {
        return Math.sqrt(Math.pow((xTo - xFrom), 2) + Math.pow((yTo - yFrom), 2));
    }

    public void move() {

        double oneTurnFlyDistance = 25 * 10d / gameFPS;

        double distance = distance(this.obstacleXStart, this.obstacleYStart, this.xTo, this.yTo);

        if (distance < oneTurnFlyDistance) {
            int[] nextCoordinates = defineNewCoordinates();
            this.xTo = nextCoordinates[0];
            this.yTo = nextCoordinates[1];
            distance = 0;
        }

        double partOfTheDistance;

        if (distance != 0) {
            partOfTheDistance = (oneTurnFlyDistance / distance);
        } else {
            partOfTheDistance = 0;
        }

        this.obstacleXStart = partOfTheDistance * this.xTo + ((1 - partOfTheDistance) * this.obstacleXStart);
        this.obstacleYStart = partOfTheDistance * this.yTo + ((1 - partOfTheDistance) * this.obstacleYStart);
    }

    private int[] defineNewCoordinates() {
        int xStart = (int) Random.random(-450, 450 - obstacleWidth);
        int yStart = (int) Random.random(-450, 450 - obstacleHeight);
        return new int[]{xStart, yStart};
    }

    public double getObstacleXStart() {
        return obstacleXStart;
    }

    public double getObstacleYStart() {
        return obstacleYStart;
    }

    public double getObstacleXEnd() {
        return obstacleXStart + obstacleWidth;
    }

    public double getObstacleYEnd() {
        return obstacleYStart + obstacleHeight;
    }

    public Node[] display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        boolean canDisplay = !(obstacleWidth == 0 && obstacleHeight == 0);

        int xStart = (int) (holeXStart / distance + 500);
        int yStart = (int) (holeYStart / distance + 500);
        int xEnd = (int) (holeXEnd / distance + 500);
        int yEnd = (int) (holeYEnd / distance + 500);

        int xHoleStartScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xStart);
        int yHoleStartScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yStart);
        int xHoleEndScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xEnd);
        int yHoleEndScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yEnd);

        int xStartWall = (int) (-500 / distance + 500);
        int yStartWall = (int) (-500 / distance + 500);
        int xEndWall = (int) (500 / distance + 500);
        int yEndWall = (int) (500 / distance + 500);

        int xWallStartScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xStartWall);
        int yWallStartScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yStartWall);
        int xWallEndScreen = (int) (gameScreenX + (double) gameScreenScale / 1000 * xEndWall);
        int yWallEndScreen = (int) (gameScreenY + (double) gameScreenScale / 1000 * yEndWall);

        int xStartObstacleGame = (int) (this.obstacleXStart / distance + 500);
        int yStartObstacleGame = (int) (this.obstacleYStart / distance + 500);
        int xEndObstacleGame = (int) ((this.obstacleWidth + this.obstacleXStart) / distance + 500);
        int yEndObstacleGame = (int) ((this.obstacleHeight + this.obstacleYStart) / distance + 500);

        int xStartObstacleScreen = (int) (gameScreenX + gameScreenScale / 1000d * xStartObstacleGame);
        int yStartObstacleScreen = (int) (gameScreenY + gameScreenScale / 1000d * yStartObstacleGame);
        int xEndObstacleScreen = (int) (gameScreenX + gameScreenScale / 1000d * xEndObstacleGame);
        int yEndObstacleScreen = (int) (gameScreenY + gameScreenScale / 1000d * yEndObstacleGame);

        if (xHoleStartScreen < gameScreenX) {
            xHoleStartScreen = gameScreenX;
        }
        if (yHoleStartScreen < gameScreenY) {
            yHoleStartScreen = gameScreenY;
        }
        if (xHoleEndScreen > gameScreenX + gameScreenScale) {
            xHoleEndScreen = gameScreenX + gameScreenScale;
        }

        if (yHoleEndScreen > gameScreenY + gameScreenScale) {
            yHoleEndScreen = gameScreenY + gameScreenScale;
        }

        Rectangle[] rectangle;

        if (canDisplay) {
            rectangle = new Rectangle[5];
        } else {
            rectangle = new Rectangle[4];
        }

        rectangle[0] = new Rectangle();
        rectangle[0].setX(gameScreenX);
        rectangle[0].setY(gameScreenY);
        rectangle[0].setWidth(gameScreenScale);
        rectangle[0].setHeight(yHoleStartScreen);
        rectangle[0].setFill(Color.rgb(color, color, color, 1));

        rectangle[1] = new Rectangle();
        rectangle[1].setX(xHoleEndScreen);
        rectangle[1].setY(gameScreenY);
        rectangle[1].setWidth(gameScreenX + gameScreenScale - xHoleEndScreen);
        rectangle[1].setHeight(gameScreenScale);
        rectangle[1].setFill(Color.rgb(color, color, color, 1));

        rectangle[2] = new Rectangle();
        rectangle[2].setX(gameScreenX);
        rectangle[2].setY(yHoleEndScreen);
        rectangle[2].setWidth(gameScreenScale);
        rectangle[2].setHeight(gameScreenScale - yHoleEndScreen);
        rectangle[2].setFill(Color.rgb(color, color, color, 1));

        rectangle[3] = new Rectangle();
        rectangle[3].setX(gameScreenX);
        rectangle[3].setY(gameScreenY);
        rectangle[3].setWidth(xHoleStartScreen - gameScreenX);
        rectangle[3].setHeight(gameScreenScale);
        rectangle[3].setFill(Color.rgb(color, color, color, 1));

        if (canDisplay) {
            rectangle[4] = new Rectangle();
            rectangle[4].setX(xStartObstacleScreen);
            rectangle[4].setY(yStartObstacleScreen);
            rectangle[4].setWidth(xEndObstacleScreen - xStartObstacleScreen);
            rectangle[4].setHeight(yEndObstacleScreen - yStartObstacleScreen);
            rectangle[4].setFill(Color.rgb(color, color, color, 1));
            rectangle[4].setStroke(Color.BLACK);
        }

        Line[] line = new Line[12];

        line[0] = new Line();
        line[0].setStartX(gameScreenX);
        line[0].setStartY(gameScreenY);
        line[0].setEndX(xWallStartScreen);
        line[0].setEndY(yWallStartScreen);

        line[1] = new Line();
        line[1].setStartX(gameScreenX + gameScreenScale);
        line[1].setStartY(gameScreenY);
        line[1].setEndX(xWallEndScreen);
        line[1].setEndY(yWallStartScreen);

        line[2] = new Line();
        line[2].setStartX(gameScreenX + gameScreenScale);
        line[2].setStartY(gameScreenY + gameScreenScale);
        line[2].setEndX(xWallEndScreen);
        line[2].setEndY(yWallEndScreen);

        line[3] = new Line();
        line[3].setStartX(gameScreenX);
        line[3].setStartY(gameScreenY + gameScreenScale);
        line[3].setEndX(xWallStartScreen);
        line[3].setEndY(yWallEndScreen);

        line[4] = new Line();
        line[4].setStartX(xWallStartScreen);
        line[4].setStartY(yWallStartScreen);
        line[4].setEndX(xWallStartScreen);
        line[4].setEndY(yWallEndScreen);

        line[5] = new Line();
        line[5].setStartX(xWallStartScreen);
        line[5].setStartY(yWallEndScreen);
        line[5].setEndX(xWallEndScreen);
        line[5].setEndY(yWallEndScreen);

        line[6] = new Line();
        line[6].setStartX(xWallEndScreen);
        line[6].setStartY(yWallEndScreen);
        line[6].setEndX(xWallEndScreen);
        line[6].setEndY(yWallStartScreen);

        line[7] = new Line();
        line[7].setStartX(xWallEndScreen);
        line[7].setStartY(yWallStartScreen);
        line[7].setEndX(xWallStartScreen);
        line[7].setEndY(yWallStartScreen);

        line[8] = new Line();
        line[8].setStartX(xHoleStartScreen);
        line[8].setStartY(yHoleStartScreen);
        line[8].setEndX(xHoleEndScreen);
        line[8].setEndY(yHoleStartScreen);

        line[9] = new Line();
        line[9].setStartX(xHoleEndScreen);
        line[9].setStartY(yHoleStartScreen);
        line[9].setEndX(xHoleEndScreen);
        line[9].setEndY(yHoleEndScreen);

        line[10] = new Line();
        line[10].setStartX(xHoleEndScreen);
        line[10].setStartY(yHoleEndScreen);
        line[10].setEndX(xHoleStartScreen);
        line[10].setEndY(yHoleEndScreen);

        line[11] = new Line();
        line[11].setStartX(xHoleStartScreen);
        line[11].setStartY(yHoleEndScreen);
        line[11].setEndX(xHoleStartScreen);
        line[11].setEndY(yHoleStartScreen);

        Node[] node;
        if (canDisplay) {
            node = new Node[17];
            System.arraycopy(rectangle, 0, node, 0, 5);
            System.arraycopy(line, 0, node, 5, 12);
        } else {
            node = new Node[16];
            System.arraycopy(rectangle, 0, node, 0, 4);
            System.arraycopy(line, 0, node, 4, 12);
        }
        return node;
    }
}
