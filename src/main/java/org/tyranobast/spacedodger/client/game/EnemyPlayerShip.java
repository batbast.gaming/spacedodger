package org.tyranobast.spacedodger.client.game;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.tyranobast.spacedodger.client.Random;
import org.tyranobast.spacedodger.client.game.effects.ExplosionParticle;

import java.util.ArrayList;

class EnemyPlayerShip {

    private double enemyX;
    private double enemyY;
    private double distance;
    private final int shipScale;
    private final String username;
    private boolean isDead;
    private final ArrayList<ExplosionParticle> explosionParticles;
    private final int gameFPS;

    public EnemyPlayerShip(String username, int gameFPS) {
        this.username = username;
        this.shipScale = 50;
        this.enemyX = 0;
        this.enemyY = 0;
        this.distance = 0;
        this.isDead = false;
        this.explosionParticles = new ArrayList<>();
        this.gameFPS = gameFPS;
    }

    public void move(double enemyX, double enemyY, double enemyZ, double playerZ) {

        if (isDead) {
            ExplosionParticle[] toRemoveExplosion = new ExplosionParticle[explosionParticles.size()];

            for (int i = 0; i < explosionParticles.size(); i++) {
                explosionParticles.get(i).move(this.gameFPS);
                explosionParticles.get(i).approach(this.gameFPS);
                if (explosionParticles.get(i).shade(this.gameFPS)) {
                    toRemoveExplosion[i] = explosionParticles.get(i);
                }
            }

            for (ExplosionParticle remove : toRemoveExplosion) {
                explosionParticles.remove(remove);
            }

        } else {
            this.enemyX = enemyX;
            this.enemyY = enemyY;
            this.distance = enemyZ - playerZ;

            if (this.distance < 0) {
                this.distance = 0;
            }
        }
    }

    public void die() {
        if (!isDead) {
            isDead = true;
            for (int i = 0; i < 10; i++) {
                explosionParticles.add(new ExplosionParticle(this.enemyX, this.enemyY, this.enemyX + Random.random(-200, 200), this.enemyY + Random.random(-200, 200), this.distance));
            }
        }
    }

    public String getUsername() {
        return username;
    }

    public double getDistance() {
        return distance;
    }

    public Node[] display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        if (!isDead) {
            Node[] nodes;

            if (distance > 0) {

                nodes = new Node[explosionParticles.size() + 1];

                double xCenterGame = this.enemyX / distance + 500;
                double yCenterGame = this.enemyY / distance + 500;
                double shipScaleGame = this.shipScale / distance;

                double xCenterScreen = gameScreenX + (double) gameScreenScale / 1000 * xCenterGame;
                double yCenterScreen = gameScreenY + (double) gameScreenScale / 1000 * yCenterGame;
                double shipScaleScreen = (double) gameScreenScale / 1000 * shipScaleGame;

                Circle circle = new Circle();
                circle.setCenterX(xCenterScreen);
                circle.setCenterY(yCenterScreen);
                circle.setRadius(shipScaleScreen);
                circle.setFill(Color.ROYALBLUE);

                nodes[0] = circle;
            } else {
                nodes = new Node[0];
            }

            int printIteration = 1;

            for (ExplosionParticle toAdd : explosionParticles) {
                nodes[printIteration] = toAdd.display(gameScreenScale, gameScreenX, gameScreenY);
                printIteration++;
            }
            return nodes;
        } else {
            return new Node[0];
        }
    }
}
