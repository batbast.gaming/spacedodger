package org.tyranobast.spacedodger.client.game.effects;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class ExplosionParticle extends ShipReactorParticle {

    private double distance;

    public ExplosionParticle(double x, double y, double xTo, double yTo, double distance) {
        super(x, y, xTo, yTo);

        this.particleSpeed = 20;

        this.distance = distance;
    }

    public void approach(int gameFPS) {
        distance = distance - (10d / gameFPS) / 2;
    }

    @Override
    public Circle display(int gameScreenScale, int gameScreenX, int gameScreenY) {

        int xCenter = (int) (gameScreenX + (double) gameScreenScale / 1000d * (this.x / this.distance + 500));
        int yCenter = (int) (gameScreenY + (double) gameScreenScale / 1000d * (this.y / this.distance + 500));

        Circle circle = new Circle();
        circle.setCenterX(xCenter);
        circle.setCenterY(yCenter);
        circle.setRadius(gameScreenScale / 150d);
        circle.setFill(Color.rgb(255, (int) color, 0));
        circle.setOpacity(durationLeft * 10 / 100d);

        return circle;
    }
}
