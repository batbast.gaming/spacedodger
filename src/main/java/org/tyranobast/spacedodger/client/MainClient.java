package org.tyranobast.spacedodger.client;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.tyranobast.spacedodger.client.game.*;
import org.tyranobast.spacedodger.client.menus.account.*;
import org.tyranobast.spacedodger.client.menus.credits.CreditsMenu;
import org.tyranobast.spacedodger.client.menus.level.LevelMenu;
import org.tyranobast.spacedodger.client.menus.looser.LooserMenu;
import org.tyranobast.spacedodger.client.menus.main.MainMenu;
import org.tyranobast.spacedodger.client.menus.multiplayer.gameend.MultiplayerGameEndMenu;
import org.tyranobast.spacedodger.client.menus.multiplayer.hub.MultiplayerHubMenu;
import org.tyranobast.spacedodger.client.menus.multiplayer.hub.ServerWaiterMenu;
import org.tyranobast.spacedodger.client.menus.multiplayer.win.MultiplayerWinMenu;
import org.tyranobast.spacedodger.client.menus.option.MainOption;
import org.tyranobast.spacedodger.client.menus.transition.MenuTransition;

import java.net.URL;
import java.util.ArrayList;

public class MainClient extends Application {

    private static Stage stage;
    public static MainMenu mainMenu;
    public static ArrayList<Game> gameLevelX;
    public static LooserMenu looserMenu;
    public static CreditsMenu creditsMenu;
    public static MenuTransition menuTransition;
    public static MultiplayerHubMenu multiplayerHubMenu;
    public static ServerWaiterMenu serverWaiterMenu;
    public static Config config;
    public static LevelMenu levelMenu;
    public static NetworkerClient networkerClient;
    public static MultiplayerWinMenu multiplayerWinMenu;
    public static MultiplayerGameEndMenu multiplayerGameEndMenu;
    public static AccountMenu accountMenu;
    public static SignUpMenu signUpMenu;
    public static LogInMenu logInMenu;
    public static MainOption mainOption;
    public static EditUsernameMenu editUsernameMenu;
    public static EditPasswordMenu editPasswordMenu;
    public static boolean isLogged;
    public static String onlineToken;
    public static String onlineUsername;

    @Override
    public void start(Stage stageA) {

        Runtime.getRuntime().addShutdownHook(new Thread(MainClient::shutdown));

        stage = stageA;

        stage.setTitle("Space Dodger");
        stage.show();

        URL iconURL = getClass().getResource("/images/game_icon.png");
        stage.getIcons().add(new Image(String.valueOf(iconURL)));

        Group gameGroup = new Group();
        Group GUIGroup = new Group();
        GUIGroup.getChildren().add(gameGroup);

        Scene scene = new Scene(GUIGroup);
        scene.setFill(Color.WHITE);

        stage.setScene(scene);
        stage.setFullScreen(true);
        stage.show();

        stage.setOnCloseRequest(event -> System.exit(0));

        stage.fullScreenProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue)
                stage.setFullScreen(true);
        });

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

        int screenWidth = (int) screenBounds.getWidth();
        int screenHeight = (int) screenBounds.getHeight();
        int gameScreenScale = (int) screenBounds.getHeight();
        int gameScreenX = (int) screenBounds.getWidth() / 2 - gameScreenScale / 2;
        int gameScreenY = 0;

        networkerClient = new NetworkerClient();
        config = new Config();

        gameLevelX = new ArrayList<>();

        gameLevelX.add(new GameLevel1(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth));
        gameLevelX.add(new GameLevel2(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth));
        gameLevelX.add(new GameLevel3(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth));
        gameLevelX.add(new GameLevel4(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth));

        mainMenu = new MainMenu(gameGroup, gameScreenScale, gameScreenX, gameScreenY, screenHeight, screenWidth);
        multiplayerWinMenu = new MultiplayerWinMenu(gameGroup, gameScreenScale, screenWidth, screenHeight);
        multiplayerGameEndMenu = new MultiplayerGameEndMenu(gameGroup, gameScreenScale, screenWidth, screenHeight);
        looserMenu = new LooserMenu(gameGroup, gameScreenScale, screenWidth, screenHeight);
        creditsMenu = new CreditsMenu(gameGroup, gameScreenScale, screenWidth, screenHeight);
        menuTransition = new MenuTransition(gameGroup, screenWidth, screenHeight, gameScreenScale);
        multiplayerHubMenu = new MultiplayerHubMenu(gameGroup, gameScreenScale, screenHeight, screenWidth);
        levelMenu = new LevelMenu(gameGroup, gameScreenScale, screenHeight, screenWidth);
        serverWaiterMenu = new ServerWaiterMenu(gameGroup, gameScreenScale, screenHeight, screenWidth);
        accountMenu = new AccountMenu(gameGroup, gameScreenScale, screenHeight, screenWidth);
        signUpMenu = new SignUpMenu(gameGroup, GUIGroup, gameScreenScale, screenHeight, screenWidth);
        logInMenu = new LogInMenu(gameGroup, GUIGroup, gameScreenScale, screenHeight, screenWidth);
        mainOption = new MainOption(gameGroup, GUIGroup, gameScreenScale, screenHeight, screenWidth);
        editUsernameMenu = new EditUsernameMenu(gameGroup, GUIGroup, gameScreenScale, screenHeight, screenWidth);
        editPasswordMenu = new EditPasswordMenu(gameGroup, GUIGroup, gameScreenScale, screenHeight, screenWidth);

        mainMenu.menuStart();
    }

    private static void shutdown() {
        if (MainClient.isLogged) {
            MainClient.networkerClient.sendMessage("log_out " + MainClient.onlineToken, "setting");
        }
        networkerClient.closeNetworker();
        System.out.println("shutdown hook successfully executed");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
