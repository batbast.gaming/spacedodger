package org.tyranobast.spacedodger.client;

public enum Transmission {
    Message,
    Setting,
    Coordinate,
    Util
}
