package org.tyranobast.spacedodger.client;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Scanner;

public class Config {
    private final ArrayList<String> configValue;
    private final ArrayList<String> defaultValues;
    private File externalConfigLocation = null;

    public Config() {
        try {
            File externalConfigFile = new File(Config.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            this.externalConfigLocation = new File(externalConfigFile.getParent() + File.separator + "config.txt");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        this.configValue = new ArrayList<>();

        String rawInternalConfig = loadInternalResources();

        rawInternalConfig = rawInternalConfig.replaceAll("(?m)^#.*", "");

        configValue.addAll(List.of(rawInternalConfig.split("\n")));

        configValue.remove("");

        defaultValues = new ArrayList<>(configValue);

        String rawExternalConfig = loadExternalResources();

        if (!rawExternalConfig.equals("")) {
            rawExternalConfig = rawExternalConfig.replaceAll("^#.*", "");

            ArrayList<String> externalConfigValue = new ArrayList<>(List.of(rawExternalConfig.split("\n")));

            externalConfigValue.remove("");

            for (int i = 0; i < configValue.size(); i++) {
                for (String externalValue : externalConfigValue) {
                    if (configValue.get(i).split(" ")[0].equals(externalValue.split(" ")[0])) {
                        configValue.set(i, externalValue);
                        break;
                    }
                }
            }
        }
    }

    public void updateExternalConfigFile() {
        String[] internalValues = loadInternalResources().split("\n");

        StringBuilder toWrite = new StringBuilder();

        for (String internalValue : internalValues) {
            if (internalValue.split("")[0].equals("#")) {
                toWrite.append(internalValue);
            } else {
                boolean found = false;
                for (String internalConfigValue : configValue) {
                    if (internalValue.split(" ")[0].equals(internalConfigValue.split(" ")[0])) {
                        toWrite.append(internalConfigValue);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    toWrite.append(internalValue);
                }
            }
            toWrite.append("\n");
        }

        try {
            FileWriter myWriter = new FileWriter(externalConfigLocation);
            myWriter.write(toWrite.toString());
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setValue(String valueName, String newValue) {
        for (int i = 0; i < configValue.size(); i++) {
            if (configValue.get(i).contains(valueName)) {
                configValue.set(i, valueName + " = " + newValue);
                break;
            }
        }
    }

    private String getValue(String valueName) {
        String toReturn = "";

        for (String value : configValue) {
            if (value.contains(valueName)) {
                toReturn = value.replace(valueName + " = ", "");
                break;
            }
        }

        return toReturn;
    }

    private String getDefaultValue(String valueName) {
        String toReturn = "";

        for (String value : defaultValues) {
            if (value.contains(valueName)) {
                toReturn = value.replace(valueName + " = ", "");
                break;
            }
        }

        return toReturn;
    }

    public int getIntValue(String valueName) {
        return Integer.parseInt(getValue(valueName));
    }

    public int getIntDefaultValue(String valueName) {
        return Integer.parseInt(getDefaultValue(valueName));
    }

    public boolean getBooleanValue(String valueName) {
        return Boolean.parseBoolean(getValue(valueName));
    }

    public boolean getBooleanDefaultValue(String valueName) {
        return Boolean.parseBoolean(getDefaultValue(valueName));
    }

    public String getStringValue(String valueName) {
        return getValue(valueName);
    }

    public String getStringDefaultValue(String valueName) {
        return getDefaultValue(valueName);
    }

    public double getDoubleValue(String valueName) {
        return Double.parseDouble(getValue(valueName));
    }

    public double getDoubleDefaultValue(String valueName) {
        return Double.parseDouble(getDefaultValue(valueName));
    }

    private String loadExternalResources() {
        InputStream resource = null;

        File externalConfigFile = externalConfigLocation;

        if (externalConfigFile != null && externalConfigFile.exists()) {
            try {
                resource = new FileInputStream(externalConfigFile.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            return "";
        }

        if (resource == null) {
            throw new MissingResourceException("external Config is empty", getClass().getName(), "config.txt");
        }

        Scanner scanner = new Scanner(resource).useDelimiter("\\A");

        StringBuilder builder = new StringBuilder();
        while (scanner.hasNext()) {
            builder.append(scanner.next());
        }
        return builder.toString();

    }

    private String loadInternalResources() {

        String path = "/config.txt";

        InputStream resource = this.getClass().getResourceAsStream(path);

        if (resource == null) {
            throw new MissingResourceException("Internal config not found", getClass().getName(), "config.txt");
        }
        Scanner scanner = new Scanner(resource).useDelimiter("\\A");

        StringBuilder builder = new StringBuilder();
        while (scanner.hasNext()) {
            builder.append(scanner.next());
        }
        return builder.toString();
    }
}
