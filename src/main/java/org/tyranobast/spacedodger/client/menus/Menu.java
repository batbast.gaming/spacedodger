package org.tyranobast.spacedodger.client.menus;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;

import java.util.ArrayList;

public class Menu {

    protected final Group gameGroup;
    protected final int gameScreenScale;
    protected final int screenWidth;
    protected final int screenHeight;
    protected final Scene scene;
    protected Timeline menuTimeline;
    protected final ArrayList<MenuButton> menuButtons;
    protected int xMouse;
    protected int yMouse;
    protected int xClick;
    protected int yClick;
    protected int menuFPS;
    protected EventHandler<MouseEvent> mousePressedEvent;
    protected EventHandler<MouseEvent> mouseMovedEvent;

    public Menu(Group gameGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        this.menuFPS = MainClient.config.getIntValue("menu_fps");

        this.gameGroup = gameGroup;
        this.gameScreenScale = gameScreenScale;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.scene = gameGroup.getScene();
        this.menuButtons = new ArrayList<>();
        this.xClick = 0;
        this.yClick = 0;
        this.xMouse = 0;
        this.yMouse = 0;

        mousePressedEvent = this::mousePressed;

        mouseMovedEvent = this::mouseMoved;

        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / menuFPS), event -> menuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    protected void mousePressed(MouseEvent event) {
        xClick = (int) event.getX();
        yClick = (int) event.getY();
    }

    protected void mouseMoved(MouseEvent event) {
        xMouse = (int) event.getX();
        yMouse = (int) event.getY();
    }

    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        scene.setFill(Color.PALEVIOLETRED);

        double buttonWidth = screenWidth / 2d;
        double buttonHeight = screenHeight / 3d;

        menuButtons.clear();
        menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight / 3d, buttonHeight, buttonWidth, "Go back", "back"));
    }

    protected void reloadConfig() {
        this.menuFPS = MainClient.config.getIntValue("menu_fps");
        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / menuFPS), event -> menuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    public void menuStart() {
        reloadConfig();
        initMenu();
        menuTimeline.play();
    }

    protected void menuStop() {
        scene.removeEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.removeEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);
        menuTimeline.stop();
    }

    protected void menuLoop() {
        gameGroup.getChildren().clear();

        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {
                xClick = 0;
                yClick = 0;
                xMouse = 0;
                yMouse = 0;
                menuStop();
                gameGroup.getChildren().clear();
                if ("back".equals(menuButton.getData())) {
                    menuStop();
                    MainClient.menuTransition.menuStart("menu");
                    break;
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}