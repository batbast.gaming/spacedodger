package org.tyranobast.spacedodger.client.menus.main;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

public class MainMenu extends Menu {

    private final int gameScreenX;
    private final int gameScreenY;
    private MenuFont menuFont;
    private int fontRotationSpeed;
    private double fontAngleSpeed;
    private int fontAngleSpeedReduction;
    private boolean areButtonsShown;
    private double minFontSpacing;
    private double maxFontSpacing;

    public MainMenu(Group gameGroup, int gameScreenScale, int gameScreenX, int gameScreenY, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);

        this.fontRotationSpeed = MainClient.config.getIntValue("menu_font_rotation");
        this.minFontSpacing = MainClient.config.getDoubleValue("menu_font_spacing_min");
        this.maxFontSpacing = MainClient.config.getDoubleValue("menu_font_spacing_max");
        this.fontAngleSpeed = MainClient.config.getDoubleValue("menu_font_angle_speed");
        this.fontAngleSpeedReduction = MainClient.config.getIntValue("menu_font_angle_reduction");
        this.gameScreenX = gameScreenX;
        this.gameScreenY = gameScreenY;
        this.areButtonsShown = true;
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        menuFont = new MenuFont(gameScreenScale, gameScreenX, gameScreenY, minFontSpacing, maxFontSpacing);

        int[] RGBColor = menuFont.calculateDraw();

        scene.setFill(Color.rgb(RGBColor[0], RGBColor[1], RGBColor[2]));

        int buttonWidth = screenWidth * 2 / 7;
        int buttonHeight = screenHeight / 7;
        int hideButtonWidth = screenWidth / 7;
        int hideButtonHeight = screenHeight / 9;
        this.areButtonsShown = true;

        menuButtons.clear();

        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight / 7d, buttonHeight, buttonWidth, "Play", "play"));
        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 3 / 7d, buttonHeight, buttonWidth, "Credits", "credits"));
        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Account", "account"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight / 7d, buttonHeight, buttonWidth, "Options", "option"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight * 3 / 7d, buttonHeight, buttonWidth, "Leave", "exit"));
        menuButtons.add(new MenuButton(screenWidth * 6 / 7d, screenHeight * 8 / 9d, hideButtonHeight, hideButtonWidth, "Hide menu", "show_hide"));
    }

    @Override
    protected void reloadConfig() {
        this.menuFPS = MainClient.config.getIntValue("menu_fps");
        this.fontRotationSpeed = MainClient.config.getIntValue("menu_font_rotation");
        this.minFontSpacing = MainClient.config.getDoubleValue("menu_font_spacing_min");
        this.maxFontSpacing = MainClient.config.getDoubleValue("menu_font_spacing_max");
        this.fontAngleSpeed = MainClient.config.getDoubleValue("menu_font_angle_speed");
        this.fontAngleSpeedReduction = MainClient.config.getIntValue("menu_font_angle_reduction");
        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / menuFPS), event -> menuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    @Override
    protected void menuLoop() {
        menuFont.changeDraw(fontAngleSpeed * 10 / menuFPS, fontAngleSpeedReduction);
        menuFont.rotate(fontRotationSpeed * 10d / menuFPS);

        gameGroup.getChildren().clear();

        gameGroup.getChildren().addAll(menuFont.display());

        displayButtons:
        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {
                xClick = 0;
                yClick = 0;
                xMouse = 0;
                yMouse = 0;
                switch (menuButton.getData()) {
                    case "exit" -> {
                        if (MainClient.isLogged) {
                            MainClient.networkerClient.sendMessage("log_out " + MainClient.onlineToken, "setting");
                        }
                        System.exit(0);
                    }
                    case "play" -> {
                        menuStop();
                        MainClient.menuTransition.menuStart("level");
                        break displayButtons;
                    }
                    case "credits" -> {
                        menuStop();
                        MainClient.menuTransition.menuStart("credits");
                        break displayButtons;
                    }
                    case "account" -> {
                        menuStop();
                        MainClient.menuTransition.menuStart("account");
                        break displayButtons;
                    }
                    case "option" -> {
                        menuStop();
                        MainClient.menuTransition.menuStart("option");
                        break displayButtons;
                    }
                    case "show_hide" -> {
                        if (areButtonsShown) {
                            menuButton.changeText("Show menu");
                            areButtonsShown = false;
                            for (MenuButton menuButtonToModify : menuButtons) {
                                if (menuButtonToModify != menuButton) {
                                    menuButtonToModify.hide();
                                }
                            }
                        } else {
                            menuButton.changeText("Hide menu");
                            areButtonsShown = true;
                            for (MenuButton menuButtonToModify : menuButtons) {
                                if (menuButtonToModify != menuButton) {
                                    menuButtonToModify.show();
                                }
                            }
                        }
                    }
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}