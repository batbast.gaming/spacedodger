package org.tyranobast.spacedodger.client.menus.multiplayer.hub;

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.Transmission;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

public class MultiplayerHubMenu extends Menu {

    private String gameInitData;
    private Label userListLabel;
    private boolean isWaitingForUserList;

    public MultiplayerHubMenu(Group gameGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.setFill(Color.PALEVIOLETRED);

        isWaitingForUserList = false;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        double buttonWidth = screenWidth / 2d;
        double buttonHeight = screenHeight / 7d;

        menuButtons.clear();

        menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight / 7d, buttonHeight, buttonWidth, "Leave", "leave"));
        menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight * 3 / 7d, buttonHeight, buttonWidth, "Actualise online players", "listPlayers"));
        menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Ask for starting game", "start"));

        Font font = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 40d);
        this.userListLabel = new Label("");
        this.userListLabel.setFont(font);
        this.userListLabel.setTextFill(Color.MEDIUMVIOLETRED);
    }

    @Override
    public void menuStart() {
        reloadConfig();
        initMenu();
        MainClient.networkerClient.sendMessage("join " + MainClient.onlineToken, "util");
        menuTimeline.play();
    }

    @Override
    protected void menuLoop() {
        gameGroup.getChildren().clear();

        if (isWaitingForUserList) {
            String message = MainClient.networkerClient.getMessage(Transmission.Message, (String data) ->
                    data.split(">")[0].equals("users"));
            this.userListLabel.setText(message.split(">")[1]);
            isWaitingForUserList = false;
        }

        String messagePing = MainClient.networkerClient.getMessage(Transmission.Util, (String data) ->
                data.equals("ping"));
        if (!messagePing.equals("")) {
            MainClient.networkerClient.sendMessage("pong " + MainClient.onlineToken, "util");
        }

        String messageNoStart = MainClient.networkerClient.getMessage(Transmission.Util, (String data) ->
                data.split(" ")[0].equals("noStart"));
        if (!messageNoStart.equals("")) {
            String[] text = messageNoStart.split(" ");
            StringBuilder finalText = new StringBuilder();
            for (String textElement : text) {
                if (!textElement.equals("noStart")) {
                    finalText.append(textElement).append(" ");
                }
            }
            menuButtons.get(2).changeText(finalText.toString());
        }

        label:
        for (MenuButton menuButton : menuButtons) {

            menuButton.changeGlow(xMouse, yMouse, menuFPS);

            String messageLaunch = MainClient.networkerClient.getMessage(Transmission.Util, (String data) ->
                    data.split(">")[0].equals("game launched"));
            if (menuButton.hasBeenClicked(xClick, yClick)) {
                xClick = 0;
                yClick = 0;
                switch (menuButton.getData()) {
                    case "leave":
                        MainClient.networkerClient.sendMessage("leave " + MainClient.onlineToken, "util");
                        menuStop();
                        MainClient.menuTransition.menuStart("level");
                        xMouse = 0;
                        yMouse = 0;
                        break label;
                    case "listPlayers":
                        MainClient.networkerClient.sendMessage("getUserList " + MainClient.onlineToken, "util");
                        this.isWaitingForUserList = true;
                        break;
                    case "start":
                        menuButtons.get(2).changeText("Waiting for server");
                        MainClient.networkerClient.sendMessage("start " + MainClient.onlineToken, "util");
                        break;
                }
            } else if (!messageLaunch.equals("")) {
                menuStop();
                xMouse = 0;
                yMouse = 0;
                gameInitData = messageLaunch.split(">")[1];
                MainClient.gameLevelX.get(3).startGame();
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }

        gameGroup.getChildren().add(userListLabel);
    }

    public String getGameInitData() {
        return gameInitData;
    }
}
