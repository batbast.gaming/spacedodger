package org.tyranobast.spacedodger.client.menus.account;

import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.ShaMaker;
import org.tyranobast.spacedodger.client.Transmission;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

public class EditPasswordMenu extends Menu {

    private final Group GUIGroup;

    private PasswordField newPassword;
    private PasswordField passwordConfirmation;
    private CheckBox newPasswordCheckBox;
    private CheckBox passwordConfirmCheckBox;
    private Tooltip newPasswordTooltip;
    private Tooltip passwordConfirmTooltip;
    private Label newPasswordLabel;
    private Label passwordConfirmLabel;
    private boolean waitingForServer;

    public EditPasswordMenu(Group gameGroup, Group GUIGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);

        this.GUIGroup = GUIGroup;
        this.waitingForServer = false;
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        this.waitingForServer = false;

        scene.setFill(Color.PALEVIOLETRED);

        double buttonWidth = screenWidth * 2 / 7d;
        double buttonHeight = screenHeight / 5d;
        Pane textPane = new Pane();
        Font font = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 20d);
        Font tooltipFont = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 40d);
        Font labelFont = Font.font("Verdana", FontWeight.BOLD, buttonHeight / 10d);

        menuButtons.clear();

        newPasswordTooltip = new Tooltip();
        newPasswordTooltip.setFont(tooltipFont);

        newPasswordCheckBox = new CheckBox();
        newPasswordCheckBox.setPrefWidth(buttonWidth / 10);
        newPasswordCheckBox.setPrefHeight(buttonHeight / 10);
        newPasswordCheckBox.setLayoutX(screenWidth / 7d);
        newPasswordCheckBox.setLayoutY(screenHeight / 5d + buttonHeight);
        newPasswordCheckBox.setOnAction(event -> {
            if (newPasswordCheckBox.isSelected()) {
                newPasswordTooltip.setText(newPassword.getText());
                newPasswordTooltip.show(newPassword, screenWidth / 7d + buttonWidth, screenHeight / 5d + buttonHeight);
                newPasswordLabel.setText("Hide password");
            } else {
                newPasswordTooltip.hide();
                newPasswordLabel.setText("Show password");
            }
        });

        newPasswordLabel = new Label("Show password");
        newPasswordLabel.setFont(labelFont);
        newPasswordLabel.setLayoutX(screenWidth / 7d + buttonWidth / 15);
        newPasswordLabel.setLayoutY(screenHeight / 5d + buttonHeight);

        newPassword = new PasswordField();
        newPassword.setFont(font);
        newPassword.setPromptText("new password");
        newPassword.setLayoutX(screenWidth / 7d);
        newPassword.setLayoutY(screenHeight / 5d);
        newPassword.setPrefWidth(buttonWidth);
        newPassword.setPrefHeight(buttonHeight);
        newPassword.setOnKeyTyped(e -> {
            if (newPasswordCheckBox.isSelected()) {
                newPasswordTooltip.setText(newPassword.getText());
                newPasswordTooltip.setX(screenWidth / 7d + buttonWidth);
                newPasswordTooltip.setY(screenHeight / 5d + buttonHeight);
            }
        });

        passwordConfirmTooltip = new Tooltip();
        passwordConfirmTooltip.setFont(tooltipFont);

        passwordConfirmCheckBox = new CheckBox();
        passwordConfirmCheckBox.setPrefWidth(buttonWidth / 10);
        passwordConfirmCheckBox.setPrefHeight(buttonHeight / 10);
        passwordConfirmCheckBox.setLayoutX(screenWidth * 4 / 7d);
        passwordConfirmCheckBox.setLayoutY(screenHeight / 5d + buttonHeight);
        passwordConfirmCheckBox.setOnAction(event -> {
            if (passwordConfirmCheckBox.isSelected()) {
                passwordConfirmTooltip.setText(passwordConfirmation.getText());
                passwordConfirmTooltip.show(newPassword, screenWidth * 4 / 7d + buttonWidth, screenHeight / 5d + buttonHeight);
                passwordConfirmLabel.setText("Hide password");
            } else {
                passwordConfirmTooltip.hide();
                passwordConfirmLabel.setText("Show password");
            }
        });

        passwordConfirmLabel = new Label("Show password");
        passwordConfirmLabel.setFont(labelFont);
        passwordConfirmLabel.setLayoutX(screenWidth * 4 / 7d + buttonWidth / 15);
        passwordConfirmLabel.setLayoutY(screenHeight / 5d + buttonHeight);

        passwordConfirmation = new PasswordField();
        passwordConfirmation.setFont(font);
        passwordConfirmation.setPromptText("confirmation");
        passwordConfirmation.setLayoutX(screenWidth * 4 / 7d);
        passwordConfirmation.setLayoutY(screenHeight / 5d);
        passwordConfirmation.setPrefWidth(buttonWidth);
        passwordConfirmation.setPrefHeight(buttonHeight);
        passwordConfirmation.setOnKeyTyped(e -> {
            if (passwordConfirmCheckBox.isSelected()) {
                passwordConfirmTooltip.setText(passwordConfirmation.getText());
                passwordConfirmTooltip.setX(screenWidth * 4 / 7d + buttonWidth);
                passwordConfirmTooltip.setY(screenHeight / 5d + buttonHeight);
            }
        });

        textPane.getChildren().addAll(newPassword, newPasswordLabel, newPasswordCheckBox);
        textPane.getChildren().addAll(passwordConfirmation, passwordConfirmLabel, passwordConfirmCheckBox);

        GUIGroup.getChildren().add(textPane);

        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 3 / 5d, buttonHeight, buttonWidth, "Validate", "validate"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight * 3 / 5d, buttonHeight, buttonWidth, "Cancel", "cancel"));
    }

    private void verifyingData(String newPassword) {
        gameGroup.getChildren().clear();
        menuButtons.get(0).changeText("Waiting for server");

        String hashedPassword = ShaMaker.getSHA(newPassword);

        MainClient.networkerClient.sendMessage("edit_password " + MainClient.onlineToken + " " + hashedPassword, "setting");
        waitingForServer = true;
    }

    @Override
    protected void menuLoop() {

        String messageEdit = MainClient.networkerClient.getMessage(Transmission.Setting, (String data) ->
                data.split(" ")[0].equals(MainClient.onlineToken) &&
                        data.split(" ")[1].equals("edit_password"));

        if (waitingForServer && !messageEdit.equals("")) {
            waitingForServer = false;
            if (messageEdit.split(" ")[2].equals("ok")) {
                menuStop();
                GUIGroup.getChildren().clear();
                GUIGroup.getChildren().add(gameGroup);
                MainClient.menuTransition.menuStart("account");
            } else {
                menuButtons.get(0).changeText("Request refused");
            }
        }

        gameGroup.getChildren().clear();

        displayButtons:
        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {
                switch (menuButton.getData()) {
                    case "cancel" -> {
                        menuStop();
                        gameGroup.getChildren().clear();
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        GUIGroup.getChildren().clear();
                        GUIGroup.getChildren().add(gameGroup);
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                    case "validate" -> {
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        if (newPassword.getText().equals(passwordConfirmation.getText())) {
                            verifyingData(newPassword.getText());
                        } else {
                            menuButtons.get(0).changeText("Invalid passwords");
                        }
                    }
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}