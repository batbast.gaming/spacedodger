package org.tyranobast.spacedodger.client.menus;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuSlider {

    private final double xStart;
    private final double yStart;
    private final double width;
    private final double height;
    private String text;
    private final String data;
    private int value;
    private double step;
    private double minimum;
    private double length;
    private boolean isSelected;
    private double defaultValue;

    public MenuSlider(double xStart, double yStart, double height, double width, String text, String data) {
        this.data = data;
        this.text = text;
        this.xStart = xStart;
        this.yStart = yStart;
        this.width = width;
        this.height = height;
        this.isSelected = false;
    }

    public void setMinMax(double minimum, double maximum) {
        this.minimum = Math.round(minimum / step);
        this.length = (Math.round(maximum / step) - this.minimum);
    }

    public void setDefaultValue(double defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setStep(double step) {
        this.step = step;
    }

    public void setInitValues(int value) {
        this.value = (int) Math.round(value / step);
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getData() {
        return data;
    }

    public String getValue() {
        String toReturn;
        toReturn = String.valueOf(this.value * this.step).replaceAll("\\.0*$", "");
        return toReturn;
    }

    public void move(int xMouse) {
        int newValue = (int) Math.round((((xMouse - xStart) / width) * length + minimum));
        if (isSelected) {
            if (newValue >= minimum && newValue <= minimum + length) {
                value = newValue;
            } else if (newValue < minimum) {
                value = (int) Math.round(minimum);
            } else if (newValue > minimum + length) {
                value = (int) Math.round(minimum + length);
            }
        }
    }

    private boolean hasBeenClicked(int xClick, int yClick) {
        return (xClick > xStart && xClick < xStart + width) && (yClick > yStart && yClick < yStart + height);
    }

    public Node[] display() {
        Pane pane = new Pane();
        Node[] nodes = new Node[3];

        Rectangle rail = new Rectangle();
        rail.setX(xStart);
        rail.setY(yStart + height * 2 / 4);
        rail.setWidth(width);
        rail.setHeight(height / 4);
        rail.setFill(Color.LIMEGREEN);
        rail.setStroke(Color.WHITESMOKE);
        rail.setStrokeWidth(height / 16d);

        nodes[0] = rail;

        Rectangle selector = new Rectangle();
        selector.setX(xStart + width * ((value - minimum) / length) - width / 20);
        selector.setY(yStart + height / 4);
        selector.setWidth(width / 10);
        selector.setHeight(height * 3 / 4);
        selector.setFill(Color.LIMEGREEN);
        selector.setStroke(Color.WHITESMOKE);
        selector.setStrokeWidth(height / 16d);

        nodes[1] = selector;

        Font font = Font.font("Verdana", FontWeight.BOLD, height / 4d);
        Text textPrint = new Text();
        textPrint.setText(text + " (Default " + String.valueOf(defaultValue).replaceAll("\\.0*$", "") + ") : " + value);
        textPrint.setFill(Color.WHITESMOKE);
        textPrint.setFont(font);
        textPrint.setX(xStart + width / 2);
        textPrint.setY(yStart + height / 8);
        textPrint.setX(textPrint.getX() - textPrint.getLayoutBounds().getWidth() / 2);
        textPrint.setY(textPrint.getY() + textPrint.getLayoutBounds().getHeight() / 4);

        pane.getChildren().add(textPrint);

        nodes[2] = pane;

        return nodes;
    }

    public void select(int xClick, int yClick) {
        if (hasBeenClicked(xClick, yClick)) {
            this.isSelected = true;
        }
    }

    public void unselect() {
        this.isSelected = false;
    }
}
