package org.tyranobast.spacedodger.client.menus.multiplayer.hub;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.Transmission;

public class ServerWaiterMenu {
    private Timeline serverWaiter;
    private final int maxTimeout;
    private int timeout;
    private final Group gameGroup;
    private final int gameScreenScale;
    private final int screenWidth;
    private final int screenHeight;
    private Text waitingText;
    private boolean isServerWaiterMenuOn;

    public ServerWaiterMenu(Group gameGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        this.gameGroup = gameGroup;
        this.gameScreenScale = gameScreenScale;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.maxTimeout = 15;
        Scene scene = gameGroup.getScene();
        this.isServerWaiterMenuOn = false;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            if (isServerWaiterMenuOn) {
                goBack();
            }
        });
    }

    private void waitForServer() {
        this.isServerWaiterMenuOn = true;

        gameGroup.getChildren().clear();

        Font font = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 20d);

        waitingText = new Text("Connecting to server... 0 / " + maxTimeout + "\nClick to go back");
        waitingText.setFont(font);
        waitingText.setFill(Color.WHITESMOKE);
        waitingText.setX(screenWidth / 2d);
        waitingText.setY(screenHeight / 2d);
        waitingText.setX(waitingText.getX() - waitingText.getLayoutBounds().getWidth() / 2);
        waitingText.setY(waitingText.getY() + waitingText.getLayoutBounds().getHeight() / 2);
        gameGroup.getChildren().add(waitingText);

        MainClient.networkerClient.sendMessage("ping " + MainClient.onlineToken, "util");

        serverWaiter = new Timeline(new KeyFrame(Duration.seconds(1), event -> mainServerWaiterLoop()));
        serverWaiter.setCycleCount(Timeline.INDEFINITE);
    }

    private void mainServerWaiterLoop() {
        String messagePong = MainClient.networkerClient.getMessage(Transmission.Util, (String data) ->
                data.equals("pong"));
        if (!messagePong.equals("")) {
            stopServerWaiterMenu();
            this.isServerWaiterMenuOn = false;
            MainClient.multiplayerHubMenu.menuStart();
        } else if (timeout >= maxTimeout) {
            stopServerWaiterMenu();
            serverTimedOut();
        } else {
            timeout++;
            waitingText.setText("Connecting to server... " + timeout + " / " + maxTimeout + "\nClick to go back");
        }
    }

    public void startServerWaiterMenu() {
        waitForServer();
        serverWaiter.play();
    }

    private void serverTimedOut() {
        gameGroup.getChildren().clear();

        Font font = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 20d);

        Text timedOutText = new Text("Server timed out ! Can't connect !\nClick to go back");
        timedOutText.setFont(font);
        timedOutText.setFill(Color.WHITESMOKE);
        timedOutText.setX(screenWidth / 2d);
        timedOutText.setY(screenHeight / 2d);
        timedOutText.setX(timedOutText.getX() - timedOutText.getLayoutBounds().getWidth() / 2);
        timedOutText.setY(timedOutText.getY() + timedOutText.getLayoutBounds().getHeight() / 2);
        gameGroup.getChildren().add(timedOutText);
    }

    private void goBack() {
        stopServerWaiterMenu();
        this.isServerWaiterMenuOn = false;
        MainClient.menuTransition.menuStart("level");
    }

    private void stopServerWaiterMenu() {
        serverWaiter.stop();
    }
}
