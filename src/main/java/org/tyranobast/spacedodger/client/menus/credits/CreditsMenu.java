package org.tyranobast.spacedodger.client.menus.credits;

import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.menus.Menu;

public class CreditsMenu extends Menu {

    private CreditsMenuText creditsMenuText;

    public CreditsMenu(Group gameGroup, int gameScreenScale, int width, int height) {
        super(gameGroup, gameScreenScale, height, width);
        this.creditsMenuText = new CreditsMenuText(height, width);
    }

    @Override
    protected void mousePressed(MouseEvent event) {
        menuStop();
        MainClient.menuTransition.menuStart("menu");
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        scene.setFill(Color.MEDIUMSEAGREEN);

        this.creditsMenuText = new CreditsMenuText(screenHeight, screenWidth);
    }

    @Override
    protected void menuLoop() {
        gameGroup.getChildren().clear();
        creditsMenuText.changeOrientation(menuFPS);
        gameGroup.getChildren().add(creditsMenuText.display());
    }
}
