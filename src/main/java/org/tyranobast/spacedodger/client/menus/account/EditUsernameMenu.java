package org.tyranobast.spacedodger.client.menus.account;

import javafx.scene.Group;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.Transmission;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

public class EditUsernameMenu extends Menu {

    private final Group GUIGroup;
    private TextField newUsername;
    private boolean waitingForServer;

    public EditUsernameMenu(Group gameGroup, Group GUIGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);

        this.GUIGroup = GUIGroup;
        this.waitingForServer = false;
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        this.waitingForServer = false;

        scene.setFill(Color.PALEVIOLETRED);

        double buttonWidth = screenWidth * 2 / 7d;
        double buttonHeight = screenHeight / 5d;

        menuButtons.clear();

        Pane textPane = new Pane();
        Font font = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 20d);
        newUsername = new TextField();
        newUsername.setFont(font);
        newUsername.setText("username");
        newUsername.setLayoutX(screenWidth / 2d - buttonWidth / 2d);
        newUsername.setLayoutY(screenHeight / 5d);
        newUsername.setPrefWidth(buttonWidth);
        newUsername.setPrefHeight(buttonHeight);

        textPane.getChildren().addAll(newUsername);

        GUIGroup.getChildren().add(textPane);

        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 3 / 5d, buttonHeight, buttonWidth, "Validate", "validate"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight * 3 / 5d, buttonHeight, buttonWidth, "Cancel", "cancel"));
    }

    private void verifyingData(String newUsername) {
        gameGroup.getChildren().clear();
        menuButtons.get(0).changeText("Waiting for server");

        MainClient.networkerClient.sendMessage("edit_username " + MainClient.onlineToken + " " + newUsername, "setting");
        waitingForServer = true;
    }

    @Override
    protected void menuLoop() {

        String messageEdit = MainClient.networkerClient.getMessage(Transmission.Setting, (String data) ->
                data.split(" ")[0].equals(MainClient.onlineToken) &&
                        data.split(" ")[1].equals("edit_username"));
        if (waitingForServer && !messageEdit.equals("")) {
            waitingForServer = false;
            if (messageEdit.split(" ")[2].equals("ok")) {
                menuStop();
                GUIGroup.getChildren().clear();
                GUIGroup.getChildren().add(gameGroup);
                MainClient.menuTransition.menuStart("account");
            } else {
                menuButtons.get(0).changeText("Request refused");
            }
        }

        gameGroup.getChildren().clear();

        displayButtons:
        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {

                switch (menuButton.getData()) {
                    case "cancel" -> {
                        menuStop();
                        gameGroup.getChildren().clear();
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        GUIGroup.getChildren().clear();
                        GUIGroup.getChildren().add(gameGroup);
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                    case "validate" -> {
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        verifyingData(newUsername.getText());
                    }
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}