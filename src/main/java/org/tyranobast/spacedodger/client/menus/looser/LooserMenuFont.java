package org.tyranobast.spacedodger.client.menus.looser;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

class LooserMenuFont {

    private final int score;
    private boolean isZooming;
    private double zoom;

    public LooserMenuFont(int score) {
        this.score = score;
        this.isZooming = false;
    }

    public void changeZoom() {
        if (isZooming && zoom < 2 * Math.PI) {
            zoom = zoom + 2 * Math.PI / 60;
        } else if (!isZooming && zoom > 0) {
            zoom = zoom - 2 * Math.PI / 60;
        } else {
            isZooming = !isZooming;
        }
    }

    public Pane display(double gameScreenScale, double width, double height) {

        Pane pane = new Pane();

        Font font = Font.font("Verdana", FontWeight.BOLD, (gameScreenScale / 20) * Math.abs(Math.sin(zoom)));
        Text textPrint = new Text();
        textPrint.setText("YOU LOSE\nYour score was : " + score);
        textPrint.setFill(Color.WHITESMOKE);
        textPrint.setOpacity(Math.abs(Math.sin(zoom)));
        textPrint.setFont(font);
        textPrint.setX(width / 2);
        textPrint.setY(height / 2);
        textPrint.setX(textPrint.getX() - textPrint.getLayoutBounds().getWidth() / 2);
        textPrint.setY(textPrint.getY() + textPrint.getLayoutBounds().getHeight() / 4);

        pane.getChildren().add(textPrint);

        return pane;
    }
}
