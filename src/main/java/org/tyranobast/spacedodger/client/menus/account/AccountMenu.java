package org.tyranobast.spacedodger.client.menus.account;

import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

public class AccountMenu extends Menu {

    public AccountMenu(Group gameGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        scene.setFill(Color.PALEVIOLETRED);

        menuButtons.clear();

        double buttonWidth = screenWidth / 2d;

        if (!MainClient.isLogged) {
            double buttonHeight = screenHeight / 7d;
            menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight / 7d, buttonHeight, buttonWidth, "Sign up", "sign"));
            menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight * 3 / 7d, buttonHeight, buttonWidth, "Log in", "log_in"));
            menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Go back", "back"));
        } else {
            double buttonHeight = screenHeight / 9d;
            menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight / 9d, buttonHeight, buttonWidth, "Edit username", "edit_username"));
            menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight * 3 / 9d, buttonHeight, buttonWidth, "Edit password", "edit_password"));
            menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight * 5 / 9d, buttonHeight, buttonWidth, "Log out", "log_out"));
            menuButtons.add(new MenuButton(screenWidth / 4d, screenHeight * 7 / 9d, buttonHeight, buttonWidth, "Go back", "back"));
        }
    }

    @Override
    protected void menuLoop() {
        gameGroup.getChildren().clear();

        displayButtons:
        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {
                xClick = 0;
                yClick = 0;
                xMouse = 0;
                yMouse = 0;
                menuStop();
                gameGroup.getChildren().clear();
                switch (menuButton.getData()) {
                    case "edit_username" -> {
                        MainClient.menuTransition.menuStart("edit_username");
                        break displayButtons;
                    }
                    case "edit_password" -> {
                        MainClient.menuTransition.menuStart("edit_password");
                        break displayButtons;
                    }
                    case "log_in" -> {
                        MainClient.menuTransition.menuStart("log_in");
                        break displayButtons;
                    }
                    case "log_out" -> {
                        MainClient.networkerClient.sendMessage("log_out " + MainClient.onlineToken, "setting");
                        MainClient.onlineToken = "";
                        MainClient.isLogged = false;
                        MainClient.menuTransition.menuStart("account");
                        break displayButtons;
                    }
                    case "sign" -> {
                        MainClient.menuTransition.menuStart("sign_up");
                        break displayButtons;
                    }
                    case "back" -> {
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}