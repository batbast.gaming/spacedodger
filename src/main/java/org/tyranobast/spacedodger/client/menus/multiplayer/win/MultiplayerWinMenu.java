package org.tyranobast.spacedodger.client.menus.multiplayer.win;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;

public class MultiplayerWinMenu {

    private final Group gameGroup;
    private final int gameScreenScale;
    private final int width;
    private final int height;
    private final Scene scene;
    private final Timeline menuTimeline;
    private MultiplayerWinMenuFont multiplayerWinMenuFont;
    private boolean isWinMenuOn;

    public MultiplayerWinMenu(Group gameGroup, int gameScreenScale, int width, int height) {
        this.gameGroup = gameGroup;
        this.gameScreenScale = gameScreenScale;
        this.scene = gameGroup.getScene();
        this.width = width;
        this.height = height;
        this.multiplayerWinMenuFont = new MultiplayerWinMenuFont();

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            if (isWinMenuOn) {
                winMenuStop();
                MainClient.menuTransition.menuStart("menu");
            }
        });

        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(0.1), event -> mainWinMenuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    private void initWinMenu() {
        this.multiplayerWinMenuFont = new MultiplayerWinMenuFont();
        scene.setFill(Color.SLATEGRAY);
        isWinMenuOn = true;
    }

    public void winMenuStart() {
        initWinMenu();
        menuTimeline.play();
    }

    private void winMenuStop() {
        menuTimeline.stop();
        isWinMenuOn = false;
    }

    private void mainWinMenuLoop() {
        gameGroup.getChildren().clear();
        multiplayerWinMenuFont.changeZoom();
        gameGroup.getChildren().add(multiplayerWinMenuFont.display(gameScreenScale, width, height));
    }
}
