package org.tyranobast.spacedodger.client.menus.account;

import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.ShaMaker;
import org.tyranobast.spacedodger.client.Transmission;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

import java.util.UUID;

public class LogInMenu extends Menu {

    private final Group GUIGroup;
    private TextField usernameField;
    private PasswordField passwordField;
    private Label passwordLabel;
    private Tooltip passwordTooltip;
    private CheckBox passwordCheckbox;
    private boolean waitingForServer;
    private String tmpUUID;
    private String username;

    public LogInMenu(Group gameGroup, Group GUIGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);
        this.GUIGroup = GUIGroup;
        this.waitingForServer = false;
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        this.tmpUUID = UUID.randomUUID().toString();

        this.waitingForServer = false;

        scene.setFill(Color.PALEVIOLETRED);

        double buttonWidth = screenWidth * 2 / 7d;
        double buttonHeight = screenHeight / 7d;

        menuButtons.clear();

        Pane textPane = new Pane();
        Font font = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 30d);
        Font tooltipFont = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 40d);
        Font labelFont = Font.font("Verdana", FontWeight.BOLD, buttonHeight / 10d);

        usernameField = new TextField();
        usernameField.setFont(font);
        usernameField.setText("username");
        usernameField.setLayoutX(screenWidth * 5 / 14d);
        usernameField.setLayoutY(screenHeight / 7d);
        usernameField.setPrefWidth(buttonWidth);
        usernameField.setPrefHeight(buttonHeight);

        passwordTooltip = new Tooltip();
        passwordTooltip.setFont(tooltipFont);

        passwordCheckbox = new CheckBox();
        passwordCheckbox.setPrefWidth(buttonWidth / 10);
        passwordCheckbox.setPrefHeight(buttonHeight / 10);
        passwordCheckbox.setLayoutX(screenWidth * 5 / 14d);
        passwordCheckbox.setLayoutY(screenHeight * 3 / 7d + buttonHeight);
        passwordCheckbox.setOnAction(event -> {
            if (passwordCheckbox.isSelected()) {
                passwordTooltip.setText(passwordField.getText());
                passwordTooltip.show(passwordField, screenWidth * 5 / 14d + buttonWidth, screenHeight * 3 / 7d + buttonHeight);
                passwordLabel.setText("Hide password");
            } else {
                passwordTooltip.hide();
                passwordLabel.setText("Show password");
            }
        });

        passwordLabel = new Label("Show password");
        passwordLabel.setFont(labelFont);
        passwordLabel.setLayoutX(screenWidth * 5 / 14d + buttonWidth / 15);
        passwordLabel.setLayoutY(screenHeight * 3 / 7d + buttonHeight);

        passwordField = new PasswordField();
        passwordField.setFont(font);
        passwordField.setPromptText("password");
        passwordField.setLayoutX(screenWidth * 5 / 14d);
        passwordField.setLayoutY(screenHeight * 3 / 7d);
        passwordField.setPrefWidth(buttonWidth);
        passwordField.setPrefHeight(buttonHeight);
        passwordField.setOnKeyTyped(e -> {
            if (passwordCheckbox.isSelected()) {
                passwordTooltip.setText(passwordField.getText());
                passwordTooltip.setX(screenWidth * 5 / 14d + buttonWidth);
                passwordTooltip.setY(screenHeight * 3 / 7d + buttonHeight);
            }
        });
        textPane.getChildren().addAll(usernameField, passwordField, passwordLabel, passwordCheckbox);

        GUIGroup.getChildren().add(textPane);

        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Validate", "validate"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Cancel", "cancel"));
    }

    private void verifyingData(String rawUsername, String password) {
        gameGroup.getChildren().clear();
        menuButtons.get(0).changeText("Waiting for server");

        String hashedPassword = ShaMaker.getSHA(password);

        this.username = rawUsername;

        MainClient.networkerClient.sendMessage("log_in " + tmpUUID + " " + rawUsername + " " + hashedPassword, "setting");
        waitingForServer = true;
    }

    @Override
    protected void menuLoop() {

        String messageLogin = MainClient.networkerClient.getMessage(Transmission.Setting, (String data) ->
                data.split(" ")[0].equals(this.tmpUUID) &&
                        data.split(" ")[1].equals("log_in"));

        if (waitingForServer && !messageLogin.equals("")) {
            waitingForServer = false;
            if (messageLogin.split(" ")[2].equals("ok")) {
                menuStop();
                GUIGroup.getChildren().clear();
                GUIGroup.getChildren().add(gameGroup);
                MainClient.onlineToken = messageLogin.split(" ")[3];
                MainClient.isLogged = true;
                MainClient.onlineUsername = username;
                MainClient.menuTransition.menuStart("account");
            } else {
                menuButtons.get(0).changeText("Bad credentials");
            }
        }

        gameGroup.getChildren().clear();

        displayButtons:
        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {

                switch (menuButton.getData()) {
                    case "cancel" -> {
                        menuStop();
                        gameGroup.getChildren().clear();
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        GUIGroup.getChildren().clear();
                        GUIGroup.getChildren().add(gameGroup);
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                    case "validate" -> {
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        verifyingData(usernameField.getText(), passwordField.getText());
                    }
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}