package org.tyranobast.spacedodger.client.menus;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuText {

    protected boolean isZooming;
    protected double zoom;
    protected final int height;
    protected final int width;
    protected String text;
    protected double orientation;

    public MenuText(int screenHeight, int screenWidth, String text) {
        this.height = screenHeight;
        this.width = screenWidth;
        this.text = text;
        this.isZooming = false;
        this.orientation = 0;
        this.zoom = 1;
    }

    public void changeText(String text) {
        this.text = text;
    }

    public void changeZoom(int fps) {
        if (isZooming && zoom < 2 * Math.PI) {
            zoom = zoom + (2 * Math.PI / 60) * 10d / fps;
        } else if (!isZooming && zoom > 0){
            zoom = zoom - (2 * Math.PI / 60) * 10d / fps;
        } else {
            isZooming = !isZooming;
        }
    }

    public void changeOrientation(int fps) {
        if (orientation >= 2 * Math.PI) {
            orientation = 0;
        }
        orientation = orientation + (2 * Math.PI / 90) * 10d / fps;
    }

    public Pane display() {
        Pane pane = new Pane();

        Font font = Font.font("Verdana", FontWeight.BOLD, (height / 20d) * Math.abs(Math.sin(zoom)));
        Text textPrint = new Text();
        textPrint.setText(text);
        textPrint.setFill(Color.WHITESMOKE);
        textPrint.setOpacity(Math.abs(Math.sin(zoom)));
        textPrint.setRotate(Math.sin(orientation) * 5);
        textPrint.setFont(font);
        textPrint.setX(width / 2d);
        textPrint.setY(height / 2d);
        textPrint.setX(textPrint.getX() - textPrint.getLayoutBounds().getWidth() / 2);
        textPrint.setY(textPrint.getY() + textPrint.getLayoutBounds().getHeight() / 4);

        pane.getChildren().add(textPrint);

        return pane;
    }
}