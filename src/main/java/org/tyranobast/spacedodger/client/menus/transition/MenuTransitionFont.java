package org.tyranobast.spacedodger.client.menus.transition;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineJoin;

import java.util.ArrayList;

class MenuTransitionFont {

    private int iteration;
    private double functionAYPrev;
    private double functionBYPrev;
    private double functionAY;
    private double functionBY;
    private final int width;
    private final int height;
    private final double iterationSinMax;
    private final double offset;
    private double finalCountDown;
    private final double iterationMax;
    private final ArrayList<Line> lines;

    public MenuTransitionFont(int width, int height) {
        this.finalCountDown = 0;
        this.width = width;
        this.height = height;
        this.iterationMax = width / 2d + height / (Math.PI * 2) + 10;
        this.iterationSinMax = width / 2d;

        this.lines = new ArrayList<>();

        this.offset = (width % Math.PI) * height / 2d;

        this.iteration = 0;

        this.functionAYPrev = height / 2d;
        this.functionBYPrev = height / 2d;

        functionAY = height / 2d + Math.sin(Math.toRadians(iteration + offset)) * height / 2;
        functionBY = height / 2d + Math.sin(Math.toRadians((width - iteration) + offset)) * height / 2;
    }

    public void addLines(int gameScreenScale) {
        if (iteration < iterationSinMax) {
            functionAYPrev = functionAY;
            functionBYPrev = functionBY;

            functionAY = height / 2d + Math.sin(Math.toRadians(iteration + offset)) * height / 2;
            functionBY = height / 2d + Math.sin(Math.toRadians((width - iteration) + offset)) * height / 2;

            Line lineFunctionA = new Line();
            lineFunctionA.setStartX(iteration - 1);
            lineFunctionA.setStartY(functionAYPrev);
            lineFunctionA.setEndX(iteration);
            lineFunctionA.setEndY(functionAY);
            lineFunctionA.setStrokeWidth(gameScreenScale / 500d);
            lineFunctionA.setStroke(Color.ORANGERED);
            lineFunctionA.setStrokeLineJoin(StrokeLineJoin.ROUND);

            lines.add(lineFunctionA);

            Line lineFunctionB = new Line();
            lineFunctionB.setStartX(width - iteration + 1);
            lineFunctionB.setStartY(functionBYPrev);
            lineFunctionB.setEndX(width - iteration);
            lineFunctionB.setEndY(functionBY);
            lineFunctionB.setStrokeWidth(gameScreenScale / 500d);
            lineFunctionB.setStroke(Color.ORANGERED);
            lineFunctionB.setStrokeLineJoin(StrokeLineJoin.ROUND);

            lines.add(lineFunctionB);

            Line lineFunctionC = new Line();
            lineFunctionC.setStartX(iteration - 1);
            lineFunctionC.setStartY(height / 2d - (functionAYPrev - height / 2d));
            lineFunctionC.setEndX(iteration);
            lineFunctionC.setEndY(height / 2d - (functionAY - height / 2d));
            lineFunctionC.setStrokeWidth(gameScreenScale / 500d);
            lineFunctionC.setStroke(Color.ORANGERED);
            lineFunctionC.setStrokeLineJoin(StrokeLineJoin.ROUND);

            lines.add(lineFunctionC);

            Line lineFunctionD = new Line();
            lineFunctionD.setStartX(width - iteration + 1);
            lineFunctionD.setStartY(height / 2d - (functionBYPrev - height / 2d));
            lineFunctionD.setEndX(width - iteration);
            lineFunctionD.setEndY(height / 2d - (functionBY - height / 2d));
            lineFunctionD.setStrokeWidth(gameScreenScale / 500d);
            lineFunctionD.setStroke(Color.ORANGERED);
            lineFunctionD.setStrokeLineJoin(StrokeLineJoin.ROUND);

            lines.add(lineFunctionD);

        } else if (iteration > iterationMax) {
            finalCountDown++;
        }

        for (int i = 0; i < lines.size(); i++) {
            if (lines.get(i).getStartX() < iteration - height / (Math.PI * 2) || lines.get(i).getStartX() > width - iteration + height / (Math.PI * 2)) {
                lines.remove(lines.get(i));
            }
        }

        iteration++;
    }

    public boolean isFinished() {
        return finalCountDown > 200;
    }

    public ArrayList<Line> display() {
        return lines;
    }
}
