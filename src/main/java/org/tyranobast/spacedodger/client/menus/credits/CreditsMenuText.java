package org.tyranobast.spacedodger.client.menus.credits;

import org.tyranobast.spacedodger.client.menus.MenuText;

import java.io.InputStream;
import java.util.Scanner;

class CreditsMenuText extends MenuText {

    public CreditsMenuText(int screenHeight, int screenWidth) {
        super(screenHeight, screenWidth, "");
        changeText(loadResources());
    }

    private String loadResources() {
        String path = "/credits.txt";

        InputStream resource = this.getClass().getResourceAsStream(path);

        if (resource == null) {
            return "error : no credits found";
        }

        Scanner scanner = new Scanner(resource).useDelimiter("\\A");

        StringBuilder builder = new StringBuilder();
        while (scanner.hasNext()) {
            builder.append(scanner.next());
        }

        if (builder.isEmpty()) {
            return "error : no credits found";
        } else {
            return builder.toString();
        }
    }
}
