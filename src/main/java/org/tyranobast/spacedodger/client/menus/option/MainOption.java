package org.tyranobast.spacedodger.client.menus.option;

import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;
import org.tyranobast.spacedodger.client.menus.MenuSlider;
import org.tyranobast.spacedodger.client.menus.MenuSwitch;

import java.util.ArrayList;

public class MainOption extends Menu {

    private final ArrayList<MenuSlider> menuSliders;
    private final ArrayList<MenuSwitch> menuSwitches;
    private final Group GUIGroup;
    private boolean isClicked;
    private int xDrag;

    public MainOption(Group gameGroup, Group GUIGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);

        this.menuSliders = new ArrayList<>();
        this.menuSwitches = new ArrayList<>();
        this.GUIGroup = GUIGroup;
        this.isClicked = false;

        scene.addEventHandler(MouseEvent.MOUSE_DRAGGED, mouseEvent -> this.xDrag = (int) mouseEvent.getX());

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            this.isClicked = true;
            this.xDrag = (int) mouseEvent.getX();
        });

        scene.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseEvent -> this.isClicked = false);
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        scene.setFill(Color.PALEVIOLETRED);

        double buttonWidth = screenWidth * 2 / 7d;
        double buttonHeight = screenHeight / 9d;

        menuSliders.clear();

        menuSliders.add(new MenuSlider(screenWidth / 7d, screenHeight / 9d, buttonHeight, buttonWidth,
                "Game FPS", "game_fps"));
        menuSliders.get(0).setStep(1);
        menuSliders.get(0).setMinMax(1, 120);
        menuSliders.get(0).setDefaultValue(MainClient.config.getIntDefaultValue("game_fps"));
        menuSliders.get(0).setInitValues(MainClient.config.getIntValue("game_fps"));

        menuSliders.add(new MenuSlider(screenWidth * 4 / 7d, screenHeight / 9d, buttonHeight, buttonWidth,
                "Menu FPS", "menu_fps"));
        menuSliders.get(1).setStep(1);
        menuSliders.get(1).setMinMax(1, 120);
        menuSliders.get(1).setDefaultValue(MainClient.config.getIntDefaultValue("menu_fps"));
        menuSliders.get(1).setInitValues(MainClient.config.getIntValue("menu_fps"));

        menuSliders.add(new MenuSlider(screenWidth / 7d, screenHeight * 3 / 9d, buttonHeight, buttonWidth,
                "Networking rate", "network_rate"));
        menuSliders.get(2).setStep(1);
        menuSliders.get(2).setMinMax(5, 30);
        menuSliders.get(2).setDefaultValue(MainClient.config.getIntDefaultValue("network_rate"));
        menuSliders.get(2).setInitValues(MainClient.config.getIntValue("network_rate"));

        menuSwitches.clear();

        menuSwitches.add(new MenuSwitch(screenWidth * 4 / 7d, screenHeight * 3 / 9d, buttonWidth, buttonHeight, "control"));
        menuSwitches.get(0).setOption1("Control by click", "click");
        menuSwitches.get(0).setOption2("Control with mouse", "mouse");
        menuSwitches.get(0).setOrigin(MainClient.config.getStringValue("control").equals("click") ? (byte) 0 : (byte) 1);

        menuButtons.clear();

        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 7 / 9d, buttonHeight, buttonWidth, "Save on disk", "write"));
        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 5 / 9d, buttonHeight, buttonWidth, "Cancel", "back"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight * 5 / 9d, buttonHeight, buttonWidth, "Apply", "apply"));
    }

    private void menuSave() {
        for (MenuSlider menuSlider : menuSliders) {
            MainClient.config.setValue(menuSlider.getData(), menuSlider.getValue());
        }
        for (MenuSwitch menuSwitch : menuSwitches) {
            MainClient.config.setValue(menuSwitch.getValueName(), menuSwitch.getData());
        }
    }

    @Override
    protected void menuLoop() {
        gameGroup.getChildren().clear();

        for (MenuSwitch menuSwitch : menuSwitches) {
            menuSwitch.move(xClick, yClick, this.menuFPS);
            gameGroup.getChildren().addAll(menuSwitch.display());
        }

        for (MenuSlider menuSlider : menuSliders) {
            if (isClicked) {
                menuSlider.select(xClick, yClick);
            } else {
                menuSlider.unselect();
            }

            menuSlider.move(xDrag);

            gameGroup.getChildren().addAll(menuSlider.display());
        }
        displayButtons:
        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {
                xMouse = 0;
                yMouse = 0;
                menuStop();
                gameGroup.getChildren().clear();
                GUIGroup.getChildren().clear();
                GUIGroup.getChildren().add(gameGroup);
                switch (menuButton.getData()) {
                    case "write" -> {
                        menuSave();
                        MainClient.config.updateExternalConfigFile();
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                    case "apply" -> {
                        menuSave();
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                    case "back" -> {
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
        xClick = 0;
        yClick = 0;
    }
}
