package org.tyranobast.spacedodger.client.menus;

import javafx.scene.Node;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuSwitch {

    private final double xStart;
    private final double yStart;
    private final double height;
    private String text1;
    private String text2;
    private String dataName1;
    private String dataName2;
    private final double width;
    private int data;
    private double cursorX;
    private double zoom;
    private boolean travelDone;
    private final String valueName;

    public MenuSwitch(double xStart, double yStart, double width, double height, String valueName) {
        this.text1 = "on";
        this.text2 = "off";
        this.data = 1;
        this.xStart = xStart + (width - height * 5 / 2d) / 2;
        this.yStart = yStart;
        this.height = height;
        this.width = height * 5 / 2;
        this.zoom = 20;
        this.travelDone = true;
        this.valueName = valueName;
    }

    public void setOption1(String text1, String dataName) {
        this.text1 = text1;
        this.dataName1 = dataName;
    }

    public void setOption2(String text2, String dataName) {
        this.text2 = text2;
        this.dataName2 = dataName;
    }

    public void setOrigin(byte option) {
        if (option == 0 || option == 1) {
            data = option;
            this.cursorX = option == 1 ? 1 : 99;
        }
    }

    public String getValueName() {
        return valueName;
    }

    public String getData() {
        if (data == 1) {
            return dataName1;
        } else {
            return dataName2;
        }
    }

    public void move(int xClick, int yClick, int menuFPS) {
        if ((xClick > xStart && xClick < xStart + width) && (yClick > yStart && yClick < yStart + height)) {
            travelDone = false;
            if (data == 1) {
                data = 2;
            } else {
                data = 1;
            }
        }
        double speedBoost = (Math.abs(Math.sin(Math.toRadians((cursorX / 200d) * 360))) * 25) + 0.1;
        if (data == 2) {
            if (!travelDone) {
                if (cursorX < 100 && zoom > 2) {
                    zoom = zoom - 2 * 10d / menuFPS;
                } else if (cursorX < 100) {
                    cursorX = cursorX + speedBoost * 10d / menuFPS;
                } else if (zoom < 20) {
                    zoom = zoom + 2 * 10d / menuFPS;
                } else {
                    travelDone = true;
                }
            }
        } else {
            if (!travelDone) {
                if (cursorX > 0 && zoom > 2) {
                    zoom = zoom - 2 * 10d / menuFPS;
                } else if (cursorX > 0) {
                    cursorX = cursorX - speedBoost * 10d / menuFPS;
                } else if (zoom < 20) {
                    zoom = zoom + 2 * 10d / menuFPS;
                } else {
                    travelDone = true;
                }
            }
        }

        //temp debug
        if (zoom < 1) {
            zoom = 1;
        }
        if (zoom > 20) {
            zoom = 20;
        }

        if (cursorX > 100) {
            cursorX = 100;
        }
        if (cursorX < 0) {
            cursorX = 0;
        }
    }

    public Node[] display() {

        double margin = height / 16;

        Pane pane = new Pane();
        Node[] nodes = new Node[8];

        Rectangle middleFont = new Rectangle();
        middleFont.setX(xStart + width / 5);
        middleFont.setY(yStart + height / 2);
        middleFont.setWidth(width * 3 / 5);
        middleFont.setHeight(height / 2);
        middleFont.setFill(Color.WHITESMOKE);

        nodes[0] = middleFont;

        Circle circleFontLeft = new Circle();
        circleFontLeft.setCenterX(xStart + width / 5);
        circleFontLeft.setCenterY(yStart + height * 3 / 4);
        circleFontLeft.setRadius(height / 4 * 13 / 10d);
        circleFontLeft.setFill(Color.WHITESMOKE);

        nodes[1] = circleFontLeft;

        Circle circleFontRight = new Circle();
        circleFontRight.setCenterX(xStart + width * 4 / 5);
        circleFontRight.setCenterY(yStart + height * 3 / 4);
        circleFontRight.setRadius(height / 4 * 13 / 10d);
        circleFontRight.setFill(Color.WHITESMOKE);

        nodes[2] = circleFontRight;

        Rectangle middle = new Rectangle();
        middle.setX(xStart + width / 5 + margin);
        middle.setY(yStart + height / 2 + margin);
        middle.setWidth(width * 3 / 5 - margin * 2);
        middle.setHeight(height / 2 - margin * 2);
        middle.setFill(Color.LIMEGREEN);

        nodes[3] = middle;

        Circle circleLeft = new Circle();
        circleLeft.setCenterX(xStart + width / 5);
        circleLeft.setCenterY(yStart + height * 3 / 4);
        circleLeft.setRadius(height / 4 * 13 / 10d - margin);
        circleLeft.setFill(Color.LIMEGREEN);

        nodes[4] = circleLeft;

        Circle circleRight = new Circle();
        circleRight.setCenterX(xStart + width * 4 / 5);
        circleRight.setCenterY(yStart + height * 3 / 4);
        circleRight.setRadius(height / 4 * 13 / 10d - margin);
        circleRight.setFill(Color.LIMEGREEN);

        nodes[5] = circleRight;

        Circle cursor = new Circle();
        cursor.setCenterX(xStart + width / 5 + (width * 3 / 5) * (cursorX / 100));
        cursor.setCenterY(yStart + height * 3 / 4);
        cursor.setRadius((height / 4 + (height / 4 * 3 / 10d * zoom / 20d)) - 2 * margin);
        cursor.setFill(Color.WHITESMOKE);

        nodes[6] = cursor;

        Font font = Font.font("Verdana", FontWeight.BOLD, height / 4d);
        Text textPrint = new Text();
        textPrint.setText(data == 1 ? text1 : text2);
        textPrint.setFill(Color.WHITESMOKE);
        textPrint.setFont(font);
        textPrint.setX(xStart + width / 2);
        textPrint.setY(yStart + height / 4);
        textPrint.setX(textPrint.getX() - textPrint.getLayoutBounds().getWidth() / 2);
        textPrint.setY(textPrint.getY() + textPrint.getLayoutBounds().getHeight() / 4);

        pane.getChildren().add(textPrint);

        nodes[7] = pane;

        return nodes;
    }
}
