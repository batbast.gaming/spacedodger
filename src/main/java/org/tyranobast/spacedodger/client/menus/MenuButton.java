package org.tyranobast.spacedodger.client.menus;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuButton {

    private double glowScale;
    private boolean isSelected;
    private final double xStart;
    private final double yStart;
    private final double width;
    private final double height;
    private final Rectangle rectangle;
    private String text;
    private final String data;
    private boolean isShown;

    public MenuButton(double xStart, double yStart, double height, double width, String text, String data) {
        this.data = data;
        this.text = text;
        this.glowScale = 0;
        this.xStart = xStart;
        this.yStart = yStart;
        this.width = width;
        this.height = height;
        this.isSelected = false;
        this.isShown = true;
        this.rectangle = new Rectangle();
    }

    public boolean isShown() {
        return isShown;
    }

    public void hide() {
        this.isShown = false;
    }

    public void show() {
        this.isShown = true;
    }

    public void changeGlow(int xMouse, int yMouse, int fps) {
        isSelected = (xMouse > xStart && xMouse < xStart + width) && (yMouse > yStart && yMouse < yStart + height);

        if (isSelected && glowScale < 10) {
            glowScale = glowScale + 10d / fps;
        } else if (!isSelected && glowScale > 0) {
            glowScale = glowScale - 10d / fps;
        }
    }

    public void changeText(String newText) {
        this.text = newText;
    }

    public String getData() {
        return data;
    }

    public boolean hasBeenClicked(int xClick, int yClick) {
        if (isShown) {
            return (xClick > xStart && xClick < xStart + width) && (yClick > yStart && yClick < yStart + height);
        } else {
            return false;
        }
    }

    public Node[] display(double gameScreenScale, Color color) {
        if (isShown) {
            double zoom = gameScreenScale * glowScale / 150d;

            Pane pane = new Pane();
            Node[] nodes = new Node[6];

            rectangle.setX(xStart - zoom / 2);
            rectangle.setY(yStart - zoom / 2);
            rectangle.setWidth(width + zoom);
            rectangle.setHeight(height + zoom);
            rectangle.setFill(color);
            rectangle.setOpacity(0.7 + 3 * glowScale / 100d);

            nodes[0] = rectangle;

            Line lineUp = new Line();
            lineUp.setStartX(xStart - zoom / 2);
            lineUp.setStartY(yStart - zoom / 2);
            lineUp.setEndX(xStart + width + zoom / 2);
            lineUp.setEndY(yStart - zoom / 2);
            lineUp.setStrokeWidth(gameScreenScale / 100d);
            lineUp.setStroke(Color.WHITESMOKE);

            nodes[1] = lineUp;

            Line lineLeft = new Line();
            lineLeft.setStartX(xStart - zoom / 2);
            lineLeft.setStartY(yStart - zoom / 2);
            lineLeft.setEndX(xStart - zoom / 2);
            lineLeft.setEndY(yStart + height + zoom / 2);
            lineLeft.setStrokeWidth(gameScreenScale / 100d);
            lineLeft.setStroke(Color.WHITESMOKE);

            nodes[2] = lineLeft;

            Line lineDown = new Line();
            lineDown.setStartX(xStart - zoom / 2);
            lineDown.setStartY(yStart + height + zoom / 2);
            lineDown.setEndX(xStart + width + zoom / 2);
            lineDown.setEndY(yStart + height + zoom / 2);
            lineDown.setStrokeWidth(gameScreenScale / 100d);
            lineDown.setStroke(Color.WHITESMOKE);

            nodes[3] = lineDown;

            Line lineRight = new Line();
            lineRight.setStartX(xStart + width + zoom / 2);
            lineRight.setStartY(yStart - zoom / 2);
            lineRight.setEndX(xStart + width + zoom / 2);
            lineRight.setEndY(yStart + height + zoom / 2);
            lineRight.setStrokeWidth(gameScreenScale / 100d);
            lineRight.setStroke(Color.WHITESMOKE);

            nodes[4] = lineRight;

            Font font = Font.font("Verdana", FontWeight.BOLD, height / 3);
            Text textPrint = new Text();
            textPrint.setText(text);
            textPrint.setFill(Color.WHITESMOKE);
            textPrint.setFont(font);
            textPrint.setX(xStart + width / 2);
            textPrint.setY(yStart + height / 2);
            textPrint.setX(textPrint.getX() - textPrint.getLayoutBounds().getWidth() / 2);
            textPrint.setY(textPrint.getY() + textPrint.getLayoutBounds().getHeight() / 4);

            pane.getChildren().add(textPrint);

            nodes[5] = pane;

            return nodes;
        } else {
            return new Node[0];
        }
    }
}