package org.tyranobast.spacedodger.client.menus.account;

import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.ShaMaker;
import org.tyranobast.spacedodger.client.Transmission;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

import java.util.UUID;

public class SignUpMenu extends Menu {

    private final Group GUIGroup;
    private TextField username;
    private PasswordField password;
    private PasswordField passwordConfirmation;
    private CheckBox passwordCheckBox;
    private CheckBox passwordConfirmCheckBox;
    private Tooltip newPasswordTooltip;
    private Tooltip passwordConfirmTooltip;
    private Label passwordLabel;
    private Label passwordConfirmLabel;
    private boolean waitingForServer;
    private String tmpUUID;

    public SignUpMenu(Group gameGroup, Group GUIGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);

        this.GUIGroup = GUIGroup;
        this.waitingForServer = false;
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        this.tmpUUID = UUID.randomUUID().toString();

        this.waitingForServer = false;

        scene.setFill(Color.PALEVIOLETRED);

        double buttonWidth = screenWidth * 2 / 7d;
        double buttonHeight = screenHeight / 7d;

        menuButtons.clear();

        Pane textPane = new Pane();
        Font font = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 30d);
        Font tooltipFont = Font.font("Verdana", FontWeight.BOLD, gameScreenScale / 40d);
        Font labelFont = Font.font("Verdana", FontWeight.BOLD, buttonHeight / 10d);

        username = new TextField();
        username.setFont(font);
        username.setText("username");
        username.setLayoutX(screenWidth * 5 / 14d);
        username.setLayoutY(screenHeight / 7d);
        username.setPrefWidth(buttonWidth);
        username.setPrefHeight(buttonHeight);

        newPasswordTooltip = new Tooltip();
        newPasswordTooltip.setFont(tooltipFont);

        passwordCheckBox = new CheckBox();
        passwordCheckBox.setPrefWidth(buttonWidth / 10);
        passwordCheckBox.setPrefHeight(buttonHeight / 10);
        passwordCheckBox.setLayoutX(screenWidth / 7d);
        passwordCheckBox.setLayoutY(screenHeight * 3 / 7d + buttonHeight);
        passwordCheckBox.setOnAction(event -> {
            if (passwordCheckBox.isSelected()) {
                newPasswordTooltip.setText(password.getText());
                newPasswordTooltip.show(password, screenWidth / 7d + buttonWidth, screenHeight * 3 / 7d + buttonHeight);
                passwordLabel.setText("Hide password");
            } else {
                newPasswordTooltip.hide();
                passwordLabel.setText("Show password");
            }
        });

        passwordLabel = new Label("Show password");
        passwordLabel.setFont(labelFont);
        passwordLabel.setLayoutX(screenWidth / 7d + buttonWidth / 15);
        passwordLabel.setLayoutY(screenHeight * 3 / 7d + buttonHeight);

        password = new PasswordField();
        password.setFont(font);
        password.setPromptText("new password");
        password.setLayoutX(screenWidth / 7d);
        password.setLayoutY(screenHeight * 3 / 7d);
        password.setPrefWidth(buttonWidth);
        password.setPrefHeight(buttonHeight);
        password.setOnKeyTyped(e -> {
            if (passwordCheckBox.isSelected()) {
                newPasswordTooltip.setText(password.getText());
                newPasswordTooltip.setX(screenWidth / 7d + buttonWidth);
                newPasswordTooltip.setY(screenHeight * 3 / 7d + buttonHeight);
            }
        });

        passwordConfirmTooltip = new Tooltip();
        passwordConfirmTooltip.setFont(tooltipFont);

        passwordConfirmCheckBox = new CheckBox();
        passwordConfirmCheckBox.setPrefWidth(buttonWidth / 10);
        passwordConfirmCheckBox.setPrefHeight(buttonHeight / 10);
        passwordConfirmCheckBox.setLayoutX(screenWidth * 4 / 7d);
        passwordConfirmCheckBox.setLayoutY(screenHeight * 3 / 7d + buttonHeight);
        passwordConfirmCheckBox.setOnAction(event -> {
            if (passwordConfirmCheckBox.isSelected()) {
                passwordConfirmTooltip.setText(passwordConfirmation.getText());
                passwordConfirmTooltip.show(password, screenWidth * 4 / 7d + buttonWidth, screenHeight * 3 / 7d + buttonHeight);
                passwordConfirmLabel.setText("Hide password");
            } else {
                passwordConfirmTooltip.hide();
                passwordConfirmLabel.setText("Show password");
            }
        });

        passwordConfirmLabel = new Label("Show password");
        passwordConfirmLabel.setFont(labelFont);
        passwordConfirmLabel.setLayoutX(screenWidth * 4 / 7d + buttonWidth / 15);
        passwordConfirmLabel.setLayoutY(screenHeight * 3 / 7d + buttonHeight);

        passwordConfirmation = new PasswordField();
        passwordConfirmation.setFont(font);
        passwordConfirmation.setPromptText("confirmation");
        passwordConfirmation.setLayoutX(screenWidth * 4 / 7d);
        passwordConfirmation.setLayoutY(screenHeight * 3 / 7d);
        passwordConfirmation.setPrefWidth(buttonWidth);
        passwordConfirmation.setPrefHeight(buttonHeight);
        passwordConfirmation.setOnKeyTyped(e -> {
            if (passwordConfirmCheckBox.isSelected()) {
                passwordConfirmTooltip.setText(passwordConfirmation.getText());
                passwordConfirmTooltip.setX(screenWidth * 4 / 7d + buttonWidth);
                passwordConfirmTooltip.setY(screenHeight * 3 / 7d + buttonHeight);
            }
        });

        textPane.getChildren().addAll(username, password, passwordCheckBox, passwordLabel);
        textPane.getChildren().addAll(passwordConfirmation, passwordConfirmCheckBox, passwordConfirmLabel);

        GUIGroup.getChildren().add(textPane);

        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Validate", "validate"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Cancel", "cancel"));
    }

    private void verifyingData(String username, String password) {
        gameGroup.getChildren().clear();
        menuButtons.get(0).changeText("Waiting for server");

        String hashedPassword = ShaMaker.getSHA(password);

        MainClient.networkerClient.sendMessage("sign_up " + tmpUUID + " " + username + " " + hashedPassword, "setting");
        waitingForServer = true;
    }

    @Override
    protected void menuLoop() {

        String messageSignup = MainClient.networkerClient.getMessage(Transmission.Setting, (String data) ->
                data.split(" ")[0].equals(this.tmpUUID) &&
                        data.split(" ")[1].equals("sign_up"));

        if (waitingForServer && !messageSignup.equals("")) {
            waitingForServer = false;
            if (messageSignup.split(" ")[2].equals("ok")) {
                menuStop();
                GUIGroup.getChildren().clear();
                GUIGroup.getChildren().add(gameGroup);
                MainClient.menuTransition.menuStart("account");
                menuButtons.get(0).changeText("Validate");
            } else {
                System.out.println("bad password or username");
                menuButtons.get(0).changeText("Name already in use");
            }
        }


        gameGroup.getChildren().clear();

        displayButtons:
        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {

                switch (menuButton.getData()) {
                    case "cancel" -> {
                        menuStop();
                        GUIGroup.getChildren().clear();
                        GUIGroup.getChildren().add(gameGroup);
                        gameGroup.getChildren().clear();
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        MainClient.menuTransition.menuStart("menu");
                        break displayButtons;
                    }
                    case "validate" -> {
                        xClick = 0;
                        yClick = 0;
                        xMouse = 0;
                        yMouse = 0;
                        if (password.getText().equals(passwordConfirmation.getText())) {
                            verifyingData(username.getText(), password.getText());
                        } else {
                            menuButtons.get(0).changeText("Invalid passwords");
                        }
                    }
                }
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}