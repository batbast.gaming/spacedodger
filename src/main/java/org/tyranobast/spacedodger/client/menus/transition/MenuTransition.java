package org.tyranobast.spacedodger.client.menus.transition;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.menus.Menu;

public class MenuTransition extends Menu {

    private MenuTransitionFont menuTransitionFont;
    private String nextClass;
    private String data;

    public MenuTransition(Group gameGroup, int screenWidth, int screenHeight, int gameScreenScale) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);
        int transitionSpeed = MainClient.config.getIntValue("transition_speed");
        this.menuTransitionFont = new MenuTransitionFont(screenWidth, screenHeight);
        this.nextClass = "";
        this.data = "";

        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / transitionSpeed), event -> menuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    protected void reloadConfig() {
        int transitionSpeed = MainClient.config.getIntValue("transition_speed");
        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(1d / transitionSpeed), event -> menuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    @Override
    protected void initMenu() {

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        reloadConfig();
        gameGroup.getChildren().clear();
        scene.setFill(Color.BLACK);
        this.menuTransitionFont = new MenuTransitionFont(screenWidth, screenHeight);
    }

    public void menuStart(String nextClass, String... data) {
        this.nextClass = nextClass;
        initMenu();
        menuTimeline.play();
        if (data.length != 0) {
            this.data = data[0];
        } else {
            this.data = "";
        }
    }

    protected void menuStop() {
        menuTimeline.stop();
        switch (nextClass) {
            case "game" -> {
                if (Integer.parseInt(data) == 4) {
                    MainClient.serverWaiterMenu.startServerWaiterMenu();
                } else {
                    MainClient.gameLevelX.get(Integer.parseInt(data) - 1).startGame();
                }
            }
            case "credits" -> MainClient.creditsMenu.menuStart();
            case "menu" -> MainClient.mainMenu.menuStart();
            case "looser" -> MainClient.looserMenu.looserMenuStart(data); //todo add config for menu speed
            case "level" -> MainClient.levelMenu.menuStart();
            case "win" -> MainClient.multiplayerWinMenu.winMenuStart(); //todo add config for menu speed
            case "gameEnd" -> MainClient.multiplayerGameEndMenu.gameEndMenuStart(data); //todo add config for menu speed
            case "account" -> MainClient.accountMenu.menuStart();
            case "sign_up" -> MainClient.signUpMenu.menuStart();
            case "log_in" -> MainClient.logInMenu.menuStart();
            case "option" -> MainClient.mainOption.menuStart();
            case "edit_username" -> MainClient.editUsernameMenu.menuStart();
            case "edit_password" -> MainClient.editPasswordMenu.menuStart();
        }
    }

    @Override
    protected void menuLoop() {
        menuTransitionFont.addLines(gameScreenScale);

        gameGroup.getChildren().clear();

        Circle centerCircle = new Circle();
        centerCircle.setCenterX(screenWidth / 2d);
        centerCircle.setCenterY(screenHeight / 2d);
        centerCircle.setRadius(screenHeight / 12d);
        centerCircle.setFill(Color.ORANGERED);
        gameGroup.getChildren().add(centerCircle);

        if (menuTransitionFont.isFinished()) {
            menuStop();
        } else {
            gameGroup.getChildren().addAll(menuTransitionFont.display());
        }
    }
}
