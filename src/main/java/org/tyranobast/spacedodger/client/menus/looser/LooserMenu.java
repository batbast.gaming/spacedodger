package org.tyranobast.spacedodger.client.menus.looser;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;

public class LooserMenu {

    private final Group gameGroup;
    private final int gameScreenScale;
    private final int width;
    private final int height;
    private final Scene scene;
    private final Timeline menuTimeline;
    private int score;
    private LooserMenuFont looserMenuFont;
    private boolean isLooserMenuOn;

    public LooserMenu(Group gameGroup, int gameScreenScale, int width, int height) {
        this.gameGroup = gameGroup;
        this.gameScreenScale = gameScreenScale;
        this.scene = gameGroup.getScene();
        this.width = width;
        this.height = height;
        this.looserMenuFont = new LooserMenuFont(score);

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            if (isLooserMenuOn) {
                looserMenuStop();
                MainClient.menuTransition.menuStart("menu");
            }
        });

        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(0.1), event -> mainLooserMenuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    private void initLooserMenu() {
        this.looserMenuFont = new LooserMenuFont(score);
        scene.setFill(Color.SLATEGRAY);
        isLooserMenuOn = true;
    }

    public void looserMenuStart(String score) {
        this.score = Integer.parseInt(score);
        initLooserMenu();
        menuTimeline.play();
    }

    private void looserMenuStop() {
        menuTimeline.stop();
        isLooserMenuOn = false;
    }

    private void mainLooserMenuLoop() {
        gameGroup.getChildren().clear();
        looserMenuFont.changeZoom();
        gameGroup.getChildren().add(looserMenuFont.display(gameScreenScale, width, height));
    }
}
