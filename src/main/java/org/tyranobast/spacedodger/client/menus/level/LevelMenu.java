package org.tyranobast.spacedodger.client.menus.level;

import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.tyranobast.spacedodger.client.MainClient;
import org.tyranobast.spacedodger.client.menus.Menu;
import org.tyranobast.spacedodger.client.menus.MenuButton;

public class LevelMenu extends Menu {

    public LevelMenu(Group gameGroup, int gameScreenScale, int screenHeight, int screenWidth) {
        super(gameGroup, gameScreenScale, screenHeight, screenWidth);
    }

    @Override
    protected void initMenu() {
        xClick = 0;
        yClick = 0;
        xMouse = 0;
        yMouse = 0;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEvent);
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEvent);

        scene.setFill(Color.PALEVIOLETRED);

        double buttonWidth = screenWidth * 2 / 7d;
        double buttonHeight = screenHeight / 7d;

        menuButtons.clear();

        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight / 7d, buttonHeight, buttonWidth, "Level 1", "1"));
        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 3 / 7d, buttonHeight, buttonWidth, "Level 2", "2"));
        menuButtons.add(new MenuButton(screenWidth / 7d, screenHeight * 5 / 7d, buttonHeight, buttonWidth, "Level 3", "3"));

        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight / 7d, buttonHeight, buttonWidth, "Multiplayer", "4"));
        menuButtons.add(new MenuButton(screenWidth * 4 / 7d, screenHeight * 3 / 7d, buttonHeight, buttonWidth, "Go back", "back"));

    }


    @Override
    protected void menuLoop() {
        gameGroup.getChildren().clear();

        for (MenuButton menuButton : menuButtons) {
            menuButton.changeGlow(xMouse, yMouse, menuFPS);
            if (menuButton.hasBeenClicked(xClick, yClick)) {
                xClick = 0;
                yClick = 0;
                xMouse = 0;
                yMouse = 0;
                menuStop();
                gameGroup.getChildren().clear();
                if (menuButton.getData().equals("back")) {
                    MainClient.menuTransition.menuStart("menu");
                } else {
                    MainClient.menuTransition.menuStart("game", menuButton.getData());
                }
                break;
            }
            gameGroup.getChildren().addAll(menuButton.display(gameScreenScale, (Color) scene.getFill()));
        }
    }
}