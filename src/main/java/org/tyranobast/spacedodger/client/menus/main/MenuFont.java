package org.tyranobast.spacedodger.client.menus.main;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineJoin;
import org.tyranobast.spacedodger.client.Random;

import java.util.ArrayList;

class MenuFont {

    private double actualAngle;
    private double addAngle;
    private double actualStep;
    private final double addStep;
    private double prevX;
    private double prevY;
    private double actualX;
    private double actualY;
    private double[][] pointsCoordinates;
    private int[][] lineColor;
    private final int gameScreenScale;
    private final int gameScreenX;
    private final int gameScreenY;
    private final int initColorR;
    private final int initColorG;
    private final int initColorB;
    private final int distance;
    private double iteration;

    public MenuFont(int gameScreenScale, int gameScreenX, int gameScreenY, double minSpacing, double maxSpacing) {
        this.gameScreenScale = gameScreenScale;
        this.gameScreenX = gameScreenX;
        this.gameScreenY = gameScreenY;
        this.addStep = Random.random(minSpacing, maxSpacing) / 10d;
        this.addAngle = Random.random(30, 330);
        this.actualAngle = 0;
        this.actualX = 0;
        this.actualY = 0;
        this.actualStep = 0;
        this.prevX = 0;
        this.prevY = 0;
        this.initColorR = (int) (Random.random(0, 255));
        this.initColorG = (int) (Random.random(0, 255));
        this.initColorB = (int) (Random.random(0, 255));
        this.distance = (int) Math.hypot(600, 600);
        this.iteration = calculateIterations();
        this.pointsCoordinates = new double[calculateIterations() + 1][2];
        this.pointsCoordinates[0][0] = 0;
        this.pointsCoordinates[0][1] = 0;
        this.lineColor = new int[calculateIterations()][3];
    }

    private int calculateIterations() {
        double x = 0;
        double y = 0;

        int iterations = 1;

        while (!(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) > distance)) {
            x = x + ((iterations * addStep) * Math.toDegrees(Math.cos(iterations * Math.toRadians(addAngle))));
            y = y + ((iterations * addStep) * Math.toDegrees(Math.sin(iterations * Math.toRadians(addAngle))));
            iterations++;
        }

        return iterations;
    }

    public void changeDraw(double speed, int speedReduction) {
        if (speed != 0) {
            this.iteration = calculateIterations();
            this.pointsCoordinates = new double[calculateIterations() + 1][2];
            this.pointsCoordinates[0][0] = 0;
            this.pointsCoordinates[0][1] = 0;
            this.lineColor = new int[calculateIterations()][3];

            if (speedReduction != 0) {
                speed = speed / (iteration / speedReduction);
            }

            if (addAngle >= 30 && addAngle <= 330) {
                addAngle = addAngle + speed;
            } else {
                double speedBoost;
                double coefficient;

                if (addAngle < 30) {
                    coefficient = addAngle;
                } else {
                    coefficient = -(360 - addAngle);
                }

                coefficient = 30 + coefficient;

                speedBoost = Math.sin(Math.toRadians((coefficient / 60d) * 180));

                addAngle = addAngle + speed + speed * (10 * speedBoost);
            }
            if (addAngle >= 360) {
                addAngle = 0;
            }
        }

        calculateDraw();
    }

    public int[] calculateDraw() {

        int colorR = initColorR;
        int colorG = initColorG;
        int colorB = initColorB;

        double addBrightnessColorR = (colorR) / iteration;
        double addBrightnessColorG = (colorG) / iteration;
        double addBrightnessColorB = (colorB) / iteration;

        colorR = 255;
        colorG = 255;
        colorB = 255;

        this.actualAngle = 0;
        this.actualX = 0;
        this.actualY = 0;
        this.actualStep = 0;
        this.prevX = 0;
        this.prevY = 0;

        double colorRBrightness = 0;
        double colorGBrightness = 0;
        double colorBBrightness = 0;

        for (int i = 0; i < iteration; i++) {

            colorRBrightness = colorRBrightness - addBrightnessColorR;
            colorGBrightness = colorGBrightness - addBrightnessColorG;
            colorBBrightness = colorBBrightness - addBrightnessColorB;

            prevX = actualX;
            prevY = actualY;

            actualAngle = actualAngle + addAngle;
            if (actualAngle >= 360) {
                actualAngle = actualAngle - 360;
            }

            actualStep = actualStep + addStep;

            actualX = actualX + (actualStep * Math.toDegrees(Math.cos(Math.toRadians(actualAngle))));
            actualY = actualY + (actualStep * Math.toDegrees(Math.sin(Math.toRadians(actualAngle))));

            pointsCoordinates[i + 1][0] = (int) actualX;
            pointsCoordinates[i + 1][1] = (int) actualY;
            lineColor[i][0] = (int) (colorR + colorRBrightness);
            lineColor[i][1] = (int) (colorG + colorGBrightness);
            lineColor[i][2] = (int) (colorB + colorBBrightness);

            Line line = new Line();
            line.setStartX(gameScreenX + (double) gameScreenScale / 1000 * (prevX + 500));
            line.setStartY(gameScreenY + (double) gameScreenScale / 1000 * (prevY + 500));
            line.setEndX(gameScreenX + (double) gameScreenScale / 1000 * (actualX + 500));
            line.setEndY(gameScreenY + (double) gameScreenScale / 1000 * (actualY + 500));
            line.setStrokeWidth(gameScreenScale / 100d);
            line.setStroke(Color.rgb((int) (colorR + colorRBrightness), (int) (colorG + colorGBrightness), (int) (colorB + colorBBrightness)));
        }

        return new int[]{(int) (colorR + colorRBrightness), (int) (colorG + colorGBrightness), (int) (colorB + colorBBrightness)};
    }

    public void rotate(double angleInDegrees) {
        for (int i = 0; i < pointsCoordinates.length; i++) {
            double[] newCoordinates = rotatePoint(pointsCoordinates[i][0], pointsCoordinates[i][1], angleInDegrees);
            pointsCoordinates[i][0] = newCoordinates[0];
            pointsCoordinates[i][1] = newCoordinates[1];
        }
    }

    private double[] rotatePoint(double pointToRotateX, double pointToRotateY, double angleInDegrees) {
        double centerPointX = 0;
        double centerPointY = 0;
        double angleInRadians = Math.toRadians(angleInDegrees);
        double cosTheta = Math.cos(angleInRadians);
        double sinTheta = Math.sin(angleInRadians);

        double x = cosTheta * (pointToRotateX - centerPointX) - sinTheta * (pointToRotateY - centerPointY) + centerPointX;
        double y = sinTheta * (pointToRotateX - centerPointX) + cosTheta * (pointToRotateY - centerPointY) + centerPointY;

        return new double[]{x, y};
    }

    public ArrayList<Line> display() {
        ArrayList<Line> lines = new ArrayList<>();

        for (int i = 0; i < iteration; i++) {
            Line line = new Line();
            line.setStartX(gameScreenX + (double) gameScreenScale / 1000 * (pointsCoordinates[i][0] + 500));
            line.setStartY(gameScreenY + (double) gameScreenScale / 1000 * (pointsCoordinates[i][1] + 500));
            line.setEndX(gameScreenX + (double) gameScreenScale / 1000 * (pointsCoordinates[i + 1][0] + 500));
            line.setEndY(gameScreenY + (double) gameScreenScale / 1000 * (pointsCoordinates[i + 1][1] + 500));
            line.setStrokeWidth(gameScreenScale / 100d);
            line.setStroke(Color.rgb(lineColor[i][0], lineColor[i][1], lineColor[i][2]));
            line.setStrokeLineJoin(StrokeLineJoin.ROUND);

            lines.add(line);
        }
        return lines;
    }
}
