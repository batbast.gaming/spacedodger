package org.tyranobast.spacedodger.client.menus.multiplayer.gameend;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.tyranobast.spacedodger.client.MainClient;

public class MultiplayerGameEndMenu {

    private final Group gameGroup;
    private final int gameScreenScale;
    private final int width;
    private final int height;
    private final Scene scene;
    private final Timeline menuTimeline;
    private String winner;
    private MultiplayerGameEndMenuFont multiplayerGameEndMenuFont;
    private boolean isGameEndMenuOn;

    public MultiplayerGameEndMenu(Group gameGroup, int gameScreenScale, int width, int height) {
        this.gameGroup = gameGroup;
        this.gameScreenScale = gameScreenScale;
        this.scene = gameGroup.getScene();
        this.width = width;
        this.height = height;

        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            if (isGameEndMenuOn) {
                gameEndMenuStop();
                MainClient.menuTransition.menuStart("menu");
            }
        });

        menuTimeline = new Timeline(new KeyFrame(Duration.seconds(0.1), event -> mainGameEndMenuLoop()));
        menuTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    private void initGameEndMenu() {
        this.multiplayerGameEndMenuFont = new MultiplayerGameEndMenuFont(winner);
        scene.setFill(Color.SLATEGRAY);
        isGameEndMenuOn = true;
    }

    public void gameEndMenuStart(String winner) {
        this.winner = winner;
        initGameEndMenu();
        menuTimeline.play();
    }

    private void gameEndMenuStop() {
        menuTimeline.stop();
        isGameEndMenuOn = false;
    }

    private void mainGameEndMenuLoop() {
        gameGroup.getChildren().clear();
        multiplayerGameEndMenuFont.changeZoom();
        gameGroup.getChildren().add(multiplayerGameEndMenuFont.display(gameScreenScale, width, height));
    }
}
