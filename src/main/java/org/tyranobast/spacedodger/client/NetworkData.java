package org.tyranobast.spacedodger.client;

import java.util.Date;

public record NetworkData(Date receiveDate, String data, Transmission type) {
}
