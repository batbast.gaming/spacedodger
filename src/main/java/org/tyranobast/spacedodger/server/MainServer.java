package org.tyranobast.spacedodger.server;

import java.util.ArrayList;

public class MainServer {
    public static MainGameServer mainGameServer;
    public static MainSettingServer mainSettingServer;
    public static NetworkerServer networkerServer;
    public static ArrayList<String[]> userTempUUID;
    public static DataBaseManager dataBaseManager;

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(MainServer::shutdown));

        userTempUUID = new ArrayList<>();
        mainGameServer = new MainGameServer();
        mainSettingServer = new MainSettingServer();
        networkerServer = new NetworkerServer();
        dataBaseManager = new DataBaseManager();
    }

    private static void shutdown() {
        networkerServer.closeNetworker();
    }
}
