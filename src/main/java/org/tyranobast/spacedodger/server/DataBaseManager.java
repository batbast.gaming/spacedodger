package org.tyranobast.spacedodger.server;

import java.io.File;
import java.net.URISyntaxException;
import java.sql.*;

public class DataBaseManager {

    public DataBaseManager() {
        createTable();
    }

    private void createTable() {
        Connection connection;
        Statement statement;

        try {
            connection = getDataBaseConnection();
            statement = connection.createStatement();
            String sql = "CREATE TABLE USERS " +
                    "(UUID      TEXT  PRIMARY KEY  NOT NULL," +
                    " USERNAME  TEXT               NOT NULL, " +
                    " PASSWORD  TEXT               NOT NULL)";
            statement.executeUpdate(sql);
            statement.close();
            connection.close();
        } catch (Exception ignored) {
            //Table USERS already exist
        }
    }

    private Connection getDataBaseConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");

        File externalConfigLocation = null;

        try {
            File externalConfigFile = new File(DataBaseManager.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            externalConfigLocation = new File(externalConfigFile.getParent() + File.separator + "users.db");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return DriverManager.getConnection("jdbc:sqlite:" + externalConfigLocation);
    }

    public boolean createUser(String username, String UUID, String password) {

        String sqlCommand = "INSERT INTO USERS (UUID,USERNAME,PASSWORD) VALUES(?,?,?)";

        try {
            Connection connection = getDataBaseConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand);
            preparedStatement.setString(1, UUID);
            preparedStatement.setString(2, username);
            preparedStatement.setString(3, password);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void deleteUser(String UUID) {

        String sqlCommand = "DELETE FROM USERS WHERE UUID='?'";

        try {
            Connection connection = getDataBaseConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand);
            preparedStatement.setString(1, UUID);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean updateUserPassword(String UUID, String newPassword) {

        String sqlCommand = "UPDATE USERS SET PASSWORD = ? WHERE UUID = ?";

        try (Connection connection = getDataBaseConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, newPassword);
            preparedStatement.setString(2, UUID);
            preparedStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public String logInUser(String username, String password) {
        String sqlCommand = "SELECT UUID FROM USERS WHERE USERNAME=? AND PASSWORD=?";

        ResultSet resultSet;

        try (Connection connection = getDataBaseConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("UUID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getPassword(String username) {
        String sqlCommand = "SELECT PASSWORD FROM USERS WHERE USERNAME=?";

        ResultSet resultSet;

        try (Connection connection = getDataBaseConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("PASSWORD");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean updateUsername(String UUID, String newUsername) {
        if (!usernameExist(newUsername)) {

            String sqlCommand = "UPDATE USERS SET USERNAME = ? WHERE UUID = ?";

            try (Connection connection = getDataBaseConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
                preparedStatement.setString(1, newUsername);
                preparedStatement.setString(2, UUID);
                preparedStatement.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        } else {
            return false;
        }
    }

    public boolean usernameExist(String username) {

        String sqlCommand = "SELECT * FROM USERS WHERE USERNAME=?";

        ResultSet resultSet;

        try (Connection connection = getDataBaseConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}