package org.tyranobast.spacedodger.server;

import org.tyranobast.spacedodger.client.Random;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainGameServer {

    private final ArrayList<String> connectedUsers;
    private final ArrayList<String> inGameUsers;
    private final ArrayList<String> messageHistory;
    private final ArrayList<String[]> userCoordinates;
    private final ArrayList<String[]> userTimeout;
    private final Timer timer;
    private final int minPlayerCount;
    private final int maxDistance;
    private final int coordinateSendRate;
    private final int maxTimeout;
    private boolean hasCurrentStartRequest;
    private boolean gameLaunched;

    public MainGameServer() {
        connectedUsers = new ArrayList<>();
        inGameUsers = new ArrayList<>();
        messageHistory = new ArrayList<>();
        timer = new Timer();
        userCoordinates = new ArrayList<>();
        userTimeout = new ArrayList<>();
        minPlayerCount = 2;
        maxDistance = 150;
        hasCurrentStartRequest = false;
        coordinateSendRate = 10;
        maxTimeout = 5;
        gameLaunched = false;
    }

    private void initPlayersData() {
        String currentTime = String.valueOf(System.currentTimeMillis());
        for (String inGameUser : inGameUsers) {
            userCoordinates.add(new String[]{inGameUser, "0", "0", "0"});
            userTimeout.add(new String[]{inGameUser, currentTime});
        }
    }

    private void gameStop(String winner) {

        if (winner.equals("nobody")) {
            MainServer.networkerServer.sendMessage("winner nobody", "util");
        } else {
            for (String[] playerData : MainServer.userTempUUID) {
                if (playerData[1].equals(winner)) {
                    MainServer.networkerServer.sendMessage("winner " + playerData[0], "util");
                    break;
                }
            }
        }
        timer.cancel();

        connectedUsers.clear();
        inGameUsers.clear();
        messageHistory.clear();
        userCoordinates.clear();
        userTimeout.clear();

        gameLaunched = false;
    }

    private int[] genWalls() {

        int[] walls = new int[(int) ((maxDistance / 15d) * 4)];

        for (int i = 0; i < walls.length; ) {
            walls[i] = (int) Random.random(-450, 300);
            i++;
            walls[i] = (int) Random.random(-450, 300);
            i++;
            walls[i] = (int) Random.random(walls[i - 2] + 150, 450);
            i++;
            walls[i] = (int) Random.random(walls[i - 2] + 150, 450);
            i++;
        }

        return walls;
    }

    private void gameStart() {
        gameLaunched = true;
        inGameUsers.clear();

        inGameUsers.addAll(connectedUsers);

        initPlayersData();

        StringBuilder toSend = new StringBuilder("game launched>");

        for (String inGameUser : inGameUsers) {
            for (String[] playerData : MainServer.userTempUUID) {
                if (playerData[1].equals(inGameUser)) {
                    toSend.append(playerData[0]).append(" ");
                    break;
                }
            }
        }

        toSend.append("&").append(maxDistance).append("&");

        for (int wall : genWalls()) {
            toSend.append(wall).append(" ");
        }

        MainServer.networkerServer.sendMessage(toSend.toString(), "util");

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                mainServerLoop();
            }
        }, 0, 1000 / coordinateSendRate);
    }

    private void mainServerLoop() {
        StringBuilder toSend = new StringBuilder();

        if (inGameUsers.size() == 1) {
            gameStop(inGameUsers.get(0));
            return;
        } else if (inGameUsers.size() == 0) {
            gameStop("nobody");
            return;
        }

        for (String[] data : userCoordinates) {
            for (String[] playerData : MainServer.userTempUUID) {
                if (playerData[1].equals(data[0])) {
                    toSend.append(playerData[0]).append(" ").append(data[1]).append(" ").append(data[2]).append(" ").append(data[3]);
                    toSend.append("£");
                    break;
                }
            }
        }

        long time = System.currentTimeMillis();

        for (int i = 0; i < userTimeout.size(); i++) {
            if (time - Long.parseLong(userTimeout.get(i)[1]) > maxTimeout * 1000L) {
                inGameUsers.remove(userTimeout.get(i)[0]);

                if (inGameUsers.size() == 1) {
                    gameStop(inGameUsers.get(0));
                } else if (inGameUsers.size() == 0) {
                    gameStop("nobody");
                } else {
                    userTimeout.remove(i);
                    System.out.println(i + " removed");
                }
                break;
            }
        }

        toSend.deleteCharAt(toSend.length() - 1);


        MainServer.networkerServer.sendMessage(toSend.toString(), "coordinate");
    }

    private void sendPlayerList() {

        StringBuilder toSend = new StringBuilder();

        if (connectedUsers.size() != 0) {
            for (String user : connectedUsers) {
                for (String[] playerData : MainServer.userTempUUID) {
                    if (playerData[1].equals(user)) {
                        toSend.append(playerData[0]).append("\n");
                        break;
                    }
                }
            }

            toSend.deleteCharAt(toSend.length() - 1);

            MainServer.networkerServer.sendMessage("users>" + toSend, "message");
        }
    }

    private void utilMessageReceived(String message) {

        String[] data = message.split(" ");

        switch (data[0]) {
            case "start" -> {
                if (connectedUsers.size() >= minPlayerCount && !hasCurrentStartRequest && !gameLaunched) {
                    hasCurrentStartRequest = true;
                    connectedUsers.clear();

                    MainServer.networkerServer.sendMessage("ping", "util");

                    Timer t = new java.util.Timer();
                    t.schedule(new java.util.TimerTask() {
                        @Override
                        public void run() {
                            if (connectedUsers.size() >= minPlayerCount) {
                                gameStart();
                            } else {
                                MainServer.networkerServer.sendMessage("noStart not enough players", "util");
                            }
                            hasCurrentStartRequest = false;
                            t.cancel();
                        }
                    }, 5000);
                } else {
                    MainServer.networkerServer.sendMessage("noStart not enough players", "util");
                }
            }
            case "loose" -> {

                for (String[] playerData : MainServer.userTempUUID) {
                    if (playerData[1].equals(data[1])) {
                        MainServer.networkerServer.sendMessage("dead " + playerData[0], "util");
                        break;
                    }
                }

                inGameUsers.remove(data[1]);

                if (inGameUsers.size() == 1) {
                    gameStop(inGameUsers.get(0));
                } else if (inGameUsers.size() == 0) {
                    gameStop("nobody");
                }
            }
            case "join", "pong" -> {
                if (!gameLaunched) {
                    for (String[] playerData : MainServer.userTempUUID) {
                        if (playerData[1].equals(data[1])) {
                            connectedUsers.add(data[1]);
                        }
                    }
                }
            }
            case "ping" -> {
                if (!gameLaunched) {
                    for (String[] playerData : MainServer.userTempUUID) {
                        if (playerData[1].equals(data[1])) {
                            MainServer.networkerServer.sendMessage("pong", "util");
                        }
                    }
                }
            }
            case "getUserList" -> sendPlayerList();
            case "leave" -> connectedUsers.remove(data[1]);
            case "win" -> {
                for (String[] playerData : MainServer.userTempUUID) {
                    if (playerData[1].equals(data[1])) {
                        gameStop(data[1]);
                    }
                }
            }
        }
    }

    public void messageReceived(String receivedMessage, String type) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now) + " received message = " + receivedMessage + " type = " + type);
        System.out.println();

        if (messageHistory.size() > 1000) {
            messageHistory.remove(0);
        }
        messageHistory.add(receivedMessage);

        switch (type) {
            case "toServerUtil" -> utilMessageReceived(receivedMessage);
            case "toServerCoordinate" -> saveCoordinateData(receivedMessage);
            case "toServerMessage" -> {
            }
            default -> System.err.println("Unexpected message format");
        }
    }

    private void saveCoordinateData(String message) {
        String[] toAdd = message.split(" ");

        for (int i = 0; i < userTimeout.size(); i++) {
            if (userTimeout.get(i)[0].equals(toAdd[0])) {
                userTimeout.set(i, new String[]{userTimeout.get(i)[0], String.valueOf(System.currentTimeMillis())});
                break;
            }
        }

        for (int i = 0; i < userCoordinates.size(); i++) {
            if (userCoordinates.get(i)[0].equals(toAdd[0])) {

                if (toAdd.length > 4) {
                    System.err.println("too much data send by user " + userCoordinates.get(i)[0].equals(toAdd[0]));
                } else {
                    userCoordinates.set(i, toAdd);
                }

                break;
            }
        }
    }
}
