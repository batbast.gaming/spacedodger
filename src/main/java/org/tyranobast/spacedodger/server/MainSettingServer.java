package org.tyranobast.spacedodger.server;

import org.mindrot.jbcrypt.BCrypt;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class MainSettingServer {

    private final ArrayList<String> messageHistory;
    private final ArrayList<String> waitingForPong;
    private final int playerMaxTimeout;

    public MainSettingServer() {
        this.messageHistory = new ArrayList<>();
        this.waitingForPong = new ArrayList<>();
        this.playerMaxTimeout = 5000; //milliseconds
    }

    public void messageReceived(String receivedMessage) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now) + " received message = " + receivedMessage + " type = setting");
        System.out.println();

        if (messageHistory.size() > 1000) {
            messageHistory.remove(0);
        }
        messageHistory.add(receivedMessage);

        String type = receivedMessage.split(" ")[0];
        switch (type) {
            case "sign_up" -> signUp(receivedMessage);
            case "log_in" -> logIn(receivedMessage);
            case "log_out" -> logOut(receivedMessage);
            case "edit_username" -> editUsername(receivedMessage);
            case "edit_password" -> editPassword(receivedMessage);
            case "pong" -> waitingForPong.add(receivedMessage.split(" ")[1]);
        }
    }

    private void editUsername(String receivedMessage) {
        String[] messageSplit = receivedMessage.split(" ");
        String token = messageSplit[1];
        for (int i = 0; i < MainServer.userTempUUID.size(); i++) {
            if (MainServer.userTempUUID.get(i)[1].equals(token)) {
                if (MainServer.dataBaseManager.updateUsername(MainServer.userTempUUID.get(i)[2], messageSplit[2])) {
                    MainServer.networkerServer.sendMessage(token + " edit_username ok", "setting");
                } else {
                    MainServer.networkerServer.sendMessage(token + " edit_username fail", "setting");
                }
                break;
            }
        }
    }

    private void editPassword(String receivedMessage) {

        String[] messageSplit = receivedMessage.split(" ");
        String token = messageSplit[1];

        String password = BCrypt.hashpw(messageSplit[2], BCrypt.gensalt());

        for (int i = 0; i < MainServer.userTempUUID.size(); i++) {
            if (MainServer.userTempUUID.get(i)[1].equals(token)) {
                if (MainServer.dataBaseManager.updateUserPassword(MainServer.userTempUUID.get(i)[2], password)) {
                    MainServer.networkerServer.sendMessage(token + " edit_password ok", "setting");
                } else {
                    MainServer.networkerServer.sendMessage(token + " edit_password fail", "setting");
                }
                break;
            }
        }
    }

    private void signUp(String receivedMessage) {

        // TODO: 28/11/2021 add minimal pseudonym size

        String[] messageSplit = receivedMessage.split(" ");
        String sender = messageSplit[1];
        String userName = messageSplit[2];
        String password = messageSplit[3];

        password = BCrypt.hashpw(password, BCrypt.gensalt());

        if (MainServer.dataBaseManager.createUser(userName, UUID.randomUUID().toString(), password)) {
            MainServer.networkerServer.sendMessage(sender + " sign_up ok", "setting");
        } else {
            MainServer.networkerServer.sendMessage(sender + " sign_up fail", "setting");
        }
    }

    private void logIn(String receivedMessage) {
        String[] messageSplit = receivedMessage.split(" ");
        String sender = messageSplit[1];
        String userName = messageSplit[2];
        String password = messageSplit[3];

        ArrayList<String[]> userTempUUID = MainServer.userTempUUID;
        for (int i = 0; i < userTempUUID.size(); i++) {
            String[] userData = userTempUUID.get(i);
            if (userData[0].equals(userName)) {

                MainServer.networkerServer.sendMessage("ping " + userData[0], "setting");

                Timer t = new Timer();
                int tmpI = i;
                t.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (waitingForPong.contains(userData[1])) {
                            for (int i = 0; i < waitingForPong.size(); i++) {
                                if (waitingForPong.get(i).equals(userData[0])) {
                                    waitingForPong.remove(i);
                                    break;
                                }
                            }
                            finalizeLogIn(userName, sender, password, true);
                        } else {
                            userTempUUID.remove(tmpI);
                            finalizeLogIn(userName, sender, password, false);
                        }
                        t.cancel();
                    }
                }, playerMaxTimeout);
                return;
            }
        }
        finalizeLogIn(userName, sender, password, false);
    }

    private void finalizeLogIn(String userName, String sender, String password, boolean alreadyConnected) {
        String rawPassword = MainServer.dataBaseManager.getPassword(userName);
        if (rawPassword != null) {
            String[] splitRawPassword = rawPassword.split("\\$");

            String salt = splitRawPassword[0] + "$" + splitRawPassword[1] + "$" + splitRawPassword[2] + "$" + splitRawPassword[3].substring(0, 22);

            password = BCrypt.hashpw(password, salt);

            String uuid = MainServer.dataBaseManager.logInUser(userName, password);

            if (uuid != null && !alreadyConnected) {
                String onlineToken = UUID.randomUUID().toString();
                MainServer.userTempUUID.add(new String[]{userName, onlineToken, uuid});
                MainServer.networkerServer.sendMessage(sender + " log_in ok " + onlineToken, "setting");
            } else {
                MainServer.networkerServer.sendMessage(sender + " log_in fail", "setting");
            }
        } else {
            MainServer.networkerServer.sendMessage(sender + " log_in fail", "setting");
        }
    }

    private void logOut(String receivedMessage) {
        String[] messageSplit = receivedMessage.split(" ");
        String token = messageSplit[1];
        for (int i = 0; i < MainServer.userTempUUID.size(); i++) {
            if (MainServer.userTempUUID.get(i)[1].equals(token)) {
                MainServer.userTempUUID.remove(i);
                break;
            }
        }
    }
}
