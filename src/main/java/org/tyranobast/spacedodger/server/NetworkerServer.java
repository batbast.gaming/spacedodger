package org.tyranobast.spacedodger.server;

import com.rabbitmq.client.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeoutException;

public class NetworkerServer {

    private final String[] channelSendIDs;
    private final String[] channelReceiveIDs;
    private final Channel[] channelsSend;
    private final Channel[] channelsReceive;
    private Connection connection;

    public NetworkerServer() {

        channelSendIDs = new String[]{"toClientCoordinate", "toClientMessage", "toClientUtil", "toClientSetting"};

        channelReceiveIDs = new String[]{"toServerCoordinate", "toServerMessage", "toServerUtil", "toServerSetting"};

        channelsSend = new Channel[channelSendIDs.length];

        channelsReceive = new Channel[channelReceiveIDs.length];

        try {
            initRabbit();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private SSLContext initKeyStore() throws Exception {
        char[] trustPassphrase = "isur2reimg8k53".toCharArray();
        KeyStore rabbitmqKeyStore = KeyStore.getInstance("JKS");
        String keyStoreLocation = "/rabbitmq/spacedodger.boiteataquets.org.jks";
        rabbitmqKeyStore.load(this.getClass().getResourceAsStream(keyStoreLocation), trustPassphrase);

        TrustManagerFactory trustFactory = TrustManagerFactory.getInstance("SunX509");
        trustFactory.init(rabbitmqKeyStore);

        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, trustFactory.getTrustManagers(), null);

        return sslContext;
    }

    private void initRabbit() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("spacedodger.boiteataquets.org");
        factory.setPort(5671);
        factory.setVirtualHost("spacedodger");
        factory.setUsername("spacedodger_server");
        factory.setPassword("vznwHUIyqeyrsuxckfqjhzlkuqsfhevwnB4kDLY6j");
        factory.enableHostnameVerification();

        try {
            factory.useSslProtocol(initKeyStore());
        } catch (Exception e) {
            e.printStackTrace();
        }

        connection = factory.newConnection();

        // Send message
        for (int i = 0; i < channelsSend.length; i++) {
            channelsSend[i] = connection.createChannel();
            channelsSend[i].queueDeclare(channelSendIDs[i], false, false, true, null);
        }

        // Receive message
        for (int i = 0; i < channelsReceive.length; i++) {
            channelsReceive[i] = connection.createChannel();
            channelsReceive[i].exchangeDeclare(channelReceiveIDs[i], BuiltinExchangeType.FANOUT);
            String queueName = channelsReceive[i].queueDeclare().getQueue();
            channelsReceive[i].queueBind(queueName, channelReceiveIDs[i], "");
            receiveMessage(i, queueName);
        }
    }

    public void sendMessage(String message, String type) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now) + " sending message = " + message + " type = " + type);
        System.out.println();

        int channelIndex;

        try {
            switch (type) {
                case "coordinate" -> channelIndex = 0;
                case "message" -> channelIndex = 1;
                case "util" -> channelIndex = 2;
                case "setting" -> channelIndex = 3;
                default -> throw new IllegalStateException("Error during message sending : unexpected value (channel index) : " + type);
            }

            channelsSend[channelIndex].basicPublish(channelSendIDs[channelIndex], "", null, message.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void receiveMessage(int i, String queueName) {
        DeliverCallback deliverCallback;

        switch (channelReceiveIDs[i]) {
            case "toServerMessage" -> deliverCallback = (consumerTag, delivery) ->
                    MainServer.mainGameServer.messageReceived(new String(delivery.getBody(), StandardCharsets.UTF_8), "toServerMessage");
            case "toServerCoordinate" -> deliverCallback = (consumerTag, delivery) ->
                    MainServer.mainGameServer.messageReceived(new String(delivery.getBody(), StandardCharsets.UTF_8), "toServerCoordinate");
            case "toServerUtil" -> deliverCallback = (consumerTag, delivery) ->
                    MainServer.mainGameServer.messageReceived(new String(delivery.getBody(), StandardCharsets.UTF_8), "toServerUtil");
            case "toServerSetting" -> deliverCallback = (consumerTag, delivery) ->
                    MainServer.mainSettingServer.messageReceived(new String(delivery.getBody(), StandardCharsets.UTF_8));
            default -> deliverCallback = null;
        }

        try {
            channelsReceive[i].basicConsume(queueName, true, deliverCallback, consumerTag -> {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeNetworker() {
        try {
            for (Channel channel : channelsSend) {
                channel.close();
            }
        } catch (TimeoutException | IOException ignored) {
            // channel already closed
        }

        try {
            connection.close();
        } catch (IOException ignored) {
            // connection already closed
        }
    }
}