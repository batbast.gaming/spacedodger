import org.junit.jupiter.api.Test;
import org.mindrot.jbcrypt.BCrypt;
import org.tyranobast.spacedodger.client.Config;
import org.tyranobast.spacedodger.client.ShaMaker;
import org.tyranobast.spacedodger.server.DataBaseManager;

import java.io.File;
import java.net.URISyntaxException;

public class Tests {

    @Test
    public void testConfigImport() throws URISyntaxException {
        System.out.println(new File(Config.class.getProtectionDomain().getCodeSource().getLocation().toURI()));
    }

    @Test
    public void testBCrypt() {
        System.out.println("BCrypt.gensalt() = " + BCrypt.gensalt());
    }

    @Test
    public void testSHA() {
        System.out.println(ShaMaker.getSHA("Totoricot"));
    }

    @Test
    public void testTimeStamp() {
        System.out.println("date = " + System.currentTimeMillis());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("date = " + System.currentTimeMillis());
    }

    @Test
    public void testUserExist() {
        DataBaseManager dataBaseManager = new DataBaseManager();
        System.out.println(dataBaseManager.usernameExist("Mallou"));
    }

    @Test
    public void modifyUsername() {
        DataBaseManager dataBaseManager = new DataBaseManager();
        System.out.println(dataBaseManager.updateUsername("79315cca-2a75-4949-add6-820f023f4a4f", "groin"));
    }

    public static void main(String[] args) {
        Tests result = new Tests();
        result.testBCrypt();
    }
}